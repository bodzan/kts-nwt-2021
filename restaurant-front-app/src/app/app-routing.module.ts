import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./core/login/login.component";
import {LoginGuard} from "./guards/login.guard";
import {RoleGuard} from "./guards/role.guard";
import {AdminHomepageComponent} from "./core/admin/admin-homepage/admin-homepage.component";
import {CreateLayoutComponent} from "./core/admin/create-layout/create-layout.component";
import {ManagerHomepageComponent} from "./core/manager/manager-homepage/manager-homepage.component";
import {ManageUsersComponent} from "./core/manager/manage-users/manage-users.component";
import {UpdateSalaryComponent} from "./core/manager/update-salary/update-salary.component";
import { CookComponent } from './core/cook/cook.component';
import { WaiterComponent } from './core/waiter/waiter.component';
import { EditQuantityComponent } from './core/waiter/edit/edit-quantity/edit-quantity.component';
import { BarHomepageComponent } from './core/bar/bar-homepage/bar-homepage.component';
import { DrinkOrdersComponent } from './core/bar/drink-orders/drink-orders.component';
import { DrinkPreparingComponent } from './core/bar/drink-preparing/drink-preparing.component';
import {ViewLayoutsComponent} from "./core/admin/view-layouts/view-layouts.component";
import {HomepageComponent} from "./core/homepage/homepage.component";
import { AddItemComponent } from './core/manager/add-item/add-item/add-item.component';
import { MeniUpdateComponent } from './core/manager/meni-update/meni-update.component';
import { MeniChangeItemPriceComponent } from './core/manager/meni-change-item-price/meni-change-item-price.component';
import { AddNewDrinkComponent } from './core/manager/add-new-drink/add-new-drink.component';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [LoginGuard]},
  { path: 'waiter/:idTable', component: WaiterComponent },
  { path: 'waiter/edit/:idTable/:idOrderItem', component: EditQuantityComponent },
  { path: 'barHomepage', component: BarHomepageComponent },
  { path: 'bar/orders', component: DrinkOrdersComponent },
  { path: 'bar/prepering', component: DrinkPreparingComponent },
  { path: 'homepage', component: HomepageComponent },

  { path: 'cook/add-item', component: AddItemComponent},
  { path: 'change-price/:idItem', component: MeniChangeItemPriceComponent },

  { path: 'admin-homepage', component: AdminHomepageComponent, data: {
      expectedRoles: 'ROLE_ADMIN'
    },
    children: [
      {path: 'create-layout', component: CreateLayoutComponent},
      {path: "view-layouts", component: ViewLayoutsComponent}
    ]
  },
  {
    path: 'manager-homepage', component: ManagerHomepageComponent,
    children: [
      {
        path: 'manage-users', component: ManageUsersComponent
      },
      {
        path: 'update-salary', component: UpdateSalaryComponent
      },
      { path: 'menu-update', component: MeniUpdateComponent},
      //{ path: 'change-price/:idItem', component: MeniChangeItemPriceComponent },
      { path: 'add-drink', component: AddNewDrinkComponent },
      {
        path: 'add-item', component: AddItemComponent
      },
      {
        path: 'add-drink', component: AddNewDrinkComponent
      },
      {
        path: 'menu-update', component: MeniUpdateComponent
      }



    ],
    data: {expectedRoles: 'ROLE_MANAGER'}
  },
  { path: 'cook-homepage', component: CookComponent, data: {
      expectedRoles: 'ROLE_COOK'
    }
  },
  {path: '**', redirectTo: "/"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
