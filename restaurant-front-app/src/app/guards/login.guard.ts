import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";
import {JwtHelperService} from "@auth0/angular-jwt";


@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(
    public auth: AuthService,
    public router: Router
  ) { }



  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.auth.isLoggedIn()) {
      const token: any = localStorage.getItem('user');
      const jwt: JwtHelperService = new JwtHelperService();

      const info = jwt.decodeToken(token);
      switch (info.role[0].name) {
        case "ROLE_ADMIN":
          this.router.navigate(['/admin-homepage']);
          break;
        case "ROLE_MANAGER":
          this.router.navigate(['/manager-homepage']);
          break;
        case "ROLE_BARTENDER":
          this.router.navigate(['/barHomepage']);
          break;
        case "ROLE_COOK":
          this.router.navigate(['/cook-homepage']);
          break;
        case "ROLE_WAITER":
          this.router.navigate(['/homepage']);
          break;
      }
      return false;
    }
    return true;
  }

}
