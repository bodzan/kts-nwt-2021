import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private auth: AuthService,
    private router: Router
  ) {

  }


  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRoles: string = route.data.expectedRoles;
    const token = localStorage.getItem('user');
    const jwt: JwtHelperService = new JwtHelperService();
    if (!token) {
      this.router.navigate(['/login']);
      return false;
    }

    const info = jwt.decodeToken(token);
    const roles: string[] = expectedRoles.split('|', 2);
    if (roles.indexOf(info.role[0].name) === -1) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
