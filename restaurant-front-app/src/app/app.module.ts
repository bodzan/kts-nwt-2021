import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MainAppComponent } from './core/main-app/main-app.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import { LoginComponent } from './core/login/login.component';
import { AdminHomepageComponent } from './core/admin/admin-homepage/admin-homepage.component';
import {MatButtonModule} from "@angular/material/button";
import { CreateLayoutComponent } from './core/admin/create-layout/create-layout.component';
import { DraggableDirective } from './core/admin/directives/draggable.directive';
import { DroppableDirective } from './core/admin/directives/droppable.directive';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {HttpClientModule} from "@angular/common/http";
import {MatInputModule} from "@angular/material/input";
import { ManagerHomepageComponent } from './core/manager/manager-homepage/manager-homepage.component';
import { ManageUsersComponent } from './core/manager/manage-users/manage-users.component';
import { UpdateSalaryComponent } from './core/manager/update-salary/update-salary.component';
import {MatSelectModule} from "@angular/material/select";
import { CookComponent } from './core/cook/cook.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import { WaiterComponent } from './core/waiter/waiter.component';
import { EditQuantityComponent } from './core/waiter/edit/edit-quantity/edit-quantity.component';
import { DrinkOrdersComponent } from './core/bar/drink-orders/drink-orders.component';
import { DrinkPreparingComponent } from './core/bar/drink-preparing/drink-preparing.component';
import { BarHomepageComponent } from './core/bar/bar-homepage/bar-homepage.component';
import {MatCardModule} from "@angular/material/card";
import {MaterialModule} from "./material/material.module";
import { OnlyNumbersDirective } from './core/admin/directives/only-numbers.directive';
import { ViewLayoutsComponent } from './core/admin/view-layouts/view-layouts.component';
import { HomepageComponent } from './core/homepage/homepage.component';
import { AddItemComponent } from './core/manager/add-item/add-item/add-item.component';
import { AddNewDrinkComponent } from './core/manager/add-new-drink/add-new-drink.component';
import { ToastrModule } from 'ngx-toastr';
import { MeniChangeItemPriceComponent } from './core/manager/meni-change-item-price/meni-change-item-price.component';
import { MeniUpdateComponent } from './core/manager/meni-update/meni-update.component';
import { EditLayoutComponent } from './core/admin/edit-layout/edit-layout.component';


@NgModule({
  declarations: [
    AppComponent,
    MainAppComponent,
    LoginComponent,
    AdminHomepageComponent,
    CreateLayoutComponent,
    DraggableDirective,
    DroppableDirective,
    ManagerHomepageComponent,
    ManageUsersComponent,
    UpdateSalaryComponent,
    CookComponent,
    WaiterComponent,
    EditQuantityComponent,
    DrinkOrdersComponent,
    DrinkPreparingComponent,
    BarHomepageComponent,
    OnlyNumbersDirective,
    ViewLayoutsComponent,
    HomepageComponent,
    AddItemComponent,
    AddNewDrinkComponent,
    MeniUpdateComponent,
    MeniChangeItemPriceComponent,
    EditLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    ToastrModule.forRoot(),

    MatTabsModule,
    MatListModule,
    FormsModule,
    MatCardModule,
    FormsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
