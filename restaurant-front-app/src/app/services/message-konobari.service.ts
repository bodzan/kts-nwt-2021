import { Injectable } from '@angular/core';
declare var SockJS: any;
declare var Stomp : any;

@Injectable({
  providedIn: 'root'
})
export class MessageKonobariService {

  constructor() {
    this.initializeWebSocketConnection();
  }
  public stompClient: any;
  public msg : any= [];
  initializeWebSocketConnection() {
    const serverUrl = 'http://localhost:8080/kuhinja';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame: any) {
      that.stompClient.subscribe('/topic/konobari', (message: any) => {
        if (message.body) {
          that.msg.push(message.body);
          window.location.reload();
        }
      });
    });
  }
  
  sendMessage(message: any) {
    this.stompClient.send('/ws/kuhinja' , {}, message);
  }

  sendMessageBar(message: any) {
    this.stompClient.send('/ws/bar' , {}, message);
  }
}
