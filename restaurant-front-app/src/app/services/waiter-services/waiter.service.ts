import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Item } from 'src/app/shared/item';
import { PriceList } from 'src/app/shared/price-list-item';
import { Table } from 'src/app/shared/table';
import { Order } from 'src/app/shared/order';
import { OrderedItem } from 'src/app/shared/ordered-item';

@Injectable({
  providedIn: 'root'
})

export class WaiterService {

  // Define API
  apiURL = 'api/';
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(private http: HttpClient) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  /*========================================
    CRUD Methods for consuming RESTful API
  =========================================*/


  // HttpClient API get() method => Fetch Items list
  getItems(): Observable<Item> {
    return this.http.get<Item>(this.apiURL+'api/items/allItems', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  orderItem(orderedItem: Item, orderId: number, quantity: number): Observable<Item> {
    const params = new HttpParams()
      .append('orderId', orderId)
      .append('quantity', quantity);
    return this.http.post<Item>(this.apiURL+'api/order/orderItemFromMenu',orderedItem, {
      headers: this.authHeader,
      params: params,
    })
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  changeStatus(idOrder: number, orderStatus: string): Observable<Order> {
    const params = new HttpParams()
      .append('orderStatus', orderStatus);
    return this.http.post<Order>(this.apiURL+'api/order/changeOrderStatus/'+idOrder,null,{
      headers: this.authHeader,
      params: params,
    })
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getItemPriceList(id:number): Observable<PriceList> {
    return this.http.get<PriceList>(this.apiURL+'api/waiter/findItemActivePriceList/'+id, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  cancelOrderAndItems(idOrder:number): Observable<Order> {
    return this.http.put<Order>(this.apiURL+'api/waiter/CancelOrderAndItems/'+idOrder,null, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getTableActiveOrder(id:number): Observable<Table> {
    return this.http.get<Table>(this.apiURL+'api/waiter/tableOrder/'+id, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getOrderedItemsForOrder(id:number): Observable<OrderedItem> {
    return this.http.get<OrderedItem>(this.apiURL+'api/orderedItem/getOrderedItemById/'+id, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getOrderedItemsForOrderWithPrice(orders:OrderedItem[]): Observable<OrderedItem> {
    return this.http.post<OrderedItem>(this.apiURL+'api/waiter/findItemActivePrice/',orders, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getOrderedItemsByID(id:number): Observable<OrderedItem> {
    return this.http.get<OrderedItem>(this.apiURL+'api/waiter/findOrderedItem/'+id, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateOrderedItems(order:OrderedItem): Observable<OrderedItem> {
    return this.http.put<OrderedItem>(this.apiURL+'api/waiter/updateOrderedItem/',order, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  finishOrder(orderId:number): Observable<Order> {
    return this.http.put<Order>(this.apiURL+'api/order/finishOrder/'+orderId,null, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateOrder(order:Order): Observable<Order> {
    return this.http.put<Order>(this.apiURL+'api/order/updateOrder',order, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  CreateEmptyOrder(tableId : number): Observable<Order> {
    return this.http.post<Order>(this.apiURL+'api/waiter/createEmptyNewOrder/'+tableId,null, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

}

