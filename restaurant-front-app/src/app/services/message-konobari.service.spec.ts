import { TestBed } from '@angular/core/testing';

import { MessageKonobariService } from './message-konobari.service';

describe('MessageKonobariService', () => {
  let service: MessageKonobariService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageKonobariService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
