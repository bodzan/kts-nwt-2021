import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.authHeader.append('Content-Type', 'application/json');
  }


  getAllUsers(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all', {headers: this.authHeader, responseType: 'json'});
  }

  getAllActiveUsers(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/allActive', {headers: this.authHeader, responseType: 'json'});
  }

  getUsersPage(page: number, size: number): Observable<any> {
    // if (page === undefined || size === undefined) {
    //   throw throwError("Number and size cannot be undefined!");
    // }
    return this.http.get('api/api/managers/page?page='+ page + '&size=' + size, {headers: this.authHeader, responseType: 'json'})
  }

  getAllAdmins(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all/admins', {headers: this.authHeader, responseType: 'json'});
  }

  getAllManagers(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all/managers', {headers: this.authHeader, responseType: 'json'});
  }

  getAllCooks(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all/cooks', {headers: this.authHeader, responseType: 'json'});
  }

  getAllBartenders(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all/bartenders', {headers: this.authHeader, responseType: 'json'});
  }

  getAllWaiters(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/managers/all/waiters', {headers: this.authHeader, responseType: 'json'});
  }

  addNewEmployee(employee: object): Observable<any> {

    return this.http.post('api/api/managers/newEmployee', employee, {headers: this.authHeader, responseType: 'json'});
  }

  deleteUserById(id: number): Observable<any> {

    return this.http.delete('api/api/managers/' + id, {headers: this.authHeader, responseType: 'json'});
  }

  getUserById(id: number): Observable<any> {
    return this.http.get('api/api/managers/' + id, {headers: this.authHeader, responseType: 'json'});
  }

  updateSalary(newSalary: any): Observable<any> {

    return this.http.put('api/api/managers/employeeSalary', newSalary, {headers: this.authHeader, responseType: 'json'});
  }

}
