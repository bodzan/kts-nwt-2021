import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Item } from 'src/app/shared/item';
import { OrderedItem } from '../shared/ordered-item';

@Injectable({
  providedIn: 'root'
})

export class CookService {

  // Define API
  apiURL = 'api/';
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(private http: HttpClient) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  /*========================================
    CRUD Methods for consuming RESTful API
  =========================================*/

  // Http Options


  // HttpClient API get() method => Fetch Items list
  getItems(): Observable<Item> {
    return this.http.get<Item>(this.apiURL+'api/items/allItems', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getOrderedItems(): Observable<OrderedItem> {
    return this.http.get<OrderedItem>(this.apiURL+'api/orderedItem/getAllOrderedItem', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getDishesInPreparation(): Observable<OrderedItem> {
    return this.http.get<OrderedItem>(this.apiURL+'api/cooks/dishesInPreparation',{headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  updateDishStatus(oi: OrderedItem): Observable<OrderedItem> {
    return this.http.put<OrderedItem>(this.apiURL+'api/orderedItem/updateOrderedItemStatus',oi, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getDishesWaiting(): Observable<OrderedItem> {
    return this.http.get<OrderedItem>(this.apiURL+'api/cooks/waitingDishes',{headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

}
