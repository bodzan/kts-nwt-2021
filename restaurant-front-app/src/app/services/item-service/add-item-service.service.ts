import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Item } from 'src/app/shared/item';
import { ItemObj } from 'src/app/shared/itemInterface';

@Injectable({
  providedIn: 'root'
})
export class AddItemServiceService {



  // Define API
  apiURL = 'api/';
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  private headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private http: HttpClient) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  //metoda vraca listu svih kategorija stavki za jela
  getCategoryListForDish(): string[]{
    return ['SOUPS', 'APPETIZERS',
    'GRILL', 'PIZZAS', 'DESERTS',
      'SALADS', 'MEALS_TO_ORDER' ]
  }

  //metoda vraca listu svih kategorija za pica
  getCategoryListForDrinks(): string[]{
    return ['WATER', 'BEER', 'WINE', 'HOT_DRINKS', 'NON_ALCOHOLIC_DRINKS', 'STRONG_DRINKS' ]
  }

  //ucitava listu alergena
  loadAlergensList(): string[] {
    return ['EGGS', 'MILK', 'CELERY', 'FISH', 'SOYA', 'GLUTEN', 'SULPHITES', 'NUTS', 'SUSAME'];
  }

  //************* dodavanje novog itema **********
  add(newItem: Item): Observable<ItemObj> {
    return this.http.post<ItemObj>(this.apiURL + "/api/items/addNewItem", newItem, {headers: this.authHeader, responseType: 'json'})
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  /** CreateEmptyOrder(tableId : number): Observable<Order> {
    return this.http.post<Order>(this.apiURL+'api/waiter/createEmptyNewOrder/'+tableId,null)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } */

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
