import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Item } from 'src/app/shared/item';
import { Menu } from 'src/app/shared/menu';
import { PriceList } from 'src/app/shared/price-list-item';

@Injectable({
  providedIn: 'root'
})
export class MenuServiceService {
  apiURL = 'api/';
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(private http: HttpClient) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  /*** pronalazak svih aktivnih stavki u meniju***/
  getActiveItemsOfMenu(): Observable<PriceList> {
    return this.http.get<PriceList>(this.apiURL+'api/menu/allActiveItemPriceList', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*******KREIRANJE PRAZNOG MENIJA ********/
  createEmptyMenu(menu: Menu): Observable<Menu> {
    return this.http.post<Menu>(this.apiURL+'api/menu/createMenu', menu, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /*** DODAVANJE STAVKE U MENI ****/
  addItemToMenu(priceListToAdd: PriceList): Observable<PriceList> {
    return this.http.post<PriceList>(this.apiURL+'api/menu/addMenuItem', priceListToAdd, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleErrorAddItem)
    )
  }


  /******LOGICKO BRISANJE STAVKE IZ MENIJA ****/
  logicalDeleteItemFromMenu(idItemToDelete: number):Observable<PriceList>{
    return this.http.delete<PriceList>(this.apiURL + 'api/menu/deleteItemFromMenu/' + idItemToDelete, {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /**********IZMENA CENE STAVKE MENIJA **********/
  changePriceOfItemFromMenu(idItemToChangePrice: number, newPriceOfItem: number):Observable<PriceList>{


    let kastovanaCena = Number(newPriceOfItem);

    const params = new HttpParams()
      .append('newPriceOfItem', kastovanaCena);

    return this.http.put<PriceList>(this.apiURL+'api/menu/changePriceOfItem/'+ idItemToChangePrice, {
      headers: this.authHeader,
      params: params
    })
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  /***** PRONALAZAK ITEMA U MENIJU PO ID-iju ********/


  /**orderItem(orderedItem: Item, orderId: number, quantity: number): Observable<Item> {
    const params = new HttpParams()
      .append('orderId', orderId)
      .append('quantity', quantity);
    return this.http.post<Item>(this.apiURL+'order/orderItemFromMenu',orderedItem, {
      headers: this.httpOptions.headers,
      params: params,
    })
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } */

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

 //obrada greske za dodavanje novih itema..
 handleErrorAddItem(error: { error: { message: string; }; status: any; message: any; }){
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = 'Stavka je vec dodata u meni..';
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
 }
}
