import { Injectable } from '@angular/core';
declare var SockJS: any;
declare var Stomp : any;

@Injectable({
  providedIn: 'root'
})
export class MessageBarService {

  constructor() {
    this.initializeWebSocketConnection();
  }
  public stompClient: any;
  public msg : any= [];
  initializeWebSocketConnection() {
    const serverUrl = 'http://localhost:8080/kuhinja';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame: any) {
      that.stompClient.subscribe('/topic/bar', (message: any) => {
        if (message.body) {
          that.msg.push(message.body);
          window.location.reload();
        }
      });
    });
  }
  
  sendMessage(message: any) {
    this.stompClient.send('/ws/konobari' , {}, message);
  }
}
