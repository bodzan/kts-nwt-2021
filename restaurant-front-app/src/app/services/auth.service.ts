import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }


  login(auth: any): Observable<any> {
    // console.log(auth);
    return this.http.post('api/auth/login', {username: auth.username, password: auth.password},
      {headers: this.headers, responseType: 'json'});
  }

  logout(): void {
    sessionStorage.clear();
    this.router.navigate(['']);

  }


  isLoggedIn(): boolean {
    // console.log(sessionStorage.getItem('user'));
    if (!sessionStorage.getItem('user')) {
      return false;
    }
    return true;
  }
}
