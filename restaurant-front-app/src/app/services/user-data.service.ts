import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private _usersList: any[] = [];

  constructor() { }


  get usersList(): any[] {
    return this._usersList;
  }

  set usersList(value: any[]) {
    this._usersList = value;
  }
}
