import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.authHeader.append('Content-Type', 'application/json');
  }



  getAllLayouts(): Observable<any> {
    // const authHeader = new HttpHeaders().set('Authorization', 'Bearer' + sessionStorage.getItem('user'));

    return this.http.get('api/api/admins/layouts/all', {headers: this.authHeader, responseType: 'json'});
  }

  getActiveLayout(): Observable<any> {

    return this.http.get('api/api/admins/layouts/active', {headers: this.authHeader, responseType: 'json'});
  }

  getLayoutById(id: number): Observable<any> {

    return this.http.get('api/api/admins/layouts/' + id, {headers: this.authHeader, responseType: 'json'});
  }

  createNewLayout(layout: any): Observable<any> {
    return this.http.post('api/api/admins/layouts/new', layout, {headers: this.authHeader, responseType: 'json'});
  }

  setLayoutActive(id: number): Observable<any> {
    return this.http.put('api/api/admins/layouts/active/' + id, null, {headers: this.authHeader, responseType: 'json'});
  }
}
