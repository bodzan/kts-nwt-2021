import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Item } from 'src/app/shared/item';
import { OrderedItem } from 'src/app/shared/ordered-item';


@Injectable({
  providedIn: 'root'
})
export class BarService {

  apiURL = 'api/';
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(private http: HttpClient) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  getWaitingDrinks(): Observable<OrderedItem[]> {
    return this.http.get<OrderedItem[]>(this.apiURL+'api/bartender/allWaitingDrinks', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getPreparingDrinks(): Observable<OrderedItem[]> {
    return this.http.get<OrderedItem[]>(this.apiURL+'api/bartender/allPreparingDrinks', {headers: this.authHeader, responseType: 'json'})
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  handleError(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
