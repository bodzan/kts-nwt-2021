import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private authHeader = new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('user'));
  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.authHeader.append('Content-Type', 'application/json');
  }

  getActiveOrdersForTable(tableId: number): Observable<any> {
    return this.http.get('api/api/tables/allOrdersOfTable/' + tableId, {headers: this.authHeader, responseType: 'json'})
  }
}
