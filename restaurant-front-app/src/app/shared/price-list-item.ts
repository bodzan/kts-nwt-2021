import { Item } from "./item";
import { Menu } from "./menu";

export class PriceList {
    idPriceList: number;
    menu: any;
    item: Item;
    itemPrice: number;
    dateFrom: any;
    dateTo: any;
    active: boolean;

    constructor(){
        this.idPriceList = 0;
        this.menu = new Menu();
        this.item = new Item();
        this.itemPrice = 0;
        this.dateFrom = [];
        this.dateTo =[];
        this.active = false;
    }
}
