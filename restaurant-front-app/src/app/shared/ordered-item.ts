import { Item } from "./item";
import { Order } from "./order";

export class OrderedItem {
    order: Order;
    menuItem: Item;
    quantity: number;
    price: number;
    orderedItemStatus: string;
    id: number;

    constructor(){
        this.quantity = 0;
        this.price = 1;
        this.order = new Order;
        this.menuItem = new Item();
        this.orderedItemStatus = "";
        this.id = 0;
    }

}
