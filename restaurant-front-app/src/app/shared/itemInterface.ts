export interface ItemObj{
    idItem: number;
    category: string;
    subcategory: string;
    alergenList: string;
    description: string;
    active: boolean;
    itemName: string;

  
}