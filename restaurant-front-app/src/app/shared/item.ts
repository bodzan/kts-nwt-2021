export class Item {
    idItem: number;
    category: string;
    subcategory: string;
    alergenList: string;
    description: string;
    active: boolean;
    itemName: string;

    constructor(){
        this.idItem = 0;
        this.category = "";
        this.subcategory = "";
        this.alergenList = "";
        this.description = "";
        this.active = false;
        this.itemName = "";
    }
    
}
