import { Table } from "./table";

export class Order {
    idOrder: number;
    dateAndTime: any;
    note: string;
    totalPrice: number;
    deliverForTable: Table;


    constructor(){
        this.idOrder = 0;
        this.dateAndTime = [];
        this.deliverForTable = new Table();
        this.note = "";
        this.totalPrice = 0;
    }
}
