import { Component, OnInit } from '@angular/core';
import {AdminService} from "../../../services/admin.service";
import {TableDto} from "../../../model/table-dto";
import {toNumbers} from "@angular/compiler-cli/src/diagnostics/typescript_version";
import {isNumeric} from "rxjs/internal-compatibility";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-view-layouts',
  templateUrl: './view-layouts.component.html',
  styleUrls: ['./view-layouts.component.sass']
})
export class ViewLayoutsComponent implements OnInit {
  layouts: any;
  selectedLayoutId: any;
  selectedLayoutTables: any;

  constructor(
    private adminService: AdminService,
    private toastrService: ToastrService

  ) {
    this.layouts = [];
  }

  ngOnInit(): void {
    this.adminService.getAllLayouts().subscribe(
      response => {
        response.forEach((layout: any) => {
          let layoutData = {
            id: layout.id,
            active: layout.active,
            tables: Array<any>()
          }
          let tableArray = layout.coordinates.split(';');
          for(let i = 0; i < layout.tables.length; i++) {
            let tableCoords = tableArray[i].split(',');
            layoutData.tables.push(new TableDto(tableCoords[0], tableCoords[1], tableCoords[2], layout.tables[i].restaurantRoom));
          }
          this.layouts.push(layoutData);
        });
        console.log(this.layouts);
      },
      error => {

      }
    );
  }

  selectedLayoutChanged(): void {
    this.selectedLayoutTables = this.layouts[this.selectedLayoutId-1].tables;
    console.log(this.selectedLayoutTables);
    // console.log(this.selectedLayoutId);
    // console.log(this.selectedLayoutTables);
  }

  setSelectedLayoutActive(): void {
    // console.log(this.selectedLayoutId)
    this.adminService.setLayoutActive(this.selectedLayoutId).subscribe(data => {
      if (data) {
        this.toastrService.success("Layout " + this.selectedLayoutId + " is now active!", "Active layout changed!");
      }
      console.log(data);
    },
      error => {

      })
  }

}
