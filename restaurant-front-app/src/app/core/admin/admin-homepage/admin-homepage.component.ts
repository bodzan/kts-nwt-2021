import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-admin-homepage',
  templateUrl: './admin-homepage.component.html',
  styleUrls: ['./admin-homepage.component.sass']
})
export class AdminHomepageComponent implements OnInit {

  constructor(
    private auth: AuthService
  ) { }

  ngOnInit(): void {

  }


  logout(): void {
    this.auth.logout();
  }

}
