  import {
  Component, Host, HostListener,
  OnInit,
  Renderer2,
  RendererFactory2
} from '@angular/core';
  import {SvgService} from "../../../services/svg.service";
  import {toNumbers} from "@angular/compiler-cli/src/diagnostics/typescript_version";
  import {TableDto} from "../../../model/table-dto";
  import {AdminService} from "../../../services/admin.service";

@Component({
  selector: 'app-create-layout',
  templateUrl: './create-layout.component.html',
  styleUrls: ['./create-layout.component.sass']
})
export class CreateLayoutComponent implements OnInit {
  tables: TableDto[];
  lastCreatedTableId: number;
  layoutId: number;

  constructor(
    private adminService: AdminService
  ) {
    this.tables = [];
    this.layoutId = -1;
    this.lastCreatedTableId = -1;
  }

  ngOnInit(): void {
    this.adminService.getActiveLayout().subscribe(data => {
      this.layoutId = data.id;
      let tableArray = data.coordinates.split(';');
      for(let i = 0; i < data.tables.length; i++) {
        let tableCoords = tableArray[i].split(',');
        this.tables.push(new TableDto(Number(tableCoords[0]), tableCoords[1], tableCoords[2], data.tables[i].restaurantRoom));
      }

      this.lastCreatedTableId = this.tables.length+1;
    });

  }

  addNewTable(): void {
    let newTable = new TableDto(this.lastCreatedTableId, 25, 25, 'GROUND_FLOOR');
    this.tables.push(newTable);
    this.lastCreatedTableId++;
  }

  deleteTable(event: any): void {
    // console.log(toNumbers(event.target.getAttribute('id'))[0]);
    this.tables = this.tables.filter(table => table.id !== toNumbers(event.target.getAttribute('id'))[0]);
    // console.log(tables);

  }

  viewTable(): void {
    console.log(this.tables);
  }

  createNewLayout(): void {
    let coordinates: string = ""
    this.tables.forEach((table) => {
      coordinates += "" + table.id + "," + table.x + "," + table.y + ";";
    });
    coordinates = coordinates.slice(0, coordinates.length - 1);
    // console.log(coordinates)
    let layoutTables: any = [];
    this.tables.forEach((table) => {
      layoutTables.push({id:table.id, room: table.room});
    })
    let newLayout = {
      active: false,
      coordinates,
      tables: layoutTables
    }
    console.log(newLayout)

    this.adminService.createNewLayout(newLayout).subscribe(data => {

    })
  }

  tablePositionChange(event: any) {
    let table: any
    for (let i = 0; i<this.tables.length; i++) {
      if (this.tables[i].id === toNumbers(event.target.attributes.id.value)[0]){
        this.tables[i].x = event.target.attributes.cx.value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        this.tables[i].y = event.target.attributes.cy.value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        // console.log(this.tables[i]);
      }
    }
  }

}
