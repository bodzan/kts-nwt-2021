import { Component, OnInit } from '@angular/core';
import {UsersService} from "../../../services/users.service";
import {UserDataService} from "../../../services/user-data.service";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-manager-homepage',
  templateUrl: './manager-homepage.component.html',
  styleUrls: ['./manager-homepage.component.sass']
})
export class ManagerHomepageComponent implements OnInit {
  managers: any;
  waiters: any;
  cooks: any;
  bartenders: any;
  admins: any;
  // users: any;


  constructor(
    private userService: UsersService,
    private usersData: UserDataService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userService.getAllActiveUsers().subscribe(data => {
      this.usersData.usersList = data;
      // console.log(data);
    },
      error => {
      console.log(error);
    });
  }

  logout(): void {
    this.authService.logout()
  }


}
