import {Component, Input, OnInit} from '@angular/core';
import {UserDataService} from "../../../services/user-data.service";
import {UsersService} from "../../../services/users.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-update-salary',
  templateUrl: './update-salary.component.html',
  styleUrls: ['./update-salary.component.sass']
})
export class UpdateSalaryComponent implements OnInit {
  form: FormGroup;
  selectedUser: any;
  users: any = [];

  constructor(
    private usersData: UserDataService,
    private userService: UsersService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      newSalary: [0, [Validators.required, Validators.maxLength(7), Validators.min(15000)]]
    })
  }

  ngOnInit(): void {
    this.users = this.usersData.usersList;
    console.log(this.users)
  }

  changedSalaryField(): void {
    console.log(this.form.value.newSalary);
  }

  updateSalary(): void {
    // console.log( Math.abs(this.selectedUser.salary - this.form.value.newSalary))
    if (this.form.valid && Math.abs(this.selectedUser.salary - this.form.value.newSalary) < this.selectedUser.salary*0.1) {
      // console.log(this.form.value.newSalary);
      this.userService.updateSalary({newSalary: this.form.value.newSalary, id: this.selectedUser.id}).subscribe(
        data => {

        }
      )
    }
  }

}
