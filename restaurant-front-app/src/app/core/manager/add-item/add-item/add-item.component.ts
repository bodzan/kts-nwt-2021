import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { AddItemServiceService } from 'src/app/services/item-service/add-item-service.service';
import { Item } from 'src/app/shared/item';

//import { AddItemServiceService } from 'src/app/services/item-service/add-item-service.service';



@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.sass']
})
export class AddItemComponent implements OnInit {
  
  itemForm!: FormGroup;
  item!: Item; //item interfejs 

  luxDugmicSelektovan: boolean = false;
  regularDugmicSelektovan: boolean = false;
  selectedSubcategory: string = "";

  alergen: string[] = []
  choosenAlergen: string[] = []
 //item interfejs 
  //wine: Wine;
  //wineForm: FormGroup;
  //number: number;

  SubCategory: String[] = []; //podkategorije

  //category: string; //izabrana selektovana kategorija

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private itemService: AddItemServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.createForm();
  }

  ngOnInit( ) {
    //this.number = this.route.snapshot.params.broj as number;
    this.SubCategory = this.itemService.getCategoryListForDish();
    this.alergen = this.itemService.loadAlergensList();
    console.log(this.SubCategory);
    console.log("Lista alergena je: " + this.alergen);

    this.regularDugmicSelektovan = false;
    this.luxDugmicSelektovan = false;

  }

  createForm() {
    this.itemForm = this.fb.group({
      itemName: [
        "", [Validators.required, Validators.minLength(2)]
      ],
      category: ["", Validators.required],
      purchasePrice: ["", Validators.required],
      description: ["", Validators.required],
      lux: [""],
      regular: [""],
      choosenAlergen: [""]

    });
  }

  onSubmit() {
    this.item = this.itemForm.value;
    this.item.active = false; //podesavamo da item nije vidljiv
    this.item.category = "DISH" //podesvamo automatski da je jelo posto ga kuvar dodaje..
    this.item.alergenList = "" //za sada je prazna lista sa alergenima..
    this.item.subcategory = this.selectedSubcategory;

    //ucitavamo sve alergene iz liste u string
    for (let a of this.choosenAlergen){
      this.item.alergenList += a +" ";
    }

    console.log("Ispis poslatih podatkaa: " + this.item.active  + ", lista alergena: " + this.item.alergenList + "kategorija " + this.item.category+
    
    " podkategorija" + this.item.subcategory + ", naziv itema: " + this.item.itemName + 
    " ,description: " + this.item.description);
    console.log("Ovo je item kliknuto je na submit......" + this.item);
    this.itemService.add(this.item).subscribe((data: any) => {
      console.log("Ovo su podaci nakon dodavanja...");
      console.log(data);
      alert("Stavka sa nazivom " + data.itemName + " je dodata..");
      //return this.toastr.success(result);
      //this.router.navigate(["wines"]);
    });

    /***this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      window.location.reload(); 
    });**/
  }



  //metoda koja se poziva prilikom izbora vrste komponente
  selectChangeHandlerS (event: any) {
    console.log("Promenjena vrednost kategorije stavke.." + event.target.value);
    this.selectedSubcategory = event.target.value;
    console.log("Selektovana vrednost je: " + this.selectedSubcategory);
    
  }

  handleChangeRadioButton(event: any){
    console.log("Kliknuto je radio button...");

    let target = event.target;

    console.log("TArget je: "  + target);

    if(target.checked){
      console.log("Selektovacu ovo....");
      //trebamo onaj drugi dugmic da deselektujemo automatski
      if(target.value == "lux"){
        console.log("Selektovan je lux dugmic");
        this.luxDugmicSelektovan = true;
        this.regularDugmicSelektovan = false;
        
        
      }
      else{
        console.log("Selektovan je regular dugmic..");
        this.luxDugmicSelektovan = false;
        this.regularDugmicSelektovan = false;
        //sad ako je jedan selektovan drugi ce automatski biti deselektovan..
      }
    }
    console.log("Status lux je: " + this.luxDugmicSelektovan + ", status regular je: " + this.regularDugmicSelektovan);
  }

  selectChangeHandlerAlergens (event: any) {
    console.log("Dodat novi alergen" + event.target.value.value);
    this.choosenAlergen.push(event.target.value);
    console.log("Lista dodatih alergena je: " + this.choosenAlergen);
    
  }

  /** handleChange(evt) {
      var target = evt.target;
      if (target.checked) {
        doSelected(target);
        this._prevSelected = target;
      } else {
        doUnSelected(this._prevSelected)
      }
    } */
  
    }
  

  
