import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeniUpdateComponent } from './meni-update.component';

describe('MeniUpdateComponent', () => {
  let component: MeniUpdateComponent;
  let fixture: ComponentFixture<MeniUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeniUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeniUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
