import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuServiceService } from 'src/app/services/menu-service/menu-service.service';
import { WaiterService } from 'src/app/services/waiter-services/waiter.service';
import { Item } from 'src/app/shared/item';
import { Menu } from 'src/app/shared/menu';
import { PriceList } from 'src/app/shared/price-list-item';

@Component({
  selector: 'app-meni-update',
  templateUrl: './meni-update.component.html',
  styleUrls: ['./meni-update.component.sass']
})
export class MeniUpdateComponent implements OnInit {

  
  SubCategory: String[] = [];
  itemList: any = [];
  filterItemList: any = [];
  activeItemsInActiveMenu: PriceList[] = [];

  //filtriranje 
  choosenCategory: any;
  choosenSubcategory: any;

  //promenljiva za filtriranje po kategoriji pice ili jelo u tabeli za prikaz trenutnog menija
  activeMenuFilter: PriceList[] = []

  menu: Menu = new Menu();

  isInputHidden: boolean = false;
  toAddItemDisplay: Item = new Item();
  priceListToAdd: PriceList = new PriceList();

  itemPrice: number = 0;
  errorMessageAddingItem: boolean = false;

  
  constructor(private menuService: MenuServiceService, private waiterService: WaiterService, private route: ActivatedRoute, private ruter: Router) { }

  ngOnInit(): void {
    this.loadItemsSubcategory()  //ucitavanje podkategorija stavki koje se mogu dodati u meni (u filter select)
    this.loadActiveItemPriceListOfActiveMenu(); //ucitamo stavke koje su trenutno u meniju..
    
  }


  /***UCITAVANJE SVIH DOSTUPNIH KATEGORIJA ZA FILTER SELECT PO PODKATEGORIJI */
  //ucitavamo podkategorije svih itema koji postoje..>>mozda bi bolje bilo onih koji nisu u meniju
  loadItemsSubcategory() {
    return this.waiterService.getItems().subscribe((data: any) => {
      this.itemList = data;
      this.filterItemList = data;
      this.activeMenuFilter = data; //prikaz u tabeli sa stavkama u aktivnom meniju
      for(let i = 0; i<this.itemList.length;i++)
      {
        if(!this.SubCategory.includes(this.itemList[i].subcategory))
          
          this.SubCategory.push(this.itemList[i].subcategory);
      }

    })
  }
  


  /**selectChangeHandlerS (event: any) {

    this.choosenS = event.target.value;
    let newItems : any = [];

    if(this.choosenS=="ALL")
    {
      this.loadItems();
      return;
    }
    this.Item = this.ItemAll;

    for(let i = 0; i<this.Item.length;i++)
      {
        if(this.Item[i].subcategory == this.choosenS)
        {

          newItems.push(this.Item[i]);
        }
      }

    this.Item = newItems;
  }**/

  /****** UCITAVANJE TRENUTNO AKTIVNIH STAVKI U MENIJU *******/
  loadActiveItemPriceListOfActiveMenu(){
    return this.menuService.getActiveItemsOfMenu().subscribe((data: any) =>{
      this.activeItemsInActiveMenu = data;
      this.activeMenuFilter = data;
      console.log("Ovo su aktivne stavke menija: " + this.activeItemsInActiveMenu);
      //ako nema aktivnih komponenti u meniju >> to znaci da meni ne postoji>> kreiramo novi meni
      //ucitavamo i aktivni meni >> ako nijedan meni nije ucitan onda pravimo novi meni..
      if (this.loadActiveItemPriceListOfActiveMenu.length == 0){
        //kreiramo meni
        //this.createMenu(this.menu);
      }
      else{
        

        
      }
    })

  }

  /** kreiramo meni ako nista nije dodato  */
  createMenu(menu: Menu){
    return this.menuService.createEmptyMenu(menu).subscribe((data: any) => {
      this.menu = data; //podaci o meniju koji je aktivan
      this.loadActiveItemPriceListOfActiveMenu();
      console.log("Aktivni meni je: " + this.menu.idMenu + "aktivan je: " + this.menu.isActive);

    })
  }
  
  /** ucitaj aktivni meni>> ako nije ucitan pravimo novi.. */
  getActiveMenu(){

  }
  /**
   * // Tables orders
  loadOrderOfTable() {
    return this.waiterService.getTableActiveOrder(this.OpendTableID).subscribe((data: any) => {
      this.OrdersOfTable = data;

      if(this.OrdersOfTable.length==0)
      {
        this.createOrder();
      }
      else{
        this.TableOrder = this.OrdersOfTable[0];
        this.loadOrderedItems();
      }

    })
  }
   */

  /**klik  na filtriranje po  kategoriji */
  selectChangeHandlerCategory(event: any) {
    //updatovanje prikaza za kartice
    let newItems : any = [];
    this.choosenCategory = event.target.value;
    console.log("Izabrana je kategorija: " + this.choosenCategory);

    if(this.choosenCategory=="ALL")
    {
      //ucitacemo sve kartice
      this.loadItemsSubcategory();
      
      //azuriramo i tabelu sa svim aktivnim stavkama u meniju
      //console.log("Potrebno je prikazati ceo meni sa svim aktivnim stavkama.....");
      this.activeMenuFilter = this.activeItemsInActiveMenu;
      //console.log(this.activeItemsInActiveMenu);
      //console.log("Svi elementi menija su: " + this.activeItemsInActiveMenu[0].item.itemName);
      return;
    }
    this.filterItemList = this.itemList;

    for(let i = 0; i<this.filterItemList.length;i++)
      {
        if(this.filterItemList[i].category == this.choosenCategory)
        {
          console.log("U listu dodata kategorija: " + this.itemList[i].itemName);
          newItems.push(this.itemList[i]);
        }
      }

    this.filterItemList = newItems;
    console.log("Ovo su filtrirane vrednosti: " + this.filterItemList);
    
    //sad vrsimo filtriranje  i u tabeli za prikaz aktivnog menija..
    //this.activeMenuFilter = newItems; //filtriranje u tabeli za prikaz po kategoriji jelo ili pice

    let newItemsOfActiveMenu : any = [];

    
    this.activeMenuFilter = this.activeItemsInActiveMenu;

    for(let i = 0; i<this.activeMenuFilter.length;i++)
      {
        if(this.activeMenuFilter[i].item.category == this.choosenCategory)
        {
          newItemsOfActiveMenu.push(this.activeMenuFilter[i]);
        }
      }

    this.activeMenuFilter = newItemsOfActiveMenu;
    console.log("Filtrirane vrednosti aktivnog menija po kategoriji pice jelo su: " + this.activeMenuFilter);
  }

  /**klik  na filtriranje po  podkategoriji */
  selectChangeHandlerSubcategory(event: any) {
    //updatovanje prikaza za kartice
    let newItems : any = [];
    this.choosenSubcategory = event.target.value;
    console.log("Izabrana je kategorija: " + this.choosenSubcategory);

    if(this.choosenSubcategory=="ALL")
    {
      //ucitacemo sve kartice
      //this.loadItemsSubcategory();
      return;
    }
    this.filterItemList = this.itemList;

    for(let i = 0; i<this.filterItemList.length;i++)
      {
        if(this.filterItemList[i].subcategory == this.choosenSubcategory)
        {
          console.log("U listu dodata kategorija: " + this.itemList[i].itemName);
          newItems.push(this.itemList[i]);
        }
      }

    this.filterItemList = newItems;
    console.log("Ovo su filtrirane vrednosti: " + this.filterItemList);

  }
  
  /******** DODAVANJE STAVKE U MENI ******/
  
  //selekcija stavke za dodavanje
  public onCardClick(event: any, num: Item){
    this.loadClickedItem(num);
  }

  //ucitavanje izabrane stavke za dodavanje
  loadClickedItem( num: Item){
    this.isInputHidden = true; //ako je izabrana stavka za dodavanje prikazi informacije u polju za dodavanje nove stavke
    this.toAddItemDisplay = num;
  }

  /**** dogadjaj aktiviran za unos cene stavke koju dodajemo u meni ****/
  public changePrice(event: any)
  {
    this.itemPrice= event.target.value;
  }

  /** logicko brisanje itema iz menija ****/
  public logicalDeleteItem(event: any, itemToDeleteLogicaly: PriceList)
  {
    //window.alert("Id itema za brisanje je: " + itemToDeleteLogicaly.idPriceList + " naziv itema koji brisemo je: " + itemToDeleteLogicaly.item.itemName);
    let idItemForDelete = itemToDeleteLogicaly.item.idItem;


    //saljemo zahtev za logicko brisanje ovog itema..

    this.menuService.logicalDeleteItemFromMenu(idItemForDelete).subscribe((data: any) =>{
      console.log("Brisanje itema sa id-ijem " + idItemForDelete + " ," + data);
      window.location.reload();
    });
    /**let OrderedItem = item;
    OrderedItem.orderedItemStatus = "CANCELED";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      window.location.reload(); 
    });*/
  }

  /*** promena cene stavke menija***/
  public onChangePriceOfItem(event: any, itemToChangePrice: PriceList)
  {
    //window.open('waiter/edit/'+this.OpendTableID+'/'+idItemOrdered,"_self"); // etc
    let idOfItemToChangePrice = itemToChangePrice.idPriceList;
    //window.alert("Vrsimo promenu cene stavke sa id-ijem: " + idOfItemToChangePrice);
    //manager/change-price/:idItem/:newPriceOfItem'

    window.open('change-price/'+ idOfItemToChangePrice,"_self"); // etc

  }

  /** dodavanje itema u meni ****/
  public addItemToMenu(event: any){
    //proverimo da li je unesena infromacija o ceni stavke koju dodajemo..
    if (this.itemPrice ==0){
      this.errorMessageAddingItem = true; //prikazi poruku da se mora uneti cena
      alert("Please enter value of item price..");
    }
    else{
      //vrsimo dodavanje izabrane stavke u meni

      console.log("Ovo su vrednosti stavke menija koju dodajemo u ovaj meni...");
      console.log("Naziv: " + this.toAddItemDisplay.itemName + ", status: " + this.toAddItemDisplay.active + "kategorija" + this.toAddItemDisplay.category);
      //napravimo novi priceList objekat >> dodajemo ovaj objekat

      //pre dodavanja setujemo vrednosti priceList objekta koji dodajemo
      this.priceListToAdd.itemPrice = this.itemPrice; //postavimo cenu koja je unesena
      this.priceListToAdd.item = this.toAddItemDisplay; //postavimo item koji dodajemo
      this.priceListToAdd.menu = this.menu;

      console.log("Za stavku koju dodajemo u meni cena je:  " + this.priceListToAdd.itemPrice + " a naziv itema je: "+ this.priceListToAdd.item.itemName);

      //zahtev ka serveru za dodavanje

      this.menuService.addItemToMenu(this.priceListToAdd).subscribe((data: any) =>{
        console.log("Ovo je dodato u meni: " + data);
        
        window.location.reload();
      });

    }
    }
}


