import { Component, OnInit } from '@angular/core';
import {UserDataService} from "../../../services/user-data.service";
import {UsersService} from "../../../services/users.service";
import {PageEvent} from "@angular/material/paginator";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.sass']
})
export class ManageUsersComponent implements OnInit {
  users: any;
  pageEvent: PageEvent | undefined;
  datasource: null;
  pageIndex: number | undefined;
  pageSize:number | undefined;
  length:number | undefined;
  form: FormGroup;

  constructor(
    private usersData: UserDataService,
    private userService: UsersService,
    private fb: FormBuilder

  ) {
    this.form = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      role: ['', Validators.required],
      salary: ['', Validators.required]
    })
  }

  ngOnInit(): void {
    this.userService.getAllActiveUsers().subscribe(data => {
      this.users = data;
      this.length = data.length;
    })

    let initPage = new PageEvent();
    initPage.pageIndex = 0;
    initPage.pageSize = 3;
    this.getPageData(initPage);
  }

  getPageData(event: PageEvent): PageEvent {
    // console.log(event);
    this.userService.getUsersPage(event.pageIndex, event.pageSize).subscribe(
      response => {
        // console.log(response);
        this.datasource = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        // console.log(this.datasource);
        // this.length = response.numberOfElements;
      }, error => {

      }
    );
    return event;
  }

  addNewEmployee(): void {
    // console.log(this.form.value);
    // console.log(this.form.valid);
    let newUser: any = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.email,
      role: this.form.value.role,
      salary: this.form.value.salary
    };
    this.userService.addNewEmployee(newUser).subscribe(data => {
      console.log(data);
      this.form.reset();
    })

  }

  deleteEmployee(event: any): void {
    console.log(event.option._element.nativeElement.getAttribute('id'));
    let id = event.option._element.nativeElement.getAttribute('id');
    this.userService.deleteUserById(id).subscribe(
      result => {
        if (result.status === 200) {

        }
      },
      error => {

      }
    );
  }



}
