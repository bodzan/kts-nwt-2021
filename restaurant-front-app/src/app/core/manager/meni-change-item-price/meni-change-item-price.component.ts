import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuServiceService } from 'src/app/services/menu-service/menu-service.service';
import { PriceList } from 'src/app/shared/price-list-item';

@Component({
  selector: 'app-meni-change-item-price',
  templateUrl: './meni-change-item-price.component.html',
  styleUrls: ['./meni-change-item-price.component.sass']
})
export class MeniChangeItemPriceComponent implements OnInit {

  itemToChangePrice: PriceList = new PriceList(); //stavka menija koju smo selektovali za izmenu cene
  idOfItemToChangePrice: any = [];
  newPrice: number = 0;

  constructor(private route: ActivatedRoute,private menuService: MenuServiceService) {
     //pronalazimo iz putanje vrednost id-ija stavke menija kojoj menjamo cenu
     this.idOfItemToChangePrice = this.route.snapshot.paramMap.get('idItem');

     console.log("Id item price list objekta kome menjamo cenu je: " + this.idOfItemToChangePrice);

   }

  ngOnInit(): void {
    this.loadItemWhichChangingPrice();
  }

  /*** ucitavanje svih itema koji su aktivni u meniju *****/
  loadItemWhichChangingPrice(){
    return this.menuService.getActiveItemsOfMenu().subscribe((data: any) => {

      //iteracijom kroz sve iteme pronadjemo onaj koji ima id isti kao onaj kome menjamo cenu
      for( let activeItem of data){

        if (activeItem.idPriceList == this.idOfItemToChangePrice){

          this.itemToChangePrice = activeItem;
        }
      }
    })
  }

  /****** obradjujemo dogadjaj za unos nove cene u polje *****/
  onChangeItemPriceInputField(event: any){
    this.newPrice = event.target.value;
    console.log("******** Nova cena je: " + this.newPrice);
  }

  /****** otkazivanje izmene cene stavke>> vracanje nazad ****/
  public cancelIt(event: any)
  {
    window.open('manager/menu-update/',"_self");
  }

  //cuvanje izmena slanje zahteva
  saveChanges(){

    return this.menuService.changePriceOfItemFromMenu(this.idOfItemToChangePrice, this.newPrice).subscribe((data: any) => {
      this.itemToChangePrice = data;
      console.log("Promenjene vrednosti: " + data.itemPrice);
      window.open('manager/update-menu' , "_self");
    })
  }

  /***** cuvanje odnosno izmena cene *****/

  public saveChangedPrice(event: any)
  {
    //if(this.newPrice < 0)
    //{
      this.itemToChangePrice.itemPrice = this.newPrice; //promenimo cenu
      this.saveChanges();
    //}
    //else
    //{
      //alert("Greska! Neispravna cena!");
    //}

  }


}
