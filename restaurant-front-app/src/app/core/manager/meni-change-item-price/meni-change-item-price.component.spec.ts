import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeniChangeItemPriceComponent } from './meni-change-item-price.component';

describe('MeniChangeItemPriceComponent', () => {
  let component: MeniChangeItemPriceComponent;
  let fixture: ComponentFixture<MeniChangeItemPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeniChangeItemPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeniChangeItemPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
