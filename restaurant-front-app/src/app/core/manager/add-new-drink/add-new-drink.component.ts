import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddItemServiceService } from 'src/app/services/item-service/add-item-service.service';
import { Item } from 'src/app/shared/item';

@Component({
  selector: 'app-add-new-drink',
  templateUrl: './add-new-drink.component.html',
  styleUrls: ['./add-new-drink.component.sass']
})
export class AddNewDrinkComponent implements OnInit {

  itemForm!: FormGroup;
  item!: Item; //item interfejs 

  luxDugmicSelektovan: boolean = false;
  regularDugmicSelektovan: boolean = false;
  selectedSubcategory: string = "";
 //item interfejs 
  //wine: Wine;
  //wineForm: FormGroup;
  //number: number;

  SubCategory: String[] = []; //podkategorije

  //category: string; //izabrana selektovana kategorija

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private itemService: AddItemServiceService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {
    this.createForm();
  }

  ngOnInit( ) {
    //this.number = this.route.snapshot.params.broj as number;
    this.SubCategory = this.itemService.getCategoryListForDrinks();
    console.log(this.SubCategory);

    this.regularDugmicSelektovan = false;
    this.luxDugmicSelektovan = false;

  }

  createForm() {
    this.itemForm = this.fb.group({
      itemName: [
        "", [Validators.required, Validators.minLength(2)]
      ],
      category: ["", Validators.required],
      purchasePrice: ["", Validators.required],
      description: ["", Validators.required],
      lux: [""],
      regular: [""]

    });
  }

  onSubmit() {
    this.item = this.itemForm.value;
    this.item.active = false; //podesavamo da item nije vidljiv
    this.item.category = "DRINK" //podesvamo automatski da je jelo posto ga kuvar dodaje..
    this.item.alergenList = "" //za sada je prazna lista sa alergenima..
    this.item.subcategory = this.selectedSubcategory;

    console.log("Ispis poslatih podatkaa: " + this.item.active  + ", lista alergena: " + this.item.alergenList + "kategorija " + this.item.category+
    
    " podkategorija" + this.item.subcategory + ", naziv itema: " + this.item.itemName + 
    " ,description: " + this.item.description);
    console.log("Ovo je item kliknuto je na submit......" + this.item);
    this.itemService.add(this.item).subscribe((data: any) => {
      console.log("Ovo su podaci nakon dodavanja...");
      console.log(data);
      alert("Stavka sa nazivom " + data.itemName + " je dodata..");

      //return this.toastr.success(result);
      //this.router.navigate(["wines"]);
    });

    /***this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      window.location.reload(); 
    });**/
  }



  //metoda koja se poziva prilikom izbora vrste komponente
  selectChangeHandlerS (event: any) {
    console.log("Promenjena vrednost kategorije stavke.." + event.target.value);
    this.selectedSubcategory = event.target.value;
    console.log("Selektovana vrednost je: " + this.selectedSubcategory);
    
  }

  handleChangeRadioButton(event: any){
    console.log("Kliknuto je radio button...");

    let target = event.target;

    console.log("TArget je: "  + target);

    if(target.checked){
      console.log("Selektovacu ovo....");
      //trebamo onaj drugi dugmic da deselektujemo automatski
      if(target.value == "lux"){
        console.log("Selektovan je lux dugmic");
        this.luxDugmicSelektovan = true;
        this.regularDugmicSelektovan = false;
        
        
      }
      else{
        console.log("Selektovan je regular dugmic..");
        this.luxDugmicSelektovan = false;
        this.regularDugmicSelektovan = false;
        //sad ako je jedan selektovan drugi ce automatski biti deselektovan..
      }
    }
    console.log("Status lux je: " + this.luxDugmicSelektovan + ", status regular je: " + this.regularDugmicSelektovan);
  }

  /** handleChange(evt) {
      var target = evt.target;
      if (target.checked) {
        doSelected(target);
        this._prevSelected = target;
      } else {
        doUnSelected(this._prevSelected)
      }
    } */
  


}
