import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkPreparingComponent } from './drink-preparing.component';

describe('DrinkPreparingComponent', () => {
  let component: DrinkPreparingComponent;
  let fixture: ComponentFixture<DrinkPreparingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrinkPreparingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkPreparingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
