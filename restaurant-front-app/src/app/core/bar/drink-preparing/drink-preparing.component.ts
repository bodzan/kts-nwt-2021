import { Component, OnInit } from '@angular/core';
import { BarService } from 'src/app/services/bar-services/bar.service';
import { WaiterService } from 'src/app/services/waiter-services/waiter.service';
import { OrderedItem } from 'src/app/shared/ordered-item';
import { Router } from '@angular/router';
import { MessageBarService } from 'src/app/services/message-bar.service';

@Component({
  selector: 'app-drink-preparing',
  templateUrl: './drink-preparing.component.html',
  styleUrls: ['./drink-preparing.component.sass']
})
export class DrinkPreparingComponent implements OnInit {

  preparingItems : any = [];

  constructor(private barServices:BarService, private waiterService:WaiterService, private ruter: Router, private messageBar: MessageBarService) { }

  ngOnInit(): void {
    this.loadItems();
  }

  loadItems()
  {
      return this.barServices.getPreparingDrinks().subscribe((data: any) => {
        this.preparingItems = data;
      })

  }

  public doneItem(event: any, item: OrderedItem)
  {
    let OrderedItem = item;
    OrderedItem.orderedItemStatus = "DONE";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      this.messageBar.sendMessage("Pice je spremno.");
      window.location.reload(); 
    });
  }

  public cancelItem(event: any, item: OrderedItem)
  {
    let OrderedItem = item;
    OrderedItem.orderedItemStatus = "CANCELED";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      this.messageBar.sendMessage("Pice je otkazano.");
      window.location.reload(); 
    });
  }
  public goBack(event: any)
  {
    //window.open('/barHomepage',"_self");
    this.ruter.navigate(['/barHomepage']);
  }

}
