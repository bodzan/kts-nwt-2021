import { Component, OnInit } from '@angular/core';
import { BarService } from 'src/app/services/bar-services/bar.service';
import { WaiterService } from 'src/app/services/waiter-services/waiter.service';
import { OrderedItem } from 'src/app/shared/ordered-item';
import { Router } from '@angular/router';
import { MessageBarService } from 'src/app/services/message-bar.service';

@Component({
  selector: 'app-drink-orders',
  templateUrl: './drink-orders.component.html',
  styleUrls: ['./drink-orders.component.sass']
})
export class DrinkOrdersComponent implements OnInit {

  waitingItems : any = [];
  constructor(private barServices:BarService, private waiterService:WaiterService,private ruter: Router, private messageBar: MessageBarService) { }

  ngOnInit(): void {
    this.loadItems();
  }

  loadItems()
  {
      return this.barServices.getWaitingDrinks().subscribe((data: any) => {
        this.waitingItems = data;
      })

  }

  public prepareItem(event: any, item: OrderedItem)
  {
    let OrderedItem = item;
    OrderedItem.orderedItemStatus = "PREPARING";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      this.messageBar.sendMessage("Pice je u pripremi.");
      window.location.reload(); 
    });
  }

  public cancelItem(event: any, item: OrderedItem)
  {
    let OrderedItem = item;
    OrderedItem.orderedItemStatus = "CANCELED";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      this.messageBar.sendMessage("Pice je otkazano.");
      window.location.reload(); 
    });
  }
  public goBack(event: any)
  {
    //window.open('/barHomepage',"_self");
    this.ruter.navigate(['/barHomepage']);
  }
}
