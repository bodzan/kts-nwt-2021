import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-bar-homepage',
  templateUrl: './bar-homepage.component.html',
  styleUrls: ['./bar-homepage.component.sass']
})
export class BarHomepageComponent implements OnInit {

  constructor(private ruter: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  public logOut()
  {
    
    this.authService.logout();
    //this.ruter.navigate(['/']);
    //window.open('/',"_self");
  }

  public waitingOrders(event: any)
  {
    this.ruter.navigate(['bar/orders']);
    //window.open('bar/orders',"_self");
  }

  public preparingOrders(event: any)
  {
    this.ruter.navigate(['bar/prepering']);
    //window.open('bar/prepering',"_self");
  }

}
