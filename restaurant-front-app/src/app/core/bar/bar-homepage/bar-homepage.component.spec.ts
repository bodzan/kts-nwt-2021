import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarHomepageComponent } from './bar-homepage.component';

describe('BarHomepageComponent', () => {
  let component: BarHomepageComponent;
  let fixture: ComponentFixture<BarHomepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarHomepageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
