import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {JwtHelperService} from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  ngOnInit(): void {
  }

  submit(): void {
    const auth: any = {};
    const jwt: JwtHelperService = new JwtHelperService();
    auth.username = this.form.value.username;
    auth.password = this.form.value.password;

    this.authService.login(auth).subscribe(
      result => {
        const token: any = result;
        sessionStorage.setItem('user', token.accessToken);
        sessionStorage.setItem('expiresIn', token.expiresIn);
        const info = jwt.decodeToken(token.accessToken);
        console.log(info);
        if (info.role[0].name === "ROLE_ADMIN") {
          this.router.navigate(['/admin-homepage']);
        } else if (info.role[0].name === "ROLE_MANAGER") {
          this.router.navigate(['/manager-homepage']);
        } else if (info.role[0].name === "ROLE_BARTENDER") {
          this.router.navigate(['/barHomepage']);
        } else if (info.role[0].name === "ROLE_COOK") {
          this.router.navigate(['/cook-homepage']);
        } else if (info.role[0].name === "ROLE_WAITER") {
          this.router.navigate(['/homepage']);
        }

      },
      error => {
        console.log(error);
        // this.toastr.error(error.error);
      }
    );
  }
}
