import { Component, OnInit } from '@angular/core';
import { WaiterService } from 'src/app/services/waiter-services/waiter.service';
import { Item } from 'src/app/shared/item';
import { PriceList } from 'src/app/shared/price-list-item';
import { Order } from 'src/app/shared/order';
import { OrderedItem } from 'src/app/shared/ordered-item';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MessageKonobariService } from 'src/app/services/message-konobari.service';

@Component({
  selector: 'app-waiter',
  templateUrl: './waiter.component.html',
  styleUrls: ['./waiter.component.sass']
})
export class WaiterComponent implements OnInit {

  OpendTableID:any = []; 

  TotalPrice: number = 0;

  Item: any = [];
  ItemAll: any = [];

  isInputHidden: boolean = false;

  // Order for table LIST
  OrdersOfTable: any = [];
  // Actual order
  TableOrder: Order = new Order();

  ToOrderedItemDisplay: Item = new Item();

  noteYes:boolean=false;

  SubCategory: String[] = [];


  quantity: number = 1;

  choosenC: String = "ALL";
  choosenS: String = "ALL";

  ItemPrice: PriceList = new PriceList();

  displayedColumns: string[] = ['Name', 'Price', 'Quantity', 'Status'];
  dataSource: OrderedItem[] = [];

  
  constructor(private waiterService: WaiterService,private route: ActivatedRoute, private ruter: Router,private messageKonobariService : MessageKonobariService) { 
      this.OpendTableID = this.route.snapshot.paramMap.get('idTable');
    }
  
  ngOnInit(): void {
    this.loadItems();
    this.loadOrderOfTable();
  }

  // Get employees list
  loadItems() {
    return this.waiterService.getItems().subscribe((data: any) => {
      this.Item = data;
      this.ItemAll = data;
      for(let i = 0; i<this.Item.length;i++)
      {
        if(!this.SubCategory.includes(this.Item[i].subcategory))
          
          this.SubCategory.push(this.Item[i].subcategory);
      }

    })
  }

  // Tables orders
  loadOrderOfTable() {
    return this.waiterService.getTableActiveOrder(this.OpendTableID).subscribe((data: any) => {
      this.OrdersOfTable = data;

      if(this.OrdersOfTable.length==0)
      {
        this.createOrder();
      }
      else{
        this.TableOrder = this.OrdersOfTable[0];
        this.loadOrderedItems();
      }

    })
  }

  createOrder(){
    return this.waiterService.CreateEmptyOrder(this.OpendTableID).subscribe((data: any) => {
      this.TableOrder = data;
      this.loadOrderedItems();
    })
  }

  loadOrderedItems(){
    return this.waiterService.getOrderedItemsForOrder(this.TableOrder.idOrder).subscribe((data: any) => {
      this.dataSource = data;

      this.loadPrice();
    })
  }

  loadPrice() {
    return this.waiterService.getOrderedItemsForOrderWithPrice(this.dataSource).subscribe((data: any) => {
      this.dataSource = data;
      this.TotalPrice = 0;

      for(let i = 0; i<this.dataSource.length;i++)
      {
        if(this.dataSource[i].orderedItemStatus!='CANCELED')
        this.TotalPrice = this.TotalPrice + this.dataSource[i].price * this.dataSource[i].quantity;
      }

      this.TableOrder.totalPrice = this.TotalPrice;

    })
  }

  loadPriceList(id : number) {
    return this.waiterService.getItemPriceList(id).subscribe((data: any) => {
      this.ItemPrice = data;
    })
  }
  public trackById(index: number, item: Item) {
    return item.idItem;
  }

  selectChangeHandlerC (event: any) {
    //update the ui
    let newItems : any = [];
    this.choosenC = event.target.value;

    if(this.choosenC=="ALL")
    {
      this.loadItems();
      return;
    }
    this.Item = this.ItemAll;

    for(let i = 0; i<this.Item.length;i++)
      {
        if(this.Item[i].category == this.choosenC)
        {

          newItems.push(this.Item[i]);
        }
      }

    this.Item = newItems;

  }

  selectChangeHandlerS (event: any) {

    this.choosenS = event.target.value;
    let newItems : any = [];

    if(this.choosenS=="ALL")
    {
      this.loadItems();
      return;
    }
    this.Item = this.ItemAll;

    for(let i = 0; i<this.Item.length;i++)
      {
        if(this.Item[i].subcategory == this.choosenS)
        {

          newItems.push(this.Item[i]);
        }
      }

    this.Item = newItems;
  }

  public onCardClick(event: any, num: Item){
    this.loadClickedItem(num);
  }

  loadClickedItem( num: Item){
    this.isInputHidden = true;
    this.ToOrderedItemDisplay = num;
  }

  public onEditItem(event: any, idItemOrdered: number)
  {
    //window.open('waiter/edit/'+this.OpendTableID+'/'+idItemOrdered,"_self"); // etc

    this.ruter.navigate(['waiter/edit/'+this.OpendTableID+'/'+idItemOrdered]);
  }

  public changeNote(event: any)
  {
    this.noteYes = false;
    this.TableOrder.note = event.target.value;
  }

  public changeQuantity(event: any)
  {
    this.quantity= event.target.value;
  }

  public cancelItem(event: any, item: OrderedItem)
  {
    let OrderedItem = item;
    OrderedItem.orderedItemStatus = "CANCELED";
    this.waiterService.updateOrderedItems(OrderedItem).subscribe((data: any) => {
      console.log(data);
      if(data.menuItem.category == "DISH"){
        this.messageKonobariService.sendMessage("Jelo je otkazano.");
      }
      if(data.menuItem.category == "DRINK"){
        this.messageKonobariService.sendMessageBar("Pice je otkazano.");
      }
      window.location.reload(); 
    });
    }

  public addItemToOrder(event: any){
    if(this.quantity>=1 && this.quantity <=100 && this.quantity%1==0)
    {
      this.waiterService.orderItem(this.ToOrderedItemDisplay,this.TableOrder.idOrder,this.quantity).subscribe((data: any) => {
        console.log(data);
        if(data.menuItem.category == "DISH"){
          this.messageKonobariService.sendMessage("Novo jelo je dodato na cekanje.");
        }
        if(data.menuItem.category == "DRINK"){
          this.messageKonobariService.sendMessageBar("Novo pice je dodato na cekanje");
        }

        window.location.reload();
      })
    }
    else
    {
    alert("Error! Quantity wrong!");
    }

  }
  public submitNote(event: any)
  {
    return this.waiterService.updateOrder(this.TableOrder).subscribe((data: any) => {
      this.TableOrder = data;
      this.noteYes = true;
    })
  }

  public cancelALL(event: any)
  {
    this.waiterService.cancelOrderAndItems(this.TableOrder.idOrder).subscribe((data: any) => {
      this.TableOrder = data;
      this.messageKonobariService.sendMessageBar("Otkazana je porudzbina.");
      this.messageKonobariService.sendMessage("Otkazana je porudzbina.");
      this.ruter.navigate(['/homepage']);
      //window.open('/homepage',"_self");
    })
  }

  public goBack(event: any)
  {
    this.ruter.navigate(['/homepage']);
    //window.open('/homepage',"_self");
  }

  public finish(event: any)
  {
    this.waiterService.finishOrder(this.TableOrder.idOrder).subscribe((data: any) => {
      console.log(data);
      this.ruter.navigate(['/homepage']);
      //window.open('/homepage',"_self");
    })
  }

}

