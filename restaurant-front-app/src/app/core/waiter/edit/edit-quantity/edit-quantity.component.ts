import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WaiterService } from 'src/app/services/waiter-services/waiter.service';
import { OrderedItem } from 'src/app/shared/ordered-item';
import { Router } from '@angular/router';
import { MessageKonobariService } from 'src/app/services/message-konobari.service';

@Component({
  selector: 'app-edit-quantity',
  templateUrl: './edit-quantity.component.html',
  styleUrls: ['./edit-quantity.component.sass']
})
export class EditQuantityComponent implements OnInit {

  quantity: number = 1;
  OrderedItem: OrderedItem = new OrderedItem();
  OpendTableID: any = [];
  OpendOrderedItemID: any = [];
  constructor(private route: ActivatedRoute,private waiterService: WaiterService,private ruter: Router, private messageKonobariService: MessageKonobariService) { 
    this.OpendTableID = this.route.snapshot.paramMap.get('idTable');
    this.OpendOrderedItemID = this.route.snapshot.paramMap.get('idOrderItem');
  }

  ngOnInit(): void {
    this.loadOrderedItem();
  }

  loadOrderedItem(){
    return this.waiterService.getOrderedItemsByID(this.OpendOrderedItemID).subscribe((data: any) => {
      this.OrderedItem = data;
      this.quantity = this.OrderedItem.quantity;
    })
  }
  saveChanges()
  {
    this.waiterService.updateOrderedItems(this.OrderedItem).subscribe((data: any) => {
      this.OrderedItem = data;
      if(data.menuItem.category == "DISH"){
        this.messageKonobariService.sendMessage("Promenjena kolicina jela.");
      }
      if(data.menuItem.category == "DRINK"){
        this.messageKonobariService.sendMessageBar("Promenjen broj pica.");
      }
      //window.open('waiter/'+this.OpendTableID,"_self");
      this.ruter.navigate(['waiter/'+this.OpendTableID]);
    })
  }

  public changeFn(event: any)
  {
    this.quantity = event.target.value;
  }

  public saveIt(event: any)
  {
    if(this.quantity >= 1 && this.quantity <=100 && this.quantity%1==0)
    {
      this.OrderedItem.quantity=this.quantity;
      this.saveChanges();
    }
    else
    {
      alert("Greska! Neispravan broj!");
    }
    
  }
  public cancelIt(event: any)
  {
    //window.open('waiter/'+this.OpendTableID,"_self");
    this.ruter.navigate(['waiter/'+this.OpendTableID]);
  }

}
