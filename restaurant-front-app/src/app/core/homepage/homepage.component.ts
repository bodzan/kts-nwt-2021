import { Component, OnInit } from '@angular/core';
import {AdminService} from "../../services/admin.service";
import {TableDto} from "../../model/table-dto";
import {OrdersService} from "../../services/orders.service";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.sass']
})
export class HomepageComponent implements OnInit {

  tables: any;
  orders: any[];
  layoutId: number;

  constructor(
    private adminService: AdminService,
    private orderService: OrdersService,
    private router: Router,
    private auth: AuthService
  ) {
    this.layoutId = -1;
    this.tables = [];
    this.orders = [];
  }

  ngOnInit(): void {
    this.adminService.getActiveLayout().subscribe(data => {
      // this.layoutId = data.id;
      let tableArray = data.coordinates.split(';');
      for (let i = 0; i < data.tables.length; i++) {
        let tableCoords = tableArray[i].split(',');
        this.tables.push(new TableDto(Number(tableCoords[0]), tableCoords[1], tableCoords[2], data.tables[i].restaurantRoom));
      }

    });

    // this.orderService.getActiveOrdersForTable().subscribe( data => {
    //
    // });

  }

  redirectToTable(selectedTableId: number): void {
    this.router.navigate(['waiter/' + selectedTableId])
  }

  logout(): void {
    this.auth.logout();
  }
}
