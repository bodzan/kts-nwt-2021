import { Component, OnInit } from '@angular/core';
import { CookService } from 'src/app/services/cook.service';
import { Item } from 'src/app/shared/item';
import { OrderedItem } from 'src/app/shared/ordered-item';
import { MessageServiceService } from 'src/app/services/message-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-cook',
  templateUrl: './cook.component.html',
  styleUrls: ['./cook.component.sass']
})
export class CookComponent implements OnInit {

  Item: any = [];
  SubCategory: String[] = [];
  Order: any = [];

  WaitingDishes: any = [];
  PreparingDishes: any = [];
  DoneDishes: any = [];


  constructor(private cookService: CookService, private messageService: MessageServiceService, private authService: AuthService) { }

  ngOnInit(): void {
    this.loadWaitingDishes();
    this.loadPreparingDishes();
  }

  // Ucitava sve koji se spremaju, treba filtrirati da ucitava samo one od ulogovanog kuvara
  loadPreparingDishes() {
    return this.cookService.getDishesInPreparation().subscribe((data: any) => {
      this.Order = data;

      for(let i = 0; i<this.Order.length;i++)
      {
        this.PreparingDishes.push(this.Order[i]);
      }

    })
  } 

  loadWaitingDishes() {
    return this.cookService.getDishesWaiting().subscribe((data: any) => {
      this.Order = data;

      for(let i = 0; i<this.Order.length;i++)
      {
        this.WaitingDishes.push(this.Order[i]);
      }
      console.log(this.WaitingDishes);
    })
  }
  
  public startPreparing(oi: OrderedItem) {
    
    let newItem = oi;
    newItem.orderedItemStatus = "PREPARING";
    console.log("=========");
    console.log(newItem);
    console.log("=========");

    this.cookService.updateDishStatus(newItem).subscribe((data: any) => {

      this.messageService.sendMessage("Jelo " + newItem.menuItem.itemName + " se sprema.");
      const index = this.WaitingDishes.indexOf(newItem);
      this.WaitingDishes.splice(index, 1);

    })
  }

  public donePreparing(oi: OrderedItem) {

    let newItem = oi;
    newItem.orderedItemStatus = "DONE";

    return this.cookService.updateDishStatus(newItem).subscribe((data: any) => {

      this.messageService.sendMessage("Jelo " + newItem.menuItem.itemName + " je spremno.");
      const index = this.PreparingDishes.indexOf(newItem);
      this.PreparingDishes.splice(index, 1);
    })
  }


  //load all items
  loadItems() {
    return this.cookService.getItems().subscribe((data: any) => {
      this.Item = data;

      for(let i = 0; i<this.Item.length;i++)
      {
        if(!this.SubCategory.includes(this.Item[i].subcategory))
          
          this.SubCategory.push(this.Item[i].subcategory);
      }


    })
  }


  // get all ordered items and sort them into 3 groups
  loadOrderedItems() {
    return this.cookService.getOrderedItems().subscribe((data: any) => {
      this.Order = data;

      for(let i = 0; i<this.Order.length;i++)
      {
        if(!this.SubCategory.includes(this.Item[i].subcategory))
          
          this.SubCategory.push(this.Item[i].subcategory);
      }


    })
  }

  public logout(){
    this.authService.logout();
  }


}
