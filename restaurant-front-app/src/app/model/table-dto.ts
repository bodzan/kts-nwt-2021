import {toNumbers} from "@angular/compiler-cli/src/diagnostics/typescript_version";

export class TableDto {
  private _id: number;
  private _x: number;
  private _y: number;
  private _room: string

  constructor(id: any, x:any, y:any, room:any) {
    this._x = x;
    this._y = y;
    this._id = id;
    this._room = room;
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get x(): number {
    return this._x;
  }

  set x(value: number) {
    this._x = value;
  }

  get y(): number {
    return this._y;
  }

  set y(value: number) {
    this._y = value;
  }

  get room(): string {
    return this._room;
  }

  set room(value: string) {
    this._room = value;
  }
}
