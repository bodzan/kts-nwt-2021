package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ManagerHomepage {
    private WebDriver driver;

    @FindBy(id = "username-field")
    private WebElement username;

    @FindBy(id = "firstname-field")
    private WebElement firstname;

    @FindBy(id = "lastname-field")
    private WebElement lastname;

    @FindBy(id = "salary-field")
    private WebElement salary;

    @FindBy(id = "role-field")
    private WebElement role;

    @FindBy(id = "mat-tab-label-0-1")
    private WebElement addUserTab;

    @FindBy(id = "manage-users")
    private WebElement manageUsersBtn;

    @FindBy(id = "add-user-btn")
    private WebElement addUserButton;

    @FindBy(id = "mat-option-1")
    private WebElement managerOption;

    public ManagerHomepage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getUsernameInput() {
        return Utilities.visibilityWait(driver, this.username, 10);
    }

    public WebElement getSalaryInput() {
        return Utilities.visibilityWait(driver, this.salary, 10);
    }

    public WebElement getFirstnameInput() {
        return Utilities.visibilityWait(driver, this.firstname, 10);
    }

    public WebElement getLastnameInput() {
        return Utilities.visibilityWait(driver, this.lastname, 10);
    }

    public WebElement getRoleInput() {
        return Utilities.visibilityWait(driver, this.role, 10);
    }

    public WebElement getTab() {
        return Utilities.visibilityWait(driver, this.addUserTab, 10);
    }

    public void clickButton() {
        WebElement dugme = Utilities.clickableWait(driver, this.addUserButton, 10);
        dugme.click();
    }

    public void clickManageUsersButton() {
        WebElement dugme = Utilities.clickableWait(driver, this.manageUsersBtn, 10);
        dugme.click();
    }

    public void clickTab() {
        WebElement dugme = Utilities.clickableWait(driver, this.addUserTab, 10);
        dugme.click();
    }

    public void clickOption() {
        WebElement dugme = Utilities.clickableWait(driver, this.managerOption, 10);
        dugme.click();
    }

    public void setUserName() {
        WebElement polje = getUsernameInput();
        polje.click();
        polje.clear();
        polje.sendKeys("novsam");
    }

    public void setFirstname() {
        WebElement polje = getFirstnameInput();
        polje.click();
        polje.clear();
        polje.sendKeys("Nikola");
    }

    public void setLastname() {
        WebElement polje = getLastnameInput();
        polje.click();
        polje.clear();
        polje.sendKeys("Nikola");
    }

    public void setSalary() {
        WebElement polje = getSalaryInput();
//        polje.click();
        polje.clear();
        polje.sendKeys("123000");
    }

    public void setRole() {
        WebElement polje = getRoleInput();
        polje.click();
    }
}
