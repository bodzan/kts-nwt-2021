package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CookPage {
	private WebDriver driver;
	
	@FindBy(id = "LogoutButton")
	private WebElement logoutButton;
	
	@FindBy(id = "PrepareButton0")
	private WebElement prepareButton0;
	
	@FindBy(id = "FinishButton0")
	private WebElement finishButton0;
	
	@FindBy(id = "mat-tab-label-0-0")
	private WebElement waitingTab;
	
	@FindBy(id = "mat-tab-label-0-1")
	private WebElement inpreparationTab;
	
	public CookPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void logoutbtnClick() {
		Utilities.clickableWait(driver, this.logoutButton, 10).click();
	}
	
	public void preparebtn0Click() {
		Utilities.clickableWait(driver, this.prepareButton0, 10).click();
	}
	
	public void finishbtn0Click() {
		Utilities.clickableWait(driver, this.finishButton0, 10).click();
	}
	
	public void inpreparationTabClick() {
		Utilities.clickableWait(driver, this.inpreparationTab, 10).click();
	}
	
	public void waitingTabClick() {
		Utilities.clickableWait(driver, this.waitingTab, 10).click();
	}

}
