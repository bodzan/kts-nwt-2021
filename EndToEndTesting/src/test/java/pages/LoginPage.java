package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
    private WebDriver driver;

    @FindBy(id = "username")
    private WebElement username;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "login-btn")
    private WebElement loginBtn;

    public WebElement getUsernameInput() {
        return Utilities.visibilityWait(driver, this.username, 10);
    }

    public WebElement getPasswordInput() {
        return Utilities.visibilityWait(driver, this.password, 10);
    }

    public void clickLoginButton() {
        WebElement dugme = Utilities.clickableWait(driver, this.loginBtn, 10);
        dugme.click();
    }

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }


    public void setUserName() {
        WebElement usernamePolje = getUsernameInput();
        usernamePolje.click();
        usernamePolje.clear();
        usernamePolje.sendKeys("admin");
    }

    public void setPassword() {
        WebElement passwordPolje = getPasswordInput();
        passwordPolje.click();
        passwordPolje.clear();
        passwordPolje.sendKeys("sifra123");
    }
    
    public void setUsernameCook() {
    	WebElement usernamePolje = getUsernameInput();
        usernamePolje.click();
        usernamePolje.clear();
        usernamePolje.sendKeys("kuvar");
    }
    
    public void setUsernameWaiter() {
    	WebElement usernamePolje = getUsernameInput();
        usernamePolje.click();
        usernamePolje.clear();
        usernamePolje.sendKeys("konobar");
    }

    public void setManagerUserName() {
        WebElement usernamePolje = getUsernameInput();
        usernamePolje.click();
        usernamePolje.clear();
        usernamePolje.sendKeys("menadzer");
    }

}
