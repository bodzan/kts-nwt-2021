package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminHomepage {

    private WebDriver driver;

    @FindBy(id = "create-layout")
    private WebElement createLayoutBtn;

    @FindBy(id = "edit-layout")
    private WebElement editLayoutBtn;

    @FindBy(id = "view-layout")
    private WebElement viewLayoutsBtn;

    @FindBy(id = "logout")
    private WebElement logoutBtn;

    public void clickLogoutButton() {
        WebElement dugme = Utilities.clickableWait(driver, this.logoutBtn, 10);
        dugme.click();
    }



}