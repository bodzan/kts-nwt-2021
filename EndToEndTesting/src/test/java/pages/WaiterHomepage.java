package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WaiterHomepage {
	private WebDriver driver;
	
	@FindBy(id = "logoutButton")
	private WebElement logoutButton;
	
	@FindBy(id = "1")
	private WebElement table1;
	
	@FindBy(id = "card1")
	private WebElement piletinaButton;
	
	@FindBy(name = "Add")
	private WebElement addButton;
	
	@FindBy(id = "XButton1")
	private WebElement cancelButton1;
	
	@FindBy(id = "finBtn")
	private WebElement finishButton;
	
	public WaiterHomepage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void logoutbtnClick() {
		Utilities.clickableWait(driver, this.logoutButton, 10).click();
	}
	
	public void table1Click() {
		Utilities.clickableWait(driver, this.table1, 10).click();
	}
	
	public void piletinaClick() {
		Utilities.clickableWait(driver, this.piletinaButton, 10).click();
	}
	
	public void addClick() {
		Utilities.clickableWait(driver, this.addButton, 10).click();
	}
	
	public void cancel1Click() {
		Utilities.clickableWait(driver, this.cancelButton1, 10).click();
	}
	
	public void finishClick() {
		Utilities.clickableWait(driver, this.finishButton, 10).click();
	}

}
