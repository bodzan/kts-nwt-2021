package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import pages.LoginPage;
import pages.CookPage;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class KitchenTest {
	
	private WebDriver browser;

    private LoginPage loginPage;
    private CookPage cookPage;

    @Before
    public void setupSelenium() {
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200/");


        loginPage = PageFactory.initElements(browser, LoginPage.class);
        cookPage = PageFactory.initElements(browser, CookPage.class);
    }

    @Test
    public void KitchenTest() throws InterruptedException {
    	
    	//LOGIN TEST
        loginPage.setUsernameCook();
        loginPage.setPassword();

        loginPage.clickLoginButton();
        FluentWait<WebDriver> wait = new FluentWait(browser);
        wait.withTimeout(5, TimeUnit.SECONDS);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.urlToBe("http://localhost:4200/cook-homepage"));

        assertEquals("http://localhost:4200/cook-homepage", browser.getCurrentUrl());
        
        //Start preparing first item on the list
        cookPage.preparebtn0Click();
        browser.navigate().refresh();
        // provera
        // Otvoriti tab In Preparation
        cookPage.inpreparationTabClick();
        // Zavrsiti jelo
        cookPage.finishbtn0Click();
        //provera
        
        
        //LOGOUT TEST
        cookPage.logoutbtnClick();
        assertEquals("http://localhost:4200/", browser.getCurrentUrl());
    }

    @After
    public void tearDown() throws Exception {
        browser.quit();
    }

}
