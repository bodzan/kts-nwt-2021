package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import pages.LoginPage;
import pages.ManagerHomepage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ManagerTests {

    private WebDriver browser;

    private LoginPage loginPage;

    private ManagerHomepage managerHomepage;

    @Before
    public void setupSelenium() {
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        // maximize window
//        browser.manage().window().maximize();
        // navigate
        browser.navigate().to("http://localhost:4200/");


        loginPage = PageFactory.initElements(browser, LoginPage.class);
        managerHomepage = PageFactory.initElements( browser, ManagerHomepage.class);
    }

    @Test
    public void successfulLoginTest() throws InterruptedException {
        loginPage.setManagerUserName();
        loginPage.setPassword();

        loginPage.clickLoginButton();
        FluentWait<WebDriver> wait = new FluentWait(browser);
        wait.withTimeout(5, TimeUnit.SECONDS);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.urlToBe("http://localhost:4200/manager-homepage"));

        assertEquals("http://localhost:4200/manager-homepage", browser.getCurrentUrl());

        managerHomepage.clickManageUsersButton();
        managerHomepage.clickTab();
        managerHomepage.setFirstname();
        managerHomepage.setLastname();
        managerHomepage.setUserName();
        managerHomepage.setSalary();
        managerHomepage.setRole();
        managerHomepage.clickOption();
        managerHomepage.clickButton();
    }

    @After
    public void tearDown() throws Exception {
//        browser.quit();
    }
}
