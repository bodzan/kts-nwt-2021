package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import pages.LoginPage;
import pages.WaiterHomepage;
import pages.CookPage;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WaiterTest {
	
	private WebDriver browser;

    private LoginPage loginPage;
    
    private WaiterHomepage waiterHomepage;

    @Before
    public void setupSelenium() {
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        browser.manage().window().maximize();
        browser.navigate().to("http://localhost:4200/");


        loginPage = PageFactory.initElements(browser, LoginPage.class);
        waiterHomepage = PageFactory.initElements(browser, WaiterHomepage.class);
    }
    
    @Test
    public void KitchenTest() throws InterruptedException {
    	
    	//LOGIN TEST
        loginPage.setUsernameWaiter();
        loginPage.setPassword();

        loginPage.clickLoginButton();
        FluentWait<WebDriver> wait = new FluentWait(browser);
        wait.withTimeout(5, TimeUnit.SECONDS);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.urlToBe("http://localhost:4200/homepage"));

        assertEquals("http://localhost:4200/homepage", browser.getCurrentUrl());
        
        //klikni na table 1 (T1) = mora preko koordinata
//        waiterHomepage.table1Click();
//        assertEquals("http://localhost:4200/waiter/1", browser.getCurrentUrl());
        
        // Odlazak na sto T1
        browser.navigate().to("http://localhost:4200/waiter/1");
        
        //Dodaj novo jelo
        waiterHomepage.piletinaClick();
        waiterHomepage.addClick();
        
        // izbaci stavku 1
//        waiterHomepage.cancel1Click();
        
        // zavrsi porudzbinu
//        waiterHomepage.finishClick();
        
        //LOGOUT TEST
//        waiterHomepage.logoutbtnClick();
//        assertEquals("http://localhost:4200/", browser.getCurrentUrl());
    }

    @After
    public void tearDown() throws Exception {
        browser.quit();
    }

}
