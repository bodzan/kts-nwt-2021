package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginTest {

    private WebDriver browser;

    private LoginPage loginPage;

    @Before
    public void setupSelenium() {
        // instantiate browser
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        browser = new ChromeDriver();
        // maximize window
//        browser.manage().window().maximize();
        // navigate
        browser.navigate().to("http://localhost:4200/");


        loginPage = PageFactory.initElements(browser, LoginPage.class);
    }

    @Test
    public void successfulLoginTest() throws InterruptedException {
        loginPage.setUserName();
        loginPage.setPassword();

        loginPage.clickLoginButton();
        FluentWait<WebDriver> wait = new FluentWait(browser);
        wait.withTimeout(5, TimeUnit.SECONDS);
        wait.pollingEvery(1, TimeUnit.SECONDS);
        wait.until(ExpectedConditions.urlToBe("http://localhost:4200/admin-homepage"));

        assertEquals("http://localhost:4200/admin-homepage", browser.getCurrentUrl());
    }

    @After
    public void tearDown() throws Exception {
//        browser.quit();
    }
}
