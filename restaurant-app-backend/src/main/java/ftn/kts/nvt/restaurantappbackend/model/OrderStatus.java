package ftn.kts.nvt.restaurantappbackend.model;

public enum OrderStatus {
    WAITING, CANCELED, PREPARING, DONE, FINISHED
}
