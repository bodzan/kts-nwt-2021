package ftn.kts.nvt.restaurantappbackend.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="orderTabela")
public class Order {
	@Id()
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long idOrder;
	
	@Column(name="dateAndTime", nullable= false)
	private LocalDateTime dateAndTime;
	
	@Column(name="note", nullable=false)
	private String note;
	
	@Enumerated(EnumType.STRING)
	//@Column(name = "status")
	private OrderStatus status;
	
	@Column(name="totalPrice", nullable= true)
	private Double totalPrice;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Tables deliverForTable;
	
	//Da li je order zavrsen bitna informacija ???????????? 
	//kako bi se znalo koji order je aktivan a koji nije.
	
	
	@OneToMany(mappedBy = "order", fetch= FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OrderedItem> orderedItemsList = new ArrayList<OrderedItem>();
	
	
	public Order() {
		
	}


	public Long getIdOrder() {
		return idOrder;
	}


	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}


	public LocalDateTime getDateAndTime() {
		return dateAndTime;
	}


	public void setDateAndTime(LocalDateTime dateAndTime) {
		this.dateAndTime = dateAndTime;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public OrderStatus getStatus() {
		return status;
	}


	public void setStatus(OrderStatus status) {
		this.status = status;
	}


	public Double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}


	public Tables getDeliverForTable() {
		return deliverForTable;
	}
	

	public void setDeliverForTable(Tables deliverForTable) {
		this.deliverForTable = deliverForTable;
	}


	public List<OrderedItem> getOrderedItemsList() {
		return orderedItemsList;
	}


	public void setOrderedItemsList(List<OrderedItem> orderedItemsList) {
		this.orderedItemsList = orderedItemsList;
	}


	@Override
	public String toString() {
		return "Order [idOrder=" + idOrder + ", dateAndTime=" + dateAndTime + ", note=" + note + ", status=" + status
				+ ", totalPrice=" + totalPrice + ", deliverForTable=" + deliverForTable + ", orderedItemsList="
				+ orderedItemsList + "]";
	}


	
}
