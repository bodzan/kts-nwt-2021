package ftn.kts.nvt.restaurantappbackend.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="stoTabela")
@AllArgsConstructor
public class Tables {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  // baza sama vodi racuna o generisanju vrednosti id-ija
	private Long idTable;
	
	@Enumerated(EnumType.STRING)
	//@Column(name="restaurantRoom")
	private Room restaurantRoom;
	
	@OneToMany(mappedBy="deliverForTable", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Order> orders = new ArrayList<Order>();
	
	@Column(name="isActive", nullable = false)
	private boolean isActive;

	
	public Tables() {
		
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}

	public Room getRestaurantRoom() {
		return restaurantRoom;
	}

	public void setRestaurantRoom(Room restaurantRoom) {
		this.restaurantRoom = restaurantRoom;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	@Override
	public String toString() {
		return "Tables [idTable=" + idTable + ", restaurantRoom=" + restaurantRoom + ", orders=" + orders
				+ ", isActive=" + isActive + "]";
	}
	
	

	
}
