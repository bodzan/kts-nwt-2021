package ftn.kts.nvt.restaurantappbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import ftn.kts.nvt.restaurantappbackend.dto.MessageDTO;
import ftn.kts.nvt.restaurantappbackend.dto.ResponseMessageDTO;

@Controller
public class WebSocketController {

    // @Autowired
    // private NotificationService notificationService;

    @MessageMapping("/kuhinja")
    @SendTo("/topic/kuhinja")
    public ResponseMessageDTO getNewDishMessage(final String message) throws InterruptedException {
        // notificationService.sendGlobalNotification();
        return new ResponseMessageDTO(HtmlUtils.htmlEscape(message));
    }

    @MessageMapping("/bar")
    @SendTo("/topic/bar")
    public ResponseMessageDTO getNewDrinkMessage(final String message) throws InterruptedException {
        // notificationService.sendGlobalNotification();
        return new ResponseMessageDTO(HtmlUtils.htmlEscape(message));
    }

    @MessageMapping("/konobari")
    @SendTo("/topic/konobari")
    public ResponseMessageDTO getOrderReadyMessage(final String message) throws InterruptedException {
        // notificationService.sendGlobalNotification();
        return new ResponseMessageDTO(HtmlUtils.htmlEscape(message));
    }
    
    
    
}
