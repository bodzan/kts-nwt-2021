package ftn.kts.nvt.restaurantappbackend.model;

public enum Subcategory {
	HOT_DRINKS, NON_ALCOHOLIC_DRINKS, WATERS, BEER, VINE, STRONG_DRINKS, APPETIZERS, SOUPS, GRILL, MEALS_TO_ORDER, DESERTS, SALATS;

}
