package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.SalaryChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalaryChangeRepository extends JpaRepository<SalaryChange, Long> {
}
