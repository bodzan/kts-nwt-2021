package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.ItemDTO;
import ftn.kts.nvt.restaurantappbackend.dto.OrderDTO;
import ftn.kts.nvt.restaurantappbackend.dto.OrderedItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import ftn.kts.nvt.restaurantappbackend.repository.BartenderRepository;
import ftn.kts.nvt.restaurantappbackend.repository.OrderedItemRepository;
import ftn.kts.nvt.restaurantappbackend.service.BartenderService;
import ftn.kts.nvt.restaurantappbackend.service.OrderService;
import ftn.kts.nvt.restaurantappbackend.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/bartender")
public class BartenderController {
	
	@Autowired
	private BartenderService bartenderService;

	@RequestMapping(value = "/allWaitingItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ItemDTO> getAllItems(){
		List<Item> listItems = bartenderService.showAllDrinksToAccept();

		//sve te objekte iz baze prebacujemo u dto objekte
		List<ItemDTO> listItemsDto = new ArrayList<>();

		for(Item i : listItems) {
			ItemDTO itemDto = new ItemDTO(i);
			listItemsDto.add(itemDto);
		}
		return listItemsDto;
	}

	@RequestMapping(value = "/allWaitingDrinks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OrderedItemDTO> getAllWaitingDrinks(){
		List<OrderedItem> listItems = bartenderService.allOrderedDrinksWaiting();

		//sve te objekte iz baze prebacujemo u dto objekte
		List<OrderedItemDTO> listItemsDto = new ArrayList<OrderedItemDTO>();

		for(OrderedItem i : listItems) {
			OrderedItemDTO itemDto = new OrderedItemDTO(i);
			listItemsDto.add(itemDto);
		}
		return listItemsDto;
	}

	@RequestMapping(value = "/allPreparingDrinks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OrderedItemDTO> getAllPreparingDrinks(){
		List<OrderedItem> listItems = bartenderService.allOrderedDrinksPreparing();

		//sve te objekte iz baze prebacujemo u dto objekte
		List<OrderedItemDTO> listItemsDto = new ArrayList<OrderedItemDTO>();

		for(OrderedItem i : listItems) {
			OrderedItemDTO itemDto = new OrderedItemDTO(i);
			listItemsDto.add(itemDto);
		}
		return listItemsDto;
	}

	
}
