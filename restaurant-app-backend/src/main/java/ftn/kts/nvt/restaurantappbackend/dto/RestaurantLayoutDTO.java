package ftn.kts.nvt.restaurantappbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RestaurantLayoutDTO {

    private Long id;
    private boolean active;
    private String coordinates;
    private List<TableLayoutDTO> tables;
}
