package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.RestaurantLayout;
import ftn.kts.nvt.restaurantappbackend.repository.RestaurantLayoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RestaurantLayoutService {

    @Autowired
    public RestaurantLayoutRepository restaurantLayoutRepository;


    
    public RestaurantLayout addNewLayout(RestaurantLayout layout) {
        return restaurantLayoutRepository.save(layout);
    }

    public RestaurantLayout getActiveLayout() {
        return restaurantLayoutRepository.findByActiveIsTrue();
    }

    public List<RestaurantLayout> getAllLayouts() {
        return restaurantLayoutRepository.findAll();
    }

    public Optional<RestaurantLayout> getById(Long id) {
        return restaurantLayoutRepository.findById(id);
    }

    public RestaurantLayout saveLayout(RestaurantLayout restaurantLayout) {
        return restaurantLayoutRepository.save(restaurantLayout);
    }

}
