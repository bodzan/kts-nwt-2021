package ftn.kts.nvt.restaurantappbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UpdateSalaryDTO {
    private Long userId;
    private double newSalary;
}
