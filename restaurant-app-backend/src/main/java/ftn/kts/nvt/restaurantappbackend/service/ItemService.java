package ftn.kts.nvt.restaurantappbackend.service;


import java.util.List;
import java.util.Optional;

import ftn.kts.nvt.restaurantappbackend.model.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;
	
	/*@Autowired
	public void setItemRepository(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}*/
	
	public List<Item> findAll(){
		return itemRepository.findAll();
	}
	
	public Item findOneById(Long idItem) {
		return itemRepository.findOneByIdItem(idItem);
	}
	
	public Item save(Item item) {
		return itemRepository.save(item);
	}
	
	public void remove(Long idItem) {
		itemRepository.deleteById(idItem);
	}

	public Optional<Item> findById(Long idItem){ return itemRepository.findById(idItem); }

	public boolean logicalRemove(Long idItem){
		Optional<Item> existingItem = itemRepository.findById(idItem);
		if (existingItem.isEmpty()) {
			return false;
		}
		Item i = existingItem.get();
		i.setActive(false);
		itemRepository.save(i);
		return true;
	}

	public boolean update(Item itemUpdate, Long id){
		Optional<Item> found = this.itemRepository.findById(id);

		if(found.isPresent()){
			Item foundItem = found.get();

			foundItem.setActive(itemUpdate.isActive());
			foundItem.setAlergenList(itemUpdate.getAlergenList());
			foundItem.setCategory(itemUpdate.getCategory());
			foundItem.setDescription(itemUpdate.getDescription());
			foundItem.setSubcategory(itemUpdate.getSubcategory());
			foundItem.setName(itemUpdate.getName());
			foundItem.setOrderedItemList(itemUpdate.getOrderedItemList());
			foundItem.setPriceListOfItems(itemUpdate.getPriceListOfItems());
			itemRepository.save(foundItem); //sacuvamo updatovane promene
			return true;
		}
		return false;
	}
}
