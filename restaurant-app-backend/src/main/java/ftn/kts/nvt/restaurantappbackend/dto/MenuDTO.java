package ftn.kts.nvt.restaurantappbackend.dto;

import ftn.kts.nvt.restaurantappbackend.model.Menu;

public class MenuDTO {
	private Long idMenu;
	private boolean isActive;
	
	public MenuDTO() {
		
	}
	
	public MenuDTO(Menu m) {
		this.idMenu = m.getIdMenu();
		this.isActive = m.isActive();
	}

	public MenuDTO(Long id, boolean isActive) {
		super();
		this.idMenu = id;
		this.isActive = isActive;
	}
	
	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
} 
