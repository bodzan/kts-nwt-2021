package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.AuthTokenDTO;
import ftn.kts.nvt.restaurantappbackend.dto.LoginDTO;
import ftn.kts.nvt.restaurantappbackend.dto.PasswordChangeDTO;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.security.AuthTokenUtils;
import ftn.kts.nvt.restaurantappbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    @Autowired
    public UserService userService;

    @Autowired
    private AuthTokenUtils tokenUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostMapping(value = "/login")
    public ResponseEntity<AuthTokenDTO> login(@Valid @RequestBody LoginDTO loginDTO, BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
//            if (loginDTO.getPin() != null) {
//
//                Authentication authentication = authenticationManager
//                        .authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPin()));
//
//                // Ubaci korisnika u trenutni security kontekst
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//
//                // Kreiraj token za tog korisnika
//                User user = (User) authentication.getPrincipal();
//                String jwt = tokenUtils.generateToken(user);
//                int expiresIn = tokenUtils.getExpiredIn();
//                return new ResponseEntity<>(new AuthTokenDTO(jwt, expiresIn), HttpStatus.OK);
//            }
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));

            // Ubaci korisnika u trenutni security kontekst
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Kreiraj token za tog korisnika
            User user = (User) authentication.getPrincipal();
            String jwt = tokenUtils.generateToken(user);
            int expiresIn = tokenUtils.getExpiredIn();
            return new ResponseEntity<>(new AuthTokenDTO(jwt, expiresIn), HttpStatus.OK);
        }


    }

//    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
//    public ResponseEntity<?> changePassword(@RequestBody PasswordChangeDTO passwordChangeDTO) {
//        userService.changePassword(passwordChangeDTO.getOldPassword(), passwordChangeDTO.getNewPassword());
//
//        Map<String, String> result = new HashMap<>();
//        result.put("result", "success");
//        result.put("message", "Password changed successfully!");
//        return ResponseEntity.accepted().body(result);
//    }

}
