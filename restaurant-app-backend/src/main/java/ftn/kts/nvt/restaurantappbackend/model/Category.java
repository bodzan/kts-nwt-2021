package ftn.kts.nvt.restaurantappbackend.model;

public enum Category {
	DISH, DRINK;
}
