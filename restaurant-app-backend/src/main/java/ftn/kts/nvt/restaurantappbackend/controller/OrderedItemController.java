package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.OrderedItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import ftn.kts.nvt.restaurantappbackend.service.OrderService;
import ftn.kts.nvt.restaurantappbackend.service.OrderedItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("/api/orderedItem")
public class OrderedItemController {

    @Autowired
    private OrderedItemService orderedItemService;

    @Autowired
    private OrderService orderService;

    /*
     * PREGLED SVIH NARUCENIH STAVKI
     */
    @RequestMapping(value="/getAllOrderedItem", method = RequestMethod.GET)
    public ResponseEntity<List<OrderedItemDTO>> getAllOrderedItem(){

        //trazimo sve narucene stavke iz menija
        List<OrderedItem> orderedItems = orderedItemService.findAll();

        List<OrderedItemDTO> orderedItemsDTO = new ArrayList<>();

        for(OrderedItem o: orderedItems) {
            orderedItemsDTO.add(new OrderedItemDTO(o));
        }
        return new ResponseEntity<>(orderedItemsDTO, HttpStatus.OK);
    }

    /*
     * PREGLED SVIH NARUCENIH STAVKI ZA NEKI ORDER PO NJEGOVOM ID-IJU
     */
    @RequestMapping(value="/getOrderedItemById/{idOrder}", method = RequestMethod.GET)
    public ResponseEntity<List<OrderedItemDTO>> getOrderedItemByOrderId(@PathVariable Long idOrder){

        //trazimo order sa tim id-ijem
        Order order = orderService.findOneById(idOrder);


        //trazimo u bazi narucene proizvode koji imaju u sebi id od ovog ordera
        List<OrderedItem> orderedItems = orderedItemService.findByOrderId(order.getIdOrder());

        List<OrderedItemDTO> orderedItemsDTO = new ArrayList<>();

        for(OrderedItem o: orderedItems) {
            orderedItemsDTO.add(new OrderedItemDTO(o));
        }
        return new ResponseEntity<>(orderedItemsDTO, HttpStatus.OK);
    }

    // Updates only the status of ordered item
    @RequestMapping(value="/updateOrderedItemStatus", method= RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderedItemDTO> updateOrderedItemStatus(@RequestBody OrderedItemDTO newOrderedItem){
		
        OrderedItem orderedItem = orderedItemService.findOneById(newOrderedItem.getId());
		
		if(orderedItem == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		orderedItem.setOrderedItemStatus(newOrderedItem.getOrderedItemStatus());
		
		orderedItemService.save(orderedItem);
		return new ResponseEntity<>(new OrderedItemDTO(orderedItem), HttpStatus.OK);
	}


}
