package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Long> {
}
