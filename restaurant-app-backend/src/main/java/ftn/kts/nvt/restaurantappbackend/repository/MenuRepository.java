package ftn.kts.nvt.restaurantappbackend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;

public interface MenuRepository  extends JpaRepository<Menu, Long>{
	
	//metode za dobavljanje podataka iz baze..
	
	//public Menu findOneByIsActive(boolean isActive);
	
	
	@Query("select pl from PriceList pl join fetch pl.item i where i.idItem=?1")
	public PriceList findOneWithItem(Long idItem);

	public Optional<Menu> findByIsActive(boolean isActive);
	
	public Optional<Menu> findByIdMenu(Long idMenu);
}


