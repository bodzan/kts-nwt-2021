package ftn.kts.nvt.restaurantappbackend.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SalaryChange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //format mesec/godina
    @Column
    private String startDate;
    //format mesec/godina
    @Column
    private String endDate;

    @Column
    private double salary;


}
