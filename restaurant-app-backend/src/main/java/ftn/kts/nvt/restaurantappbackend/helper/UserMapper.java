package ftn.kts.nvt.restaurantappbackend.helper;

import ftn.kts.nvt.restaurantappbackend.dto.UserDTO;
import ftn.kts.nvt.restaurantappbackend.model.User;

public class UserMapper implements MapperInterface<User, UserDTO> {
    @Override
    public User toEntity(UserDTO dto) {
        User user = new User();
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());

        return user;
    }

    @Override
    public UserDTO toDto(User entity) {
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setEmploymentDate(entity.getEmploymentDate());
        dto.setSalary(entity.getSalary().getCurrentSalary());
        dto.setRole(entity.getRoles().get(0).getName());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());

        return dto;
    }
}
