package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.PriceListDTO;
import ftn.kts.nvt.restaurantappbackend.dto.UpdateSalaryDTO;
import ftn.kts.nvt.restaurantappbackend.dto.UserDTO;
import ftn.kts.nvt.restaurantappbackend.helper.UserMapper;
import ftn.kts.nvt.restaurantappbackend.model.Manager;
import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.Salary;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.service.PriceListService;
import ftn.kts.nvt.restaurantappbackend.service.RoleService;
import ftn.kts.nvt.restaurantappbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/managers")
public class ManagerController {


    @Autowired
    public UserService userService;

    @Autowired
    public RoleService roleService;

    @Autowired
    public PriceListService priceListService;


    public UserMapper mapper;

    public ManagerController() {
        this.mapper = new UserMapper();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        List<UserDTO> userDTOS = users.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/allActive", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllActiveUsers() {
        List<User> users = userService.getAllActiveUsers();
        List<UserDTO> userDTOS = users.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/admins", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllAdmins() {
        List<User> admins = userService.getAllAdmins();
        List<UserDTO> userDTOS = admins.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/managers", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllManagers() {
        List<User> managers = userService.getAllManagers();
        List<UserDTO> userDTOS = managers.stream().map(mapper::toDto).collect(Collectors.toList());

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/bartenders", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllBartenders() {
        List<User> bartenders = userService.getAllBartenders();
        List<UserDTO> userDTOS = bartenders.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/waiters", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllWaiters() {
        List<User> waiters = userService.getAllWaiters();
        List<UserDTO> userDTOS = waiters.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/all/cooks", method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> getAllCooks() {
        List<User> cooks = userService.getAllCooks();
        List<UserDTO> userDTOS = cooks.stream().map(mapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getUserById(@PathVariable Long id) {
        Optional<User> existingUser = userService.getById(id);
        if (existingUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mapper.toDto(existingUser.get()), HttpStatus.OK);
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public ResponseEntity<Page<UserDTO>> getUserPage(@RequestParam int page, @RequestParam int size) {
        Page<User> userPage = userService.getUserPage(PageRequest.of(page, size));
        Page<UserDTO> userDTOS = userPage.map(mapper::toDto);

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<User> updateManager(@PathVariable Long id, @RequestBody ManagerDTO adminDTO) {
//
//        Optional<User> existingManager = userService.getById(id);
//        if (existingManager.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        userService.update(existingManager.get(), adminDTO);
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Manager> deleteUserById(@PathVariable Long id) {

        try {
            userService.deleteUserById(id);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/newEmployee", method = RequestMethod.POST)
    public ResponseEntity<UserDTO> addNewEmployee(@RequestBody UserDTO employeeDTO) {
        User newUser = new User();
        newUser.setEmail(employeeDTO.getEmail());
        newUser.setFirstName(employeeDTO.getFirstName());
        newUser.setLastName(employeeDTO.getLastName());
        Salary newSalary = new Salary();
        newSalary.setCurrentSalary(employeeDTO.getSalary());
        newUser.setSalary(newSalary);
        newUser.setDeleted(false);
        newUser.setPin("");
        newUser.setPassword("");
        newUser.setEmploymentDate(LocalDate.now());
        if (employeeDTO.getRole().equals("Waiter")) {
            Role role = roleService.getRoleByName("ROLE_WAITER");
            newUser.setRoles(Collections.singletonList(role));
        } else if (employeeDTO.getRole().equals("Cook")) {
            Role role = roleService.getRoleByName("ROLE_COOK");
            newUser.setRoles(Collections.singletonList(role));
        } else if (employeeDTO.getRole().equals("Manager")) {
            Role role = roleService.getRoleByName("ROLE_MANAGER");
            newUser.setRoles(Collections.singletonList(role));
        } else if (employeeDTO.getRole().equals("Bartender")) {
            Role role = roleService.getRoleByName("ROLE_BARTENDER");
            newUser.setRoles(Collections.singletonList(role));
        } else if (employeeDTO.getRole().equals("Admin")) {
            Role role = roleService.getRoleByName("ROLE_ADMIN");
            newUser.setRoles(Collections.singletonList(role));
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        newUser = userService.addNewUser(newUser, newSalary);

        return new ResponseEntity<>(mapper.toDto(newUser), HttpStatus.CREATED);
    }


    @RequestMapping(value = "/employeeSalary", method = RequestMethod.PUT)
    public ResponseEntity<Salary> changeEmployeesSalary(@RequestBody UpdateSalaryDTO newSalary) {
        Optional<User> existingUser = userService.getById(newSalary.getUserId());

        if (existingUser.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user = existingUser.get();
        Salary s = userService.updateSalary(user, newSalary.getNewSalary());

        return new ResponseEntity<>(s, HttpStatus.OK);
    }

//    @RequestMapping(value = "/newPriceList", method = RequestMethod.POST)
//    public ResponseEntity<PriceListDTO> createNewPriceList() {
//
//
//        return new ResponseEntity<>(HttpStatus.OK);
//    }
}
