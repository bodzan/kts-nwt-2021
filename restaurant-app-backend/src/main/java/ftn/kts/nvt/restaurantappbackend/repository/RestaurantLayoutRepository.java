package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.RestaurantLayout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestaurantLayoutRepository extends JpaRepository<RestaurantLayout, Long> {
    RestaurantLayout findByActiveIsTrue();
}
