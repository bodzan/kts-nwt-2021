package ftn.kts.nvt.restaurantappbackend.service;

import java.util.ArrayList;
import java.util.List;

import ftn.kts.nvt.restaurantappbackend.model.OrderStatus;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.repository.OrderRepository;
@Service
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepository;
	
	public List<Order> findAll() {
		return orderRepository.findAll();
	}
	
	public Order findOneById(Long idOrder) {
		return orderRepository.findOneByIdOrder(idOrder);
	}
	
	public Order save(Order order) {
		return orderRepository.save(order);
	}
	
	public void remove(Long idOrder) {
		orderRepository.deleteById(idOrder);
	}

	//trazimo sve narudzbine nekog stola koje nisu zavrsene
	public List<Order> getOrdersOfTableWichIsNotFinished(Long tableId) {

		List<Order> ordersOfTable = orderRepository.findAllWithDeliver(tableId);

		List<Order> activeOrderOfTable = new ArrayList<>();

		for (Order o : ordersOfTable) {
			if (o.getStatus() != OrderStatus.FINISHED) {
				activeOrderOfTable.add(o);
			}
		}
		return activeOrderOfTable;
	}

}
