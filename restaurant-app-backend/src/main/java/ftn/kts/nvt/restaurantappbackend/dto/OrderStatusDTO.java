package ftn.kts.nvt.restaurantappbackend.dto;


public enum OrderStatusDTO {
	WAITING, DONE, CANCELED, PREPARING;
}
