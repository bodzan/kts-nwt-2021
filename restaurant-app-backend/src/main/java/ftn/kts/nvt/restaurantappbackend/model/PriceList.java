package ftn.kts.nvt.restaurantappbackend.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PriceList {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPriceList;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Menu menu;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Item item;
	
	@Column(name="itemPrice", nullable=false)
	private double itemPrice;
	
	@Column(name="dateFrom", nullable = false)
	private LocalDate dateFrom;
	
	@Column(name="dateTo", nullable = true)
	private LocalDate dateTo;
	
	//da li je cena za ovu stavku menjana, odnosno da li je ovo trenutna aktuelna cena za tu stavku
	@Column(name="isActive", nullable= false)
	private boolean isActive;
	
	public PriceList() {
		
	}

	public PriceList(Menu menu, Item item, double itemPrice, LocalDate dateFrom, LocalDate dateTo, boolean isActive) {
		this.menu = menu;
		this.item = item;
		this.itemPrice = itemPrice;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.isActive = isActive;
	}

    public Long getId() {
		return idPriceList;
	}

	public void setId(Long id) {
		this.idPriceList = id;
	}
	

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public LocalDate getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}
	public LocalDate getDateTo() {
		return dateTo;
	}
	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "PriceList [idPriceList=" + idPriceList + ", menu=" + menu + ", item=" + item + ", itemPrice="
				+ itemPrice + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", isActive=" + isActive + "]";
	}

	
}
