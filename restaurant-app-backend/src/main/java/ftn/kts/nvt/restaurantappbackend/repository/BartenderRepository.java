package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.Category;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BartenderRepository extends JpaRepository<Item, Long> {

    @Query("select i from Item i where i.category = :category and i.idItem in (select oi.menuItem.idItem from OrderedItem oi where oi.orderedItemStatus = :status)")
    List<Item> showAllItemsWithCategoryAndStatus(@Param("category") OrderCategory category, @Param("status") MenuItemStatus status);

    List<Item> findByNameStartingWith(String prefix);
}
