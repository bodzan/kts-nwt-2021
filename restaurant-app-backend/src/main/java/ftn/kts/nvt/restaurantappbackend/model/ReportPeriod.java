package ftn.kts.nvt.restaurantappbackend.model;

public enum ReportPeriod {
    MONTH, QUARTER, YEAR
}
