package ftn.kts.nvt.restaurantappbackend.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Item {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idItem;
	
	@Column(name = "itemName", nullable = false)
	private String name;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)

	private OrderCategory category;
	
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private OrderSubcategory subcategory;

	
	@Column(name= "alergenList", nullable = false)
	private String alergenList;
	
	@Column(nullable = false)
	private String description;
	
	@OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PriceList> priceListOfItems;
	
	@OneToMany(mappedBy= "menuItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<OrderedItem> orderedItemList;

	@Column(name="isActive", nullable= false)
	private boolean isActive;
	
	public Item() {
		
	}

	public Item(String itemName1, OrderCategory itemCategory1, OrderSubcategory itemSubcategory1, String itemAlergens1, String itemDescription1, boolean isActive) {
		this.name = itemName1;
		this.category = itemCategory1;
		this.subcategory = itemSubcategory1;
		this.alergenList = itemAlergens1;
		this.description = itemDescription1;
		this.isActive = isActive;

	}

    public Long getIdItem() {
		return idItem;
	}



	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}




	public OrderCategory getCategory() {
		return category;
	}




	public void setCategory(OrderCategory category) {
		this.category = category;
	}




	public OrderSubcategory getSubcategory() {
		return subcategory;
	}




	public void setSubcategory(OrderSubcategory subcategory) {
		this.subcategory = subcategory;
	}



	public String getAlergenList() {
		return alergenList;
	}



	public void setAlergenList(String alergenList) {
		this.alergenList = alergenList;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public List<PriceList> getPriceListOfItems() {
		return priceListOfItems;
	}



	public void setPriceListOfItems(List<PriceList> priceListOfItems) {
		this.priceListOfItems = priceListOfItems;
	}



	public List<OrderedItem> getOrderedItemList() {
		return orderedItemList;
	}



	public void setOrderedItemList(List<OrderedItem> orderedItemList) {
		this.orderedItemList = orderedItemList;
	}


	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	@Override
	public String toString() {
		return "Item [idItem=" + idItem + ", name=" + name + ", category=" + category + ", subcategory=" + subcategory
				+ ", alergenList=" + alergenList + ", description=" + description + ", priceListOfItems="
				+ priceListOfItems + ", orderedItemList=" + orderedItemList + "]";
	}
}
