package ftn.kts.nvt.restaurantappbackend.controller;

import java.util.ArrayList;
import java.util.List;

import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.OrderDTO;
import ftn.kts.nvt.restaurantappbackend.dto.TableDTO;
import ftn.kts.nvt.restaurantappbackend.model.Tables;
import ftn.kts.nvt.restaurantappbackend.service.TablesService;

@RestController
@RequestMapping("api/tables")
public class TablesController {

	@Autowired
	private TablesService tablesService;

	@Autowired
	private OrderService orderService;
	
	/*
	 * preuzimanje svih stolova
	 */
	@RequestMapping(value="/getAllTables", method = RequestMethod.GET)
	public ResponseEntity<List<TableDTO>> getAllTables(){
		
		List<Tables> allTables = tablesService.findAll();
		
		List<TableDTO> allTableDTO = new ArrayList<>();
		
		for(Tables t : allTables) {
			TableDTO tableDto = new TableDTO(t);
			allTableDTO.add(tableDto);
		}
		
		return new ResponseEntity<>(allTableDTO, HttpStatus.OK);
	}
	
	/*
	 * preuzimanje stola po id-iju
	 */
	@RequestMapping(value="/getTableById/{idTable}", method = RequestMethod.GET)
	public ResponseEntity<TableDTO> getTableById(@PathVariable Long idTable){
		
		Tables table = tablesService.findOne(idTable);
		
		if(table == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new TableDTO(table), HttpStatus.OK);
	}
	
	/*
	 * update podataka o nekom stolu
	 */
	@RequestMapping(value="/updateTable", method=RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TableDTO> updateTable(@RequestBody TableDTO table){
		
		Tables tableToUpdate = tablesService.findOne(table.getIdTable());
		
		if(tableToUpdate == null) {
			new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		tableToUpdate.setRestaurantRoom(table.getRestaurantRoom());
		tableToUpdate.setActive(table.isActive());
		tablesService.save(tableToUpdate);
		
		return new ResponseEntity<>(new TableDTO(tableToUpdate), HttpStatus.OK);
	}
	
	/*
	 * dodavanje novog stola
	 */
	@RequestMapping(value="/addNewTable", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TableDTO> addNewTable(@RequestBody TableDTO newTable){
		
		Tables table = new Tables();
		table.setRestaurantRoom(newTable.getRestaurantRoom());
		table.setActive(newTable.isActive());
		
		tablesService.save(table);
		return new ResponseEntity<>(new TableDTO(table), HttpStatus.CREATED);
	}
	/*
	 * brisanje stola >> ne bi trebalo da se radi zato sto trebaju da se cuvaju informacije za izvestaje
	 */
	/*@RequestMapping(value="/removeTable/{tableId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> removeTable(@PathVariable Long tableId){
		Tables tableToRemove = tablesService.findOne(tableId);
		
		if(tableToRemove == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		else {
			tablesService.remove(tableId);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}*/
	
	/*
	 * logicko brisanje stola
	 */
	@RequestMapping(value="/logicalRemoveTable/{tableId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> logicalRemoveTable(@PathVariable Long tableId){
		Tables tableToRemove = tablesService.findOne(tableId);
		
		if(tableToRemove == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		else {
			//radimo samo logicko brisanje>> podaci potrebni za izvestaje
			tableToRemove.setActive(false);
			tablesService.save(tableToRemove);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
	
	/*
	 * pregled svih narudzbina nekog stola koje nisu zavrsene
	 */
	@RequestMapping(value="/allOrderOfTable/{tableId}")
	public ResponseEntity<List<OrderDTO>> allOrderOfTable(@PathVariable Long tableId){
		
		//trazimo sve ordere koji imaju sto sa ovim id-ijem i narudzbine nisu zavrsene
		List<Order> orders = orderService.getOrdersOfTableWichIsNotFinished(tableId);

		List<OrderDTO> orderiDto = new ArrayList<>();

		for(Order o : orders){
			orderiDto.add(new OrderDTO(o));
		}

		return new ResponseEntity<>(orderiDto, HttpStatus.OK);
	}
	
	/*
	 * pregled poslednje narudzbine za neki sto
	 */
	
	/*
	 * pregled svih narudzbina koje nisu zavrsene>> ciji je enum tip != finished
	 */
}
