package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.*;
import ftn.kts.nvt.restaurantappbackend.repository.BartenderRepository;
import ftn.kts.nvt.restaurantappbackend.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaiterService {

	@Autowired
	private OrderService orderService;

	@Autowired
	private BartenderRepository bartenderRepository;

	@Autowired
	private ItemService itemsService;

	@Autowired
	private OrderedItemService orderedItemService;

	public Order createOrder(Order order) {
		return orderService.save(order);
	}

	public Order editOrder(Long idOrder, Order order) throws Exception {
		Order orderToUpdate = orderService.findOneById(idOrder);
		if (orderToUpdate == null) {
			throw new Exception("Trazeni entitet nije pronadjen.");
		}
		orderToUpdate.setNote(order.getNote());
		orderToUpdate.setDeliverForTable(order.getDeliverForTable());
		orderToUpdate.setTotalPrice(order.getTotalPrice());
		orderToUpdate.setOrderedItemsList(order.getOrderedItemsList());

		Order orderSaved = orderService.save(orderToUpdate);
		return orderSaved;
	}

	public Order CancelOrderedAndOrderItems(Long idOrder) throws Exception
	{
		Order orderToUpdate = orderService.findOneById(idOrder);

		List<OrderedItem> list_ordered = orderedItemService.findByOrderId(idOrder);

		for(OrderedItem oi : list_ordered){
			oi.setOrderedItemStatus(MenuItemStatus.CANCELED);
			orderedItemService.save(oi);
		}

		orderToUpdate.setStatus(OrderStatus.CANCELED);

		return orderService.save(orderToUpdate);

	}

	public Order closeOrder(Long idOrder) throws Exception {
		Order orderToUpdate = orderService.findOneById(idOrder);
		if (orderToUpdate == null) {
			throw new Exception("Trazeni entitet nije pronadjen.");
		}

		orderToUpdate.setStatus(OrderStatus.DONE);

		Order orderSaved = orderService.save(orderToUpdate);
		return orderSaved;
	}

	public List<Item> findItems(String name) throws Exception {

		if(name.equals("") || name == null)
			return itemsService.findAll();
		return bartenderRepository.findByNameStartingWith(name);
	}
}
