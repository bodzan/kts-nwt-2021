package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.Salary;
import ftn.kts.nvt.restaurantappbackend.model.SalaryChange;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.repository.RoleRepository;
import ftn.kts.nvt.restaurantappbackend.repository.SalaryChangeRepository;
import ftn.kts.nvt.restaurantappbackend.repository.SalaryRepository;
import ftn.kts.nvt.restaurantappbackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public RoleRepository roleRepository;

    @Autowired
    public SalaryRepository salaryRepository;

    @Autowired
    public SalaryChangeRepository salaryChangeRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmailAndDeletedFalse(username).orElseThrow();
    }

    public Page<User> getUserPage(Pageable pageable) {
        return userRepository.findPageByDeletedFalse(pageable);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public List<User> getAllActiveUsers() {
        return userRepository.findAllByDeletedFalse();
    }

    public List<User> getAllAdmins() {
        Role role = roleRepository.findByName("ROLE_ADMIN");
        return  userRepository.findByRoles_Id(role.getId());
    }

    public List<User> getAllManagers() {
        Role role = roleRepository.findByName("ROLE_MANAGER");
        return  userRepository.findByRoles_Id(role.getId());
    }

    public List<User> getAllBartenders() {
        Role role = roleRepository.findByName("ROLE_BARTENDER");
        return  userRepository.findByRoles_IdAndDeletedIsFalse(role.getId());
    }

    public List<User> getAllCooks() {
        Role role = roleRepository.findByName("ROLE_COOK");
        return  userRepository.findByRoles_Id(role.getId());
    }

    public List<User> getAllWaiters() {
        Role role = roleRepository.findByName("ROLE_WAITER");
        return  userRepository.findByRoles_Id(role.getId());
    }


    public Optional<User> getById(Long id) {
        return userRepository.findById(id);
    }


    public boolean deleteUserById(Long id) {
        Optional<User> existing = userRepository.findById(id);
        if (existing.isEmpty()) {
            return false;
        }
        User u = existing.get();
        u.setDeleted(true);
        userRepository.save(u);
        return true;
    }

    public void trueDeleteById(Long id) {
        userRepository.deleteById(id);
    }

    public Salary updateSalary(User user, double newSalary) {


        SalaryChange newChange = new SalaryChange();
        LocalDate now = LocalDate.now();
        String endDate = String.valueOf(now.getMonthValue()) + "/" + String.valueOf(now.getYear());
        newChange.setEndDate(endDate);
        newChange.setSalary(user.getSalary().getCurrentSalary());
        if (user.getSalary().getSalaryChangeHistory().isEmpty()) {
            String startDate = String.valueOf(user.getEmploymentDate().getMonthValue()) + "/" + String.valueOf(user.getEmploymentDate().getYear());
            newChange.setStartDate(startDate);
        } else {
            SalaryChange lastChange = user.getSalary().getSalaryChangeHistory().get(user.getSalary().getSalaryChangeHistory().size()-1);
            newChange.setStartDate(lastChange.getEndDate());

        }

        Salary s = user.getSalary();
        s.setCurrentSalary(newSalary);
        s.getSalaryChangeHistory().add(newChange);

        salaryChangeRepository.save(newChange);
        salaryRepository.save(s);
        userRepository.save(user);

        return s;

    }

    public User addNewUser(User newUser, Salary newUserSalary) {
        newUser.setPassword(passwordEncoder.encode("sifra123"));
        salaryRepository.save(newUserSalary);
        return userRepository.save(newUser);
    }

//    public void changePassword(String oldPassword, String newPassword) {
//
//        // Ocitavamo trenutno ulogovanog korisnika
//        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
//        String username = ((User) currentUser.getPrincipal()).getEmail();
//
//        if (authenticationManager != null) {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
//        } else {
//            return;
//        }
//        User user = (User) loadUserByUsername(username);
//
//        user.setPassword(passwordEncoder.encode(newPassword));
//        userRepository.save(user);
//    }



}
