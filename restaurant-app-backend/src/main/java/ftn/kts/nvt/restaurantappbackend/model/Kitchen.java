package ftn.kts.nvt.restaurantappbackend.model;

import java.util.ArrayList;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Kitchen {

    private int dishesToday;
    private HashMap<Integer,OrderedItem> dishesWaiting;
    private HashMap<OrderedItem,Long> dishesInPreparation;
    private ArrayList<OrderedItem> dishesDone;

    public Kitchen(){
        dishesToday = 0;
        dishesWaiting = new HashMap<Integer,OrderedItem>();
        dishesInPreparation = new HashMap<OrderedItem,Long>();
        dishesDone = new ArrayList<OrderedItem>();
    }

    public void newDish(OrderedItem oi){
        dishesToday++;
        oi.setOrderedItemStatus(MenuItemStatus.WAITING);
        dishesWaiting.put(dishesToday, oi);
        NotifyCooks();
    }

    public void startPreparing(User cook, Integer jeloNaCekanju){
        OrderedItem oi = dishesWaiting.get(jeloNaCekanju);
        oi.setOrderedItemStatus(MenuItemStatus.PREPARING);
        dishesInPreparation.put(oi, cook.getId());
        dishesWaiting.remove(jeloNaCekanju);
    }

    public void FinishDish(OrderedItem oi){
        dishesInPreparation.remove(oi);
        oi.setOrderedItemStatus(MenuItemStatus.DONE);
        dishesDone.add(oi);
        NotifyWaiter();

    }

    public void NotifyCooks(){
        System.out.println("To be implemented");
    }

    public void NotifyWaiter(){
        System.out.println("To be implemented");
    }
    
}
