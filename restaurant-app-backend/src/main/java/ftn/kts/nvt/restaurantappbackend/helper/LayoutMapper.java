package ftn.kts.nvt.restaurantappbackend.helper;

import ftn.kts.nvt.restaurantappbackend.dto.RestaurantLayoutDTO;
import ftn.kts.nvt.restaurantappbackend.dto.TableLayoutDTO;
import ftn.kts.nvt.restaurantappbackend.model.RestaurantLayout;
import ftn.kts.nvt.restaurantappbackend.model.Tables;

import java.util.ArrayList;
import java.util.List;

public class LayoutMapper implements MapperInterface<RestaurantLayout, RestaurantLayoutDTO> {

    @Override
    public RestaurantLayout toEntity(RestaurantLayoutDTO dto) {
//        RestaurantLayout layout = new RestaurantLayout();
//        layout.setActive(dto.isActive());
//        List<Tables> tables = new ArrayList<>();
//        for (TableLayoutDTO t: dto.getTables()) {
//            tables.add(new Tables());
//        }
//        layout.setTables();
        return null;
    }

    @Override
    public RestaurantLayoutDTO toDto(RestaurantLayout entity) {
        RestaurantLayoutDTO layoutDTO = new RestaurantLayoutDTO();
        layoutDTO.setId(entity.getId());
        layoutDTO.setActive(entity.isActive());
        layoutDTO.setCoordinates(entity.getTableCoordinates());
        List<TableLayoutDTO> layoutDTOS = new ArrayList<>();
        for (Tables t: entity.getTables()) {
            layoutDTOS.add(new TableLayoutDTO(t.getIdTable(), t.getRestaurantRoom()));
        }
        layoutDTO.setTables(layoutDTOS);

        return layoutDTO;
    }
}
