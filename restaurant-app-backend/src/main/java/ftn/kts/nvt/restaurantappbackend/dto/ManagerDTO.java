package ftn.kts.nvt.restaurantappbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ManagerDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;
}
