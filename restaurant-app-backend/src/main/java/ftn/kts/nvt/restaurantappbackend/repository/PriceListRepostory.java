package ftn.kts.nvt.restaurantappbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ftn.kts.nvt.restaurantappbackend.model.Item;

import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;

import ftn.kts.nvt.restaurantappbackend.model.PriceList;

public interface PriceListRepostory extends JpaRepository<PriceList, Long>{
	
	
	public List<PriceList> findAll();
	
	/*
	 * trazimo priceList objekat koji ima isti id item objekta kao itemId atribut, kod koga je meni trenutno aktivan
	 * i kod koga je cena odnosno PriceList objekat podesen isActive na true
	*/
	@Query("select pl from PriceList pl join fetch pl.item i where i.idItem=?1")
	public PriceList findOneWithItem(Long idItem);
	
	
	//Kurs ima testove >> treba pronaci kurseve kod kojih id u okviru testa ima taj id...
	/*Query("select c from Course c join fetch c.exams e where c.id =?1")
	public Course findOneWithExams(Long courseId);*/
	
	@Query("select pl from PriceList pl join fetch pl.menu m where m.idMenu=?1")
	public List<PriceList> findAllWithMenu(Long idMenu);

	@Query("select pl from PriceList pl join fetch pl.item i where i.idItem=?1")
	public List<PriceList> findAllwithItem(Long itemId);


	/*izlistava sve cene za tu stavku menija*/
	@Query("select pl from PriceList pl join fetch pl.item i where i.idItem=?1")
	public List<PriceList> findWithItem(Long idItem);

	
	
}
