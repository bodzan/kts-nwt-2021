package ftn.kts.nvt.restaurantappbackend.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.ItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.service.ItemService;

@RestController
@RequestMapping("api/items")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	/*@Autowired
	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}*/
	
	
	/*
	 * dodavanje novoe stavke(jela ili pica) u ponudu restorana. 
	   dodajemo jelo ili pice kao sastojak jos nije dodato u meni.
	*/
	@RequestMapping( value = "/addNewItem", 
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> addItem(@RequestBody ItemDTO newItem) {
		
		Item item = new Item();
		item.setIdItem(newItem.getIdItem());
		item.setName(newItem.getItemName());
		item.setCategory(newItem.getCategory());
		item.setSubcategory(newItem.getSubcategory());
		item.setDescription(newItem.getDescription());
		item.setAlergenList(newItem.getAlergenList());
		
		itemService.save(item);
		
		return new ResponseEntity<>(new ItemDTO(item), HttpStatus.CREATED);
	}
	
	/*
	 * izlistavanje cele liste stavki 
	 */
	@RequestMapping(value = "/allItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ItemDTO> getAllItems(){
		List<Item> listItems = itemService.findAll();
		
		//sve te objekte iz baze prebacujemo u dto objekte
		List<ItemDTO> listItemsDto = new ArrayList<>();
		
		for(Item i : listItems) {
			ItemDTO itemDto = new ItemDTO(i);
			listItemsDto.add(itemDto);
		}
		return listItemsDto;
	}
	
	/*
	 * pretraga stavke menija na osnovu id-ija koji je jedinstveni podatak
	 */
	
	@RequestMapping(value="/findItem/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> getItem(@PathVariable Long id){
		
		Item item = itemService.findOneById(id);
		
		if(item == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(new ItemDTO(item), HttpStatus.OK);
	}
	
	/*
	 * izmena podataka o dodatoj stavci
	 */
	@PutMapping(value="/updateItem", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> updateItemData(@RequestBody ItemDTO item){
		
		//pronadjemo podatke o stavci sacuvanoj u bazi
		Item i = itemService.findOneById(item.getIdItem());
		
		if( i == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// azuriramo sve podatke ako je sve u redu..
		i.setAlergenList(item.getAlergenList());
		i.setCategory(item.getCategory());
		i.setDescription(item.getDescription());
		i.setName(item.getItemName());
		i.setSubcategory(item.getSubcategory());
		i.setActive(item.isActive());
		
		i = itemService.save(i);
		System.out.println("***** akitvan: " + i.isActive());
		return new ResponseEntity<>(new ItemDTO(i), HttpStatus.OK);
	}


	/*
	brisanje stavke iz sistema >> moze se promeniti i na logicko brisanje zbog izvestaja
	 */

	@DeleteMapping(value="/deleteItem/{id}")
	public ResponseEntity<Void> deleteItem(@PathVariable Long id){
		
		Item i = itemService.findOneById(id);
		
		if(i == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		else {
			itemService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}

	/*
	logicko brisanje itema
	 */
	@DeleteMapping(value="/logicDeleteItem/{id}")
	public ResponseEntity<Void> logicDeleteItem(@PathVariable Long id){

		Item i = itemService.findOneById(id);

		if(i == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		else {
			itemService.logicalRemove(id); //logicko brisanje vrsimo
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
}
