package ftn.kts.nvt.restaurantappbackend.dto;



import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;

public class ItemDTO {
	private Long idItem;
	

	private OrderCategory category;
	
	private String name;
	
	private OrderSubcategory subcategory;


	private String alergenList;
	
	private String description;

	private boolean isActive;
	
	public ItemDTO() {
	
	}
	public ItemDTO(Item i) {
		this.idItem = i.getIdItem();
		this.name = i.getName();
		this.category = i.getCategory();
		this.subcategory = i.getSubcategory();
		this.alergenList = i.getAlergenList();
		this.description = i.getDescription();
		this.isActive = i.isActive();
	}


	public ItemDTO(Long idItem, OrderCategory category, String name, OrderSubcategory subcategory, String alergenList,
			String description, boolean isActive) {
		super();
		this.idItem = idItem;
		this.category = category;
		this.name = name;
		this.subcategory = subcategory;
		this.alergenList = alergenList;
		this.description = description;
		this.isActive = isActive;
	}
	
	public String getItemName() {
		return name;
	}
	public void setItemName(String itemName) {
		this.name = itemName;
	}
	public Long getIdItem() {
		return idItem;
	}
	public void setIdItem(Long idItem) {
		this.idItem = idItem;
	}
	public OrderCategory getCategory() {
		return category;
	}
	public void setCategory(OrderCategory category) {
		this.category = category;
	}
	public OrderSubcategory getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(OrderSubcategory subcategory) {

		this.subcategory = subcategory;
	}
	public String getAlergenList() {
		return alergenList;
	}
	public void setAlergenList(String alergenList) {
		this.alergenList = alergenList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	@Override
	public String toString() {
		return "ItemDTO [idItem=" + idItem + ", category=" + category + ", subcategory=" + subcategory
				+ ", alergenList=" + alergenList + ", description=" + description + "]";
	}
}
