package ftn.kts.nvt.restaurantappbackend.dto;

import java.time.LocalDate;

import ftn.kts.nvt.restaurantappbackend.model.PriceList;

public class PriceListDTO {
	private Long idPriceList;
	private MenuDTO menu;
	private ItemDTO item;
	private double itemPrice;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	
	//da li je cena za trenutnu stavku aktivna.. odnosno informacija o tome ako je cena stavke menjana bice neaktivna..
	private boolean isActive;
	
	public PriceListDTO() {
		
	}

	public PriceListDTO(Long id, MenuDTO menu, ItemDTO item, double itemPrice, LocalDate dateFrom, LocalDate dateTo, boolean isActive) {
		super();
		this.idPriceList = id;
		this.menu = menu;
		this.item = item;
		this.itemPrice = itemPrice;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.isActive = isActive;
	}

	public PriceListDTO(PriceList p) {
		this.idPriceList = p.getId();
		this.menu = new MenuDTO(p.getMenu());
		this.item = new ItemDTO(p.getItem());
		this.itemPrice = p.getItemPrice();
		this.dateFrom = p.getDateFrom();
		this.dateTo = p.getDateTo();
		this.isActive = p.isActive();
	}

	public Long getIdPriceList() {
		return idPriceList;
	}

	public void setIdPriceList(Long idPriceList) {
		this.idPriceList = idPriceList;
	}

	public MenuDTO getMenu() {
		return menu;
	}

	public void setMenu(MenuDTO menu) {
		this.menu = menu;
	}

	public ItemDTO getItem() {
		return item;
	}

	public void setItem(ItemDTO item) {
		this.item = item;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}
