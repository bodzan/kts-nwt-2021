package ftn.kts.nvt.restaurantappbackend.dto;

import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;

public class OrderedItemDTO {
	private Long idOrderedItem;
	
	private OrderDTO order;
	private ItemDTO menuItem;
	private int quantity;
	private MenuItemStatus orderedItemStatus;
	
	public OrderedItemDTO() {
		
	}
	public OrderedItemDTO(OrderedItem i) {
		this.idOrderedItem = i.getId();
		this.order = new OrderDTO(i.getOrder());
		this.menuItem = new ItemDTO(i.getMenuItem());
		this.quantity = i.getQuantity();
		this.orderedItemStatus = i.getOrderedItemStatus();
	}

	public OrderedItemDTO(Long id, OrderDTO order, ItemDTO menuItem, int quantity, MenuItemStatus orderedItemStatus) {
		super();
		this.idOrderedItem = id;
		this.order = order;
		this.menuItem = menuItem;
		this.quantity = quantity;
		this.orderedItemStatus = orderedItemStatus;
	}

	public Long getId() {
		return idOrderedItem;
	}

	public void setId(Long id) {
		this.idOrderedItem = id;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public ItemDTO getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(ItemDTO menuItem) {
		this.menuItem = menuItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public MenuItemStatus getOrderedItemStatus() {
		return orderedItemStatus;
	}
	public void setOrderedItemStatus(MenuItemStatus orderedItemStatus) {
		this.orderedItemStatus = orderedItemStatus;
	}
	
	
}
