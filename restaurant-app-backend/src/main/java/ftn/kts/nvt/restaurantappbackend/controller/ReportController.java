package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.ReportDTO;
import ftn.kts.nvt.restaurantappbackend.model.Report;
import ftn.kts.nvt.restaurantappbackend.model.ReportPeriod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reports")
public class ReportController {



    @RequestMapping(value = "/generateExpense", method = RequestMethod.GET)
    public ResponseEntity<ReportDTO> generateExpenseReport(@RequestParam ReportPeriod period) {


        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/generateIncome", method = RequestMethod.GET)
    public ResponseEntity<ReportDTO> generateIncomeReport(ReportPeriod period) {


        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/generateSales", method = RequestMethod.GET)
    public ResponseEntity<ReportDTO> generateSalesReport(ReportPeriod period) {


        return new ResponseEntity<>(HttpStatus.OK);
    }


}
