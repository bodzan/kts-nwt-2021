package ftn.kts.nvt.restaurantappbackend.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.PriceListDTO;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.service.ItemService;
import ftn.kts.nvt.restaurantappbackend.service.MenuService;
import ftn.kts.nvt.restaurantappbackend.service.PriceListService;

@RestController
@RequestMapping("api/priceList")
public class PriceListController {

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private PriceListService priceListService;
	
	/*@Autowired
	public void setPriceListService(PriceListService priceListService) {
		this.priceListService = priceListService;
	}*/
	
	/*@RequestMapping(value="getAllActivePriceList/{idMenu}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PriceList> getAllActivePriceList(@PathVariable Long idMenu){
		
		return null;
	}*/
	
	
	
}
