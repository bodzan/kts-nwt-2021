package ftn.kts.nvt.restaurantappbackend.service;

import java.util.List;
import java.util.Optional;

import ftn.kts.nvt.restaurantappbackend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;
import ftn.kts.nvt.restaurantappbackend.repository.PriceListRepostory;

@Service
public class MenuService {
	
	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private PriceListRepostory priceListRepository;
	
	//setter based dependency injection..
	/*@Autowired
	public void setMenuRepository(MenuRepository menuRepository) {
		this.menuRepository = menuRepository;
	}*/
	
	/*public Menu findOne(Long id) {
		return  menuRepository.findById(id).orElseGet(null);
	}*/

	public Optional<Menu> findById(Long id){
		return menuRepository.findByIdMenu(id);
	}
	
	public List<Menu> findAll(){
		return menuRepository.findAll();
	}
	
	public Menu save(Menu m) {
		return menuRepository.save(m);
	}
	
	public void remove(Long id) {
		menuRepository.deleteById(id);
	}

	public boolean logicalRemove(long id){
		Optional<Menu> existingMenu = menuRepository.findByIdMenu(id);
		if (existingMenu.isEmpty()) {
			return false;
		}
		Menu m = existingMenu.get();
		m.setActive(false);
		menuRepository.save(m);
		return true;
	}
	
	//pronadji trenutno aktivni meni
	/*public Menu findActiveMenu(boolean isActive) {
		return menuRepository.findOneByIsActive(isActive);
	}*/
	
	//pronadjemo sve itemPriceObjekte koji pripadaju meniju sa prosledjenim id-ijem
	public List<PriceList> findAllWithMenuObj(Long idMenu){
		return priceListRepository.findAllWithMenu(idMenu);
	}

	//pronadji trenutno aktivni meni ako ga nema vrati null
	public Optional<Menu> findActiveMenu(boolean isActive){
		return menuRepository.findByIsActive(isActive);
	}

	//??????????odraditi testove za update
	public boolean update(Menu updateMenu, long id){
		Optional<Menu> found = this.menuRepository.findByIdMenu(id);

		if(found.isPresent()){
			found.get().setActive(updateMenu.isActive());
			menuRepository.save(found.get()); //sacuvamo updatovane promene
			return true;
		}
		return false;
	}
}
