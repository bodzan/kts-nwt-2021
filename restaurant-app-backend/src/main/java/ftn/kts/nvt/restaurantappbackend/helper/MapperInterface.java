package ftn.kts.nvt.restaurantappbackend.helper;

public interface MapperInterface<T, U> {

    T toEntity(U dto);

    U toDto(T entity);

}
