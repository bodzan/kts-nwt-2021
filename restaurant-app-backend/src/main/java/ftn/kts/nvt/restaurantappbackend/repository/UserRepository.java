package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByRoles_Id(Long id);

    List<User> findByRoles_IdAndDeletedIsFalse(Long id);

    Optional<User> findByEmailAndDeletedFalse(String email);

    List<User> findAllByDeletedFalse();

    Page<User> findPageByDeletedFalse(Pageable pageable);
}
