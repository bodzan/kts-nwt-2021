package ftn.kts.nvt.restaurantappbackend.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity //omogucuje da orm kreira tabelu u semi baze podataka za ovu klasu
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMenu;
	
	//mogu se jos dodati i informacije aktivan od datuma >> aktivan do datuma ako je potrebno pregledati informacije od kada do kad je bio aktivan ...>>
	
	@OneToMany(mappedBy = "menu", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PriceList> priceList;
	
	// MOZE SE DODATI INFORMACIJA DA LI JE MENI TRENUTNO AKTIVAN..
	private boolean isActive; //ako se kreira novi meni stari se cuvaju negde..
	
	public Menu() {
		
	}

    public Menu(boolean menuActive) {
		this.isActive = menuActive;
    }

    public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public List<PriceList> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<PriceList> priceList) {
		this.priceList = priceList;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public String toString() {
		return "Menu [idMenu=" + idMenu + ", priceList=" + priceList + ", isActive=" + isActive + "]";
	}

	
}
