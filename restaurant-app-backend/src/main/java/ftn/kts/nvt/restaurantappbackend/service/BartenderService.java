package ftn.kts.nvt.restaurantappbackend.service;


import ftn.kts.nvt.restaurantappbackend.model.*;
import ftn.kts.nvt.restaurantappbackend.repository.BartenderRepository;
import ftn.kts.nvt.restaurantappbackend.repository.OrderedItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class BartenderService {

    @Autowired
    public OrderedItemRepository orderedItemRepository;

    @Autowired
    public BartenderRepository bartenderRepository;

    @Autowired
    public ItemService itemService;

    @Autowired
    public OrderedItemService orderedItemService;

/*    public OrderedItem acceptItem(Long itemId) throws Exception {
        OrderedItem orderToUpdate = orderedItemRepository.getById(itemId);
        if (orderToUpdate == null) {
            throw new Exception("Trazeni entitet nije pronadjen.");
        }
        if(itemService.findOneById(orderToUpdate.getId()).getCategory()!= Category.DRINK){
            throw new Exception("Trazeni entitet nije pice.");
        }
        orderToUpdate.setOrderedItemStatus(MenuItemStatus.PREPARING);
        return orderedItemRepository.save(orderToUpdate);
    }*/

    public List<Item> showAllDrinksToAccept(){
        return bartenderRepository.showAllItemsWithCategoryAndStatus(OrderCategory.DRINK,MenuItemStatus.WAITING);
    }

    public List<OrderedItem> allOrderedDrinksWaiting(){
        List<OrderedItem> oiAll = orderedItemService.findAll();
        List<OrderedItem> drinks = new ArrayList<OrderedItem>();

        for(OrderedItem oi: oiAll)
        {
            if(oi.getOrderedItemStatus() == MenuItemStatus.WAITING && oi.getMenuItem().getCategory() == OrderCategory.DRINK)
            {
                drinks.add(oi);
            }
        }


        return drinks;
    }

    public List<OrderedItem> allOrderedDrinksPreparing(){
        List<OrderedItem> oiAll = orderedItemService.findAll();
        List<OrderedItem> drinks = new ArrayList<OrderedItem>();

        for(OrderedItem oi: oiAll)
        {
            if(oi.getOrderedItemStatus() == MenuItemStatus.PREPARING && oi.getMenuItem().getCategory() == OrderCategory.DRINK)
            {
                drinks.add(oi);
            }
        }


        return drinks;
    }

}
