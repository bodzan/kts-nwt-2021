package ftn.kts.nvt.restaurantappbackend.model;

public enum MenuItemStatus {
	WAITING, DONE, CANCELED, PREPARING;
}
