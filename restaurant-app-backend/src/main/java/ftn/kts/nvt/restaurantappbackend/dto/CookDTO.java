package ftn.kts.nvt.restaurantappbackend.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ftn.kts.nvt.restaurantappbackend.model.Cook;
import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.Salary;
import ftn.kts.nvt.restaurantappbackend.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CookDTO {  // extends UserDTO

    // private ArrayList<Integer> dishesInPreparation;
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private LocalDate employmentDate;

    private Salary salary;

    private Date lastPasswordResetDate;

    private List<Role> roles;
    
    public CookDTO(Long id, String firstName, String lastName, String email, String password, LocalDate employmentDate, Salary salary, Date lastPasswordResetDate){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.employmentDate = employmentDate;
        this.salary = salary;
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public CookDTO(User u){
        this.id = u.getId();
        this.firstName = u.getFirstName();
        this.lastName = u.getLastName();
        this.email = u.getEmail();
        this.password = u.getPassword();
        this.employmentDate = u.getEmploymentDate();
        this.salary = u.getSalary();
        this.lastPasswordResetDate = u.getLastPasswordResetDate();
        this.roles = u.getRoles();
    }

    // public ArrayList<Integer> getDishesInPreparation() {
	// 	return dishesInPreparation;
	// }

	// public void setDishesInPreparation(ArrayList<Integer> dishes) {
	// 	this.dishesInPreparation = dishes;
	// }
}
