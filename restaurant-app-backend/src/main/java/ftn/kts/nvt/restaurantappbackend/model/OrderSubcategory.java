package ftn.kts.nvt.restaurantappbackend.model;

public enum OrderSubcategory {
    WATER, BEER, WINE, HOT_DRINKS, NON_ALCOHOLIC_DRINKS, STRONG_DRINKS, SOUPS, APPETIZERS, GRILL, PIZZAS, DESERTS,
    SALADS, MEALS_TO_ORDER
}
