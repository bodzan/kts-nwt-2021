package ftn.kts.nvt.restaurantappbackend.dto;

import ftn.kts.nvt.restaurantappbackend.model.Room;
import ftn.kts.nvt.restaurantappbackend.model.Tables;

public class TableDTO {
	private Long idTable;
	private Room restaurantRoom;
	private boolean isActive;
	
	public TableDTO() {
		
	}

	public TableDTO(Long idTable, Room restaurantRoom, boolean isActive) {
		super();
		this.idTable = idTable;
		this.restaurantRoom = restaurantRoom;
		this.isActive = isActive;
	}
	
	public TableDTO(Tables t) {
		this.idTable = t.getIdTable();
		this.restaurantRoom = t.getRestaurantRoom();
		this.isActive = t.isActive();
	}

	public Long getIdTable() {
		return idTable;
	}

	public void setIdTable(Long idTable) {
		this.idTable = idTable;
	}

	public Room getRestaurantRoom() {
		return restaurantRoom;
	}

	public void setRestaurantRoom(Room restaurantRoom) {
		this.restaurantRoom = restaurantRoom;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
}
