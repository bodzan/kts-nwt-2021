package ftn.kts.nvt.restaurantappbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.kts.nvt.restaurantappbackend.model.Tables;

public interface TablesRepository extends JpaRepository<Tables, Long>{

}
