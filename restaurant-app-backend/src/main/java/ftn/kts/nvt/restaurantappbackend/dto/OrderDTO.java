package ftn.kts.nvt.restaurantappbackend.dto;

import java.time.LocalDateTime;

import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.model.OrderStatus;

public class OrderDTO {
	private Long idOrder;
	private LocalDateTime dateAndTime;
	private String note;
	private OrderStatus status;
	private double totalPrice;
	private TableDTO deliverForTable;
	
	public OrderDTO() {
		
	}
	public OrderDTO(Order o) {
		this.idOrder = o.getIdOrder();
		this.dateAndTime = o.getDateAndTime();
		this.note = o.getNote();
		this.status = o.getStatus();
		this.totalPrice = o.getTotalPrice();
		this.deliverForTable = new TableDTO(o.getDeliverForTable());
	}
	
	public OrderDTO(Long idOrder, LocalDateTime dateAndTime, String note, OrderStatus status, double totalPrice,
			TableDTO deliverForTable) {
		super();
		this.idOrder = idOrder;
		this.dateAndTime = dateAndTime;
		this.note = note;
		this.status = status;
		this.totalPrice = totalPrice;
		this.deliverForTable = deliverForTable;
	}
	public Long getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}
	public LocalDateTime getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(LocalDateTime dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public TableDTO getDeliverForTable() {
		return deliverForTable;
	}
	public void setDeliverForTable(TableDTO deliverForTable) {
		this.deliverForTable = deliverForTable;
	}
	
}
