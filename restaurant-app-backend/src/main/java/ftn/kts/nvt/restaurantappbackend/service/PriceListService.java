package ftn.kts.nvt.restaurantappbackend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;

import ftn.kts.nvt.restaurantappbackend.repository.PriceListRepostory;

@Service
public class PriceListService {

	@Autowired
	private PriceListRepostory priceListRepository;

	
	@Autowired
	private MenuRepository menuRepository;


	
	/*@Autowired
	public void setPriceListRepository(PriceListRepostory priceListRepository) {
		this.priceListRepository = priceListRepository;
	}*/
	
	public List<PriceList> findAll(){
		return priceListRepository.findAll();
	}
	
	public PriceList findOneById(Long id) {
		return priceListRepository.findById(id).orElseGet(null);
	}
	
	public PriceList save(PriceList priceList) {
		return priceListRepository.save(priceList);
	}
	
	public void remove(Long id) {
		priceListRepository.deleteById(id);
	}
	
	public PriceList findOneWithItem(Long itemId) {
		return priceListRepository.findOneWithItem(itemId);
	}

	public List<PriceList> findAllWithItem(Long itemId) {
		// TODO Auto-generated method stub
		return priceListRepository.findAllwithItem(itemId);
	}
	

	/*pronalazak priceList objekta po id-iju itema cija je cena trenutno aktivna u meniju*/
	public PriceList findOneByIdOfItem(Long idItem){
		
		//prvo trazimo sve ordereItema koji imaju item sa tim id-ijem
		List<PriceList> orderItemPrice = priceListRepository.findWithItem(idItem);
		
		//sad proverimo da li je ta cena iz trenutno aktivnog menija i da li je vazeca.
		//Menu activeMenu = menuRepository.findOneByIsActive(true);
		//doslo je do promene ovde
		Optional<Menu> activeMenu = menuRepository.findByIsActive(true);

		if(activeMenu.isPresent()){
			for(PriceList pl : orderItemPrice) {
				if(pl.getMenu().getIdMenu() == activeMenu.get().getIdMenu() &&
						pl.isActive() == true) {
					return pl;
				}
			}
		}

		return null;
	}


	/*
	 * pronadjemo sve itemPriceObjekte koji pripadaju meniju sa 
	 * prosledjenim id-ijem ali ujedno id njihovog itema je jednak prosledjenom
	 * u okviru argumenta i aktivni su..
	*/
	public PriceList findActiveItemPriceObjectFromMenu(Long idMenu, Long idItem) {
		
		//trazimo stavke ovog menija
		List<PriceList> menuItems = priceListRepository.findAllWithMenu(idMenu);
		
		//trazimo itemPriceList objekte koji imaju stavku sa prosledjenim id-ijem
		List<PriceList> itemPriceObjHasItem = priceListRepository.findAllwithItem(idItem);
		
		for(PriceList plM : menuItems) {
			for(PriceList plI : itemPriceObjHasItem) {
				if(plM.getId().equals(plI.getId())) {
					if(plI.isActive()) {
						return plI;
					}
				}
			}
		}
		return null;
	}
	//????????????? za update dodati testove
	public boolean update(PriceList updatePriceList, Long id){
		Optional<PriceList> foundObj = priceListRepository.findById(id);

		if(foundObj.isPresent() == true){
			PriceList found = foundObj.get();

			found.setActive(updatePriceList.isActive());
			found.setDateTo(updatePriceList.getDateTo());
			found.setDateFrom(updatePriceList.getDateFrom());
			found.setItemPrice(updatePriceList.getItemPrice());
			found.setMenu(updatePriceList.getMenu());
			found.setItem(updatePriceList.getItem());
			priceListRepository.save(found); //sacuvamo updatovane promene
			return true;
		}
		return false;
	}
}
