package ftn.kts.nvt.restaurantappbackend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.kts.nvt.restaurantappbackend.model.Item;

public interface ItemRepository extends JpaRepository<Item, Long>{
	
	public Item findOneByIdItem(Long idItem);
	
	public List<Item> findAll();

	public Optional<Item> findById(Long idItem);
	
}
