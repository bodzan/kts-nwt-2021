package ftn.kts.nvt.restaurantappbackend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.model.Category;
import ftn.kts.nvt.restaurantappbackend.model.Cook;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.User;
//import ftn.kts.nvt.restaurantappbackend.repository.CookRepository;
import ftn.kts.nvt.restaurantappbackend.repository.UserRepository;

@Service
public class CookService {

    @Autowired
    public UserRepository cookRepository;

    // public List<User> findAll(){
    //     List<User> cooks = new ArrayList<User>();
    //     List<User> allUsers = cookRepository.findAll();
    //     for (User user : allUsers) {
    //         List<Role> roles = user.getRoles();
    //         for (Role r : roles) {
    //             if (r.getName().equals("ROLE_COOK")){
    //                 cooks.add(user);
    //             }
    //         }
    //     }
    //     return cooks;
    // }

    public User findById(Long id){
        User u = cookRepository.findById(id).orElse(null);
        for (Role role : u.getRoles()) {
            if (role.getName().equals("ROLE_COOK")){
                return u;
            }
        }
        return null;
    }

    public User save(User cook) throws Exception{
        if(cookRepository.findById(cook.getId()) != null){
            return null;
        }
        return cookRepository.save(cook);
    }

    public void delete(User c) {
        cookRepository.delete(c);
    }

    public void deleteById(Long id) throws Exception{
        User u = cookRepository.findById(id).orElse(null);
        if(u == null){
            throw new Exception("User with given id doesn't exist");
        }
        //cookRepository.deleteById(id);
        u.setDeleted(true);
        cookRepository.save(u);
    }

    // public List<Item> showAllDishesToAccept(){
    //     return cookRepository.showAllItemsWithCategoryAndStatus(Category.DISH,MenuItemStatus.WAITING);
    // }

    
}
