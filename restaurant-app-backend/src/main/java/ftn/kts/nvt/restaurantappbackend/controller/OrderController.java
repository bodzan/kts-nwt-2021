package ftn.kts.nvt.restaurantappbackend.controller;

import java.util.ArrayList;
import java.util.List;

import ftn.kts.nvt.restaurantappbackend.dto.ItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.*;
import ftn.kts.nvt.restaurantappbackend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.OrderDTO;
import ftn.kts.nvt.restaurantappbackend.dto.OrderedItemDTO;

@RestController
@RequestMapping("api/order")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private TablesService tablesService;
	
	@Autowired
	private OrderedItemService orderedItemService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PriceListService priceListService;
	
	@RequestMapping(value="/allOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderDTO>> getAllMenus(){
		List<Order> orders = orderService.findAll();
		
		List<OrderDTO> orderDto = new ArrayList<>();
		
		for(Order o: orders) {
			OrderDTO orderForAdd = new OrderDTO(o);			
			orderDto.add(orderForAdd);
		}
		return new ResponseEntity<>(orderDto, HttpStatus.OK);
	}
	
	@RequestMapping(value="/getOrderById/{idOrder}", method = RequestMethod.GET)
	public ResponseEntity<OrderDTO> getOrderById(@PathVariable Long idOrder){
		
		Order o = orderService.findOneById(idOrder);
		
		if(o == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		OrderDTO orderDTO = new OrderDTO(o);
		
		return new ResponseEntity<>(orderDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value="/updateOrder", method= RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDTO> updateOrder(@RequestBody OrderDTO newOrder){
		
		Order order = orderService.findOneById(newOrder.getIdOrder());
		
		if(order == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		order.setDeliverForTable(order.getDeliverForTable()); //?? ne stavlja ove azurirane podatke
		order.setNote(newOrder.getNote());
		order.setStatus(newOrder.getStatus());
		order.setTotalPrice(newOrder.getTotalPrice());
		order.setIdOrder(newOrder.getIdOrder());
		
		orderService.save(order);
		return new ResponseEntity<>(new OrderDTO(order), HttpStatus.OK);
	}
	
	@RequestMapping(value="/removeOrder/{idOrder}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> removeOrder(@PathVariable Long idOrder){
		
		Order orderToRemove = orderService.findOneById(idOrder);
		
		if(orderToRemove == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		//oznacimo da je otkazana narudzbina to podrazumeva brisanje narudzbine
		orderToRemove.setStatus(OrderStatus.CANCELED);
		orderService.save(orderToRemove);
		
		return new ResponseEntity<> (HttpStatus.OK);
	}
	
	/************************ POSLOVNA LOGIKA *************************/
	/*
	 * KREIRANJE NOVE NARUDZBINE
	 */
	@RequestMapping(value = "/createNewOrder", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDTO> createNewOrder(@RequestBody OrderDTO newOrder){
		
		Order orderToCreate = new Order();
		
		orderToCreate.setDateAndTime(newOrder.getDateAndTime()); //moze i trenutno vreme inicijalno da se postavi
		orderToCreate.setDeliverForTable(tablesService.findOne(newOrder.getDeliverForTable().getIdTable()));
		orderToCreate.setNote(newOrder.getNote());
		orderToCreate.setStatus(newOrder.getStatus()); //status je WAITING inicijalno pri kreiranju
		orderToCreate.setTotalPrice(newOrder.getTotalPrice()); //inicijalno je null pri kreiranju
		
		orderService.save(orderToCreate);
		OrderDTO orderDTO = new OrderDTO(orderToCreate);
		
		return new ResponseEntity<>(orderDTO, HttpStatus.OK);
	}
	
	/*
	 * NARUCI STAVKU IZ MENIJA
	 */	
	@RequestMapping(value="/orderItemFromMenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderedItemDTO> orderItemFromMenu(@RequestBody ItemDTO orderedItem, @RequestParam(name="orderId") Long orderId, @RequestParam(name="quantity") int quantity){
		
		//OrderedItem itemToOrder = orderedItemService.findOneByIdOfItem(idItem); //trazimo podatak i o ceni tog proizvoda
		
		/*if(itemToOrder == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		//pronadjemo porudzbinu sa tim id-ijem
		Order order = orderService.findOneById(orderId);
		
		//u listu dodatih proizvoda za ovu porudzbinu dodajemo izabranu stavku iz ponude resotrana
		Item ordItem = new Item();
		ordItem.setAlergenList(orderedItem.getAlergenList());
		ordItem.setCategory(orderedItem.getCategory());
		ordItem.setDescription(orderedItem.getDescription());
		ordItem.setIdItem(orderedItem.getIdItem());
		ordItem.setName(orderedItem.getItemName());
		ordItem.setSubcategory(orderedItem.getSubcategory());
		
		List<Item> orderedItemList = order.getOrderedItemsList();
		order.getOrderedItemsList().add(ordItem);
		*/

		//trazimo item sa tim id-ijem
		Item itemWhichIsOrdered = itemService.findOneById(orderedItem.getIdItem());
		//PriceList priceForOrderedItem = priceListService.findActiveItemPriceObjectFromMenu(activeMenu.getIdMenu(), orderedItem.getIdItem());

		if (itemWhichIsOrdered == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		//pronadjemo porudzbinu sa tim id-ijem
		Order order = orderService.findOneById(orderId);

		//u listu dodatih proizvoda za ovu porudzbinu dodajemo izabranu stavku iz ponude resotrana
		OrderedItem ordItem = new OrderedItem();
		//ordItem.setId(orderId);
		ordItem.setMenuItem(itemWhichIsOrdered);
		ordItem.setOrder(order);
		ordItem.setOrderedItemStatus(MenuItemStatus.WAITING); //inicijalno je cekanje podeseno..
		ordItem.setQuantity(quantity);

		//sacuvamo order i orderedItem
		//orderService.save(order);
		orderedItemService.save(ordItem);

		return new ResponseEntity<>(new OrderedItemDTO(ordItem), HttpStatus.CREATED);
		//return null;
	}

	/*
	 * BRISANJE STAVKE IZ NARUDZBINE >> vrsi se pravo brisanje stavke iz narudzbine
	 */
	@RequestMapping(value="/deleteOrderedItemFromOrder/{orderId}/{orderedItemId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteOrderedItemFromOrder(@PathVariable Long orderedItemId, @PathVariable Long orderId){

		List<OrderedItem> orderedItemList = orderedItemService.findByOrderId(orderId);

		for(OrderedItem i : orderedItemList) {

			if(i.getMenuItem().getIdItem() == orderedItemId) {
				//narudzbina se moze otkazati samo ako nije pocela da se priprema >> to je dodatno ogranicenje
				//ili ako nije vec zavrsena priprema za tu stavku.
				if(i.getOrderedItemStatus() == MenuItemStatus.WAITING || i.getOrderedItemStatus() == MenuItemStatus.CANCELED) {
					//orderedItemService.remove(i.getIdOrderedItem());

					//promenicemo status na otkazano
					i.setOrderedItemStatus(MenuItemStatus.CANCELED);
					//sad ovo sacuvamo kao izmenjeno u bazi
					orderedItemService.save(i);

					return new ResponseEntity<>(HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

	}
	/*
	ZAVRSAVANJE PORUDZBINE
	 */
	@RequestMapping(value="/finishOrder/{orderId}", method= RequestMethod.PUT)
	public ResponseEntity<OrderDTO> finishOrder(@PathVariable Long orderId){
		//trazimo narudzbinu sa prosledjenim id-ijem
		Order order = orderService.findOneById(orderId);

		//trazimo sve stavke porucene u okviru te porudzbine
		List<OrderedItem> orderedItemList = orderedItemService.findByOrderId(orderId);

		//za svaku narucenu stavku u okviru porudzbine trazimo cenu
		double totalPrice = 0;
		for(OrderedItem i : orderedItemList){
			//trazimo aktivnu cenu tog proizvoda
			List<PriceList> priceListForItem = priceListService.findAllWithItem(i.getMenuItem().getIdItem());

			//za stavke cenovnika uzmemo trenutno vazecu cenu i dodamo je na ukupnu sumu
			for(PriceList price: priceListForItem){
				if(price.isActive()  == true && i.getOrderedItemStatus() != MenuItemStatus.CANCELED){
					// ako je to trenutno aktivna cena stavke i ako stavka u okviru narudzbine nije otkazana
					totalPrice += i.getQuantity() * price.getItemPrice(); //dodajemo cenu stavke pomnozenu sa brojem narucenih tih stavki
					// kako bismo izracunali ukupan racun
				}
			}
		}

		//azuiramo podatke za order
		order.setTotalPrice(totalPrice);
		order.setStatus(OrderStatus.FINISHED);

		orderService.save(order);
		return new ResponseEntity<>(new OrderDTO(order), HttpStatus.OK);
	}
	
	/*
	 * PROMENA STATUSA NARUDZBINE
	 */
	@RequestMapping(value="/changeOrderStatus/{idOrder}", method= RequestMethod.PUT)
	public ResponseEntity<OrderDTO> changeOrderStatus(@PathVariable Long idOrder, @RequestParam(name="orderStatus") OrderStatus status){
		
		Order orderTochange = orderService.findOneById(idOrder);
		
		if(orderTochange == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		orderTochange.setStatus(status);
		
		orderService.save(orderTochange);
		
		return new ResponseEntity<>(new OrderDTO(orderTochange), HttpStatus.OK);
	}
	
	
	
}
