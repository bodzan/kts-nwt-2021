package ftn.kts.nvt.restaurantappbackend.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.Tables;
import ftn.kts.nvt.restaurantappbackend.repository.TablesRepository;

@Service
public class TablesService {

	@Autowired
	private TablesRepository tablesRepository;
	
	public Tables findOne(Long id) {
		return  tablesRepository.findById(id).orElseGet(null);
	}
	public Optional<Tables> findById(Long id) {
		return tablesRepository.findById(id);
	}
	
	public List<Tables> findAll(){
		return tablesRepository.findAll();
	}
	
	public Tables save(Tables t) {
		return tablesRepository.save(t);
	}
	
	public void remove(Long id) {
		tablesRepository.deleteById(id);
	}
	
}
