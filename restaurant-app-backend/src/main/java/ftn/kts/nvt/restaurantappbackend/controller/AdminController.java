package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.RestaurantLayoutDTO;
import ftn.kts.nvt.restaurantappbackend.dto.TableLayoutDTO;
import ftn.kts.nvt.restaurantappbackend.helper.LayoutMapper;
import ftn.kts.nvt.restaurantappbackend.model.RestaurantLayout;
import ftn.kts.nvt.restaurantappbackend.model.Room;
import ftn.kts.nvt.restaurantappbackend.model.Tables;
import ftn.kts.nvt.restaurantappbackend.service.RestaurantLayoutService;
import ftn.kts.nvt.restaurantappbackend.service.TablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/admins")
public class AdminController {

    @Autowired
    public TablesService tablesService;

    @Autowired
    public RestaurantLayoutService restaurantLayoutService;

    public LayoutMapper layoutMapper;

    public AdminController() {
        this.layoutMapper = new LayoutMapper();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN_PERMISSIONS, WAITER_PERMISSIONS')")
    @RequestMapping(value = "/layouts/active", method = RequestMethod.GET)
    public ResponseEntity<RestaurantLayoutDTO> getActiveLayout() {

        RestaurantLayout layout = restaurantLayoutService.getActiveLayout();
        RestaurantLayoutDTO dto = layoutMapper.toDto(layout);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN_PERMISSIONS')")
    @RequestMapping(value = "/layouts/all", method = RequestMethod.GET)
    public ResponseEntity<List<RestaurantLayoutDTO>> getAllLayouts() {
        List<RestaurantLayout> layouts = restaurantLayoutService.getAllLayouts();
        List<RestaurantLayoutDTO> layoutDTOS = layouts.stream().map(layoutMapper::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(layoutDTOS, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN_PERMISSIONS')")
    @RequestMapping(value = "/layouts/active/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> changeActiveLayout(@PathVariable Long id) {
        Optional<RestaurantLayout> existingNewActive = restaurantLayoutService.getById(id);
        RestaurantLayout oldActive = restaurantLayoutService.getActiveLayout();
        if (existingNewActive.isPresent()) {
            RestaurantLayout newActive = existingNewActive.get();
            newActive.setActive(true);
            oldActive.setActive(false);
            restaurantLayoutService.saveLayout(newActive);
            restaurantLayoutService.saveLayout(oldActive);

            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    }


    @PreAuthorize("hasAuthority('ADMIN_PERMISSIONS')")
    @RequestMapping(value = "/layouts/{id}", method = RequestMethod.GET)
    public ResponseEntity<RestaurantLayoutDTO> getLayoutById(@PathVariable Long id) {
        Optional<RestaurantLayout> existingLayout = restaurantLayoutService.getById(id);
        if (existingLayout.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(layoutMapper.toDto(existingLayout.get()), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN_PERMISSIONS')")
    @RequestMapping(value = "/layouts/new", method = RequestMethod.POST)
    public ResponseEntity<RestaurantLayoutDTO> createNewRestaurantLayout(@RequestBody RestaurantLayoutDTO dto) {
        RestaurantLayout newLayout = new RestaurantLayout();
        newLayout.setActive(dto.isActive());
        List<Tables> tables = new ArrayList<>();
        for (TableLayoutDTO t : dto.getTables()) {
            Optional<Tables> table = tablesService.findById(t.getId());
            if (table.isEmpty()) {
                Tables newTable = new Tables();
                newTable.setActive(true);
                newTable.setRestaurantRoom(Room.GROUND_FLOOR);
                newTable = tablesService.save(newTable);
                tables.add(newTable);
            } else {
                tables.add(table.get());
            }

        }
        newLayout.setTableCoordinates(dto.getCoordinates());
        newLayout.setTables(tables);
        newLayout = restaurantLayoutService.addNewLayout(newLayout);

        return new ResponseEntity<>(layoutMapper.toDto(newLayout), HttpStatus.CREATED);
    }

}
