package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.*;
import ftn.kts.nvt.restaurantappbackend.model.*;
import ftn.kts.nvt.restaurantappbackend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/waiter")
public class WaiterController {

	@Autowired
	private WaiterService waiterService;

	@Autowired
	private PriceListService priceListService;

	@Autowired
	private OrderedItemService orderedItemService;

	@Autowired
	private TablesService tableService;

	@Autowired
	private OrderService orderService;


	@RequestMapping(value="/closeOrder/{idOrder}", method = RequestMethod.PUT)
	public ResponseEntity<OrderDTO> changeOrderStatus(@PathVariable Long idOrder) throws Exception {

		Order orderChanged = waiterService.closeOrder(idOrder);

		return new ResponseEntity<>(new OrderDTO(orderChanged), HttpStatus.OK);
	}

	@RequestMapping(value = "/findItems/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ItemDTO> findItems(@PathVariable String name) throws Exception {
		List<Item> listItems = waiterService.findItems(name);

		List<ItemDTO> listItemsDto = new ArrayList<>();

		for(Item i : listItems) {
			ItemDTO itemDto = new ItemDTO(i);
			listItemsDto.add(itemDto);
		}
		return listItemsDto;
	}


	@RequestMapping(value = "/findOrderedItem/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderedItemDTO> findOrderedItem(@PathVariable Long id) throws Exception {
		OrderedItem oItem = orderedItemService.findOneById(id);

		return new ResponseEntity<OrderedItemDTO>(new OrderedItemDTO(oItem), HttpStatus.OK);
	}

	@PutMapping(value="/updateOrderedItem", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderedItemDTO> updateOrderedItemData(@RequestBody OrderedItem item){

		OrderedItem i = orderedItemService.update(item,item.getId());

		if( i == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(new OrderedItemDTO(i), HttpStatus.OK);
	}

	@PutMapping(value="/CancelOrderAndItems/{idOrder}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderDTO> cancelOrderAndItems(@PathVariable Long idOrder) throws Exception {

		Order i = waiterService.CancelOrderedAndOrderItems(idOrder);

		if( i == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(new OrderDTO(i), HttpStatus.OK);
	}

	@RequestMapping(value="/findItemActivePriceList/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PriceListDTO> getItemPriceList(@PathVariable Long id){

		PriceList item = priceListService.findOneByIdOfItem(id);

		if(item == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(new PriceListDTO(item), HttpStatus.OK);
	}

	/*
	 * pregled svih narudzbina nekog stola koje nisu zavrsene
	 */
	@RequestMapping(value="/tableOrder/{tableId}")
	public ResponseEntity<List<OrderDTO>> OrderOfTable(@PathVariable Long tableId){

		//trazimo sve ordere koji imaju sto sa ovim id-ijem i narudzbine nisu zavrsene
		List<Order> orders = orderService.getOrdersOfTableWichIsNotFinished(tableId);

		List<OrderDTO> orderiDto = new ArrayList<>();

		for(Order o : orders){
			if(o.getStatus()!=OrderStatus.FINISHED && o.getStatus()!=OrderStatus.CANCELED)
				orderiDto.add(new OrderDTO(o));
		}

		return new ResponseEntity<>(orderiDto, HttpStatus.OK);
	}

	@RequestMapping(value="/findItemActivePrice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderedItemWithPriceDTO>> getItemPrice(@RequestBody List<OrderedItemDTO> orderedItems){

		List<OrderedItemWithPriceDTO> newList = new ArrayList<OrderedItemWithPriceDTO>();
		for (OrderedItemDTO l : orderedItems)
		{
			newList.add(new OrderedItemWithPriceDTO(l.getId(),l.getOrder(), l.getMenuItem(), priceListService.findOneByIdOfItem(l.getMenuItem().getIdItem()).getItemPrice(), l.getQuantity(), l.getOrderedItemStatus()));
		}

		return new ResponseEntity<>(new ArrayList<OrderedItemWithPriceDTO>(newList
		), HttpStatus.OK);
	}

	@RequestMapping(value="/findOrderedItemById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OrderedItemDTO> getOrderedItemById(@PathVariable Long id){

		OrderedItem item = orderedItemService.findOneById(id);

		if(item == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(new OrderedItemDTO(item), HttpStatus.OK);
	}

	/*
	 * KREIRANJE NOVE NARUDZBINE
	 */
	@RequestMapping(value = "/createEmptyNewOrder/{idTable}", method = RequestMethod.POST)
	public ResponseEntity<OrderDTO> createNewOrder(@PathVariable Long idTable){

		Order orderToCreate = new Order();

		orderToCreate.setDateAndTime(LocalDateTime.now());
		orderToCreate.setDeliverForTable(tableService.findOne(idTable));
		orderToCreate.setNote("nema napomena");
		orderToCreate.setStatus(OrderStatus.WAITING); //status je WAITING inicijalno pri kreiranju
		orderToCreate.setTotalPrice(0.0); //inicijalno je null pri kreiranju

		orderService.save(orderToCreate);
		OrderDTO orderDTO = new OrderDTO(orderToCreate);

		return new ResponseEntity<>(orderDTO, HttpStatus.OK);
	}

	
}
