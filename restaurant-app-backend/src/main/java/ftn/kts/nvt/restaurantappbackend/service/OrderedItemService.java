package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.kts.nvt.restaurantappbackend.repository.OrderedItemRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrderedItemService {

    @Autowired
    private OrderedItemRepository orderedItemRepository;

    public List<OrderedItem> findAll() {
        return orderedItemRepository.findAll();
    }

    public OrderedItem findOneById(Long idOrderedItem) {
        return orderedItemRepository.findOneByIdOrderedItem(idOrderedItem);
    }

    public OrderedItem save(OrderedItem orderedItem) {
        return orderedItemRepository.save(orderedItem);
    }

    public void remove(Long idOrderedItem) {
        orderedItemRepository.deleteById(idOrderedItem);
    }

    public OrderedItem update(OrderedItem itemUpdate, Long id){
        Optional<OrderedItem> found = this.orderedItemRepository.findById(id);

        if(found.isPresent()){
            OrderedItem foundItem = found.get();

            foundItem.setOrderedItemStatus(itemUpdate.getOrderedItemStatus());
            foundItem.setId(itemUpdate.getId());
            foundItem.setMenuItem(itemUpdate.getMenuItem());
            foundItem.setOrder(itemUpdate.getOrder());
            foundItem.setIdOrderedItem(itemUpdate.getIdOrderedItem());
            foundItem.setQuantity(itemUpdate.getQuantity());

            return orderedItemRepository.save(foundItem);
        }
        return null;
    }

    public List<OrderedItem> findByOrderId(Long idOrder) {
        return orderedItemRepository.findByOrderId(idOrder);
    }
}
