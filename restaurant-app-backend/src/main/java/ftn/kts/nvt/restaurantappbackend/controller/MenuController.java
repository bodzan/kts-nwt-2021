package ftn.kts.nvt.restaurantappbackend.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.MenuDTO;
import ftn.kts.nvt.restaurantappbackend.dto.PriceListDTO;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.service.ItemService;
import ftn.kts.nvt.restaurantappbackend.service.MenuService;
import ftn.kts.nvt.restaurantappbackend.service.PriceListService;

@RestController
@RequestMapping("api/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private PriceListService priceListService;
	
	/*@Autowired
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}*/
	
	// http metode koje gadjaju ovaj kontroler.
	
	/*
	 * lista svih menija
	 */
	@RequestMapping(value="/allMenues", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<MenuDTO>> getAllMenus(){
		List<Menu> meniji = menuService.findAll();
		
		List<MenuDTO> menijiDto = new ArrayList<>();
		
		for(Menu m: meniji) {
			menijiDto.add(new MenuDTO(m));
		}
		return new ResponseEntity<>(menijiDto, HttpStatus.OK);
	}
	
	/*
	 * pronalazak menija po id-iju
	 */
	@RequestMapping(value="/menuById/{id}")
	public ResponseEntity<MenuDTO> getMenuById(@PathVariable Long id){
		Optional<Menu> m = this.menuService.findById(id);
		
		if(m.isPresent() == false) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(new MenuDTO(m.get()), HttpStatus.OK);
	}
	
	/*
	 * kreiranje menija
	 */
	@RequestMapping(value="/createMenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<MenuDTO> createMenu(@RequestBody MenuDTO newMenu){
		Menu m = new Menu();
		m.setActive(true);
		
		// sve ostale menije proglasim za neaktivne
		List<Menu> meniji = menuService.findAll();
		for(Menu meni: meniji) {
			meni.setActive(false);
			menuService.save(meni);
		}
		
		menuService.save(m);
		return new ResponseEntity<>(new MenuDTO(m), HttpStatus.CREATED);
	}
	
	/*
	 * cuvanje menija 
	 */
	@RequestMapping(value= "/updateMenu", method=RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuDTO> saveMenu(@RequestBody MenuDTO menu){
		
		Optional<Menu> m  = menuService.findById(menu.getIdMenu());
		
		if(m.isPresent() == false) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Menu meni = m.get();
		meni.setActive(menu.isActive());
		
		menuService.save(meni);
		return new ResponseEntity<>(new MenuDTO(meni), HttpStatus.OK);
	}
	
	/*
	 * preuzimanje trenutno aktivnog menija
	 */
	@RequestMapping(value="/activeMenu", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuDTO> getActiveMenu(){
		Optional<Menu> m = menuService.findActiveMenu(true);
		
		if(m.isPresent() == false) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Menu meni = m.get();
		MenuDTO menuDto = new MenuDTO(meni);
		return new ResponseEntity<>(menuDto, HttpStatus.OK);
	}
	
	/*
	 * proglasi trenutni meni neaktivnim >> deaktiviranje menija 
	 */
	@RequestMapping(value="/deactivateMenu", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MenuDTO> deactivateMenu(@RequestBody MenuDTO menu){ //mogao je i samo id menija da se prosledi
		
		//pokusamo pronaci meni sa tim id-ijem
		Optional<Menu> m = menuService.findById(menu.getIdMenu());
		
		if(m.isPresent() == false) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Menu meni = m.get();

		meni.setActive(false);
		menuService.save(meni);
		return new ResponseEntity<>(new MenuDTO(meni), HttpStatus.OK);
	}
	/*
	 * pronadji celu listu itema koji pripadaju ovom meniju i koji su aktivni u meniju
	 */
	@RequestMapping(value="/allActiveItemPriceList", method= RequestMethod.GET)
	public  ResponseEntity<List<PriceListDTO>> getAllActiveItemPriceList() {
		
		//trenutno aktivni meni
		Optional<Menu> m = menuService.findActiveMenu(true);

		if(m.isPresent() == false){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		//sad pronadjemo sve itemPriceList kod kojih je id menija isti kao id trenutnog menija
		List<PriceList> priceListFromMenu = menuService.findAllWithMenuObj(m.get().getIdMenu());
		
		List<PriceListDTO> activePriceListDTO = new ArrayList<>();
		
		for(PriceList pl : priceListFromMenu) {
			if(pl.isActive() == true) {
				activePriceListDTO.add(new PriceListDTO(pl));
			}
		}
	
		return new ResponseEntity<>(activePriceListDTO, HttpStatus.OK);
	}
	
	/*
	 * pronadji celu listu itama koji pripadaju meniju ukljucujuci i one koji su neaktivni 
	 */
	@RequestMapping(value="/allItemPriceList", method= RequestMethod.GET)
	public  ResponseEntity<List<PriceListDTO>> getAllItemPriceList() {
		
		//trenutno aktivni meni
		Optional<Menu> menu = menuService.findActiveMenu(true);

		if(menu.isPresent() == false){
			return new  ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Menu m = menu.get();

		//sad pronadjemo sve itemPriceList kod kojih je id menija isti kao id trenutnog menija
		List<PriceList> priceListFromMenu = menuService.findAllWithMenuObj(m.getIdMenu());
		
		List<PriceListDTO> priceListDTO = new ArrayList<>();
		
		for(PriceList pl : priceListFromMenu) {
			priceListDTO.add(new PriceListDTO(pl));
		}
	
		return new ResponseEntity<>(priceListDTO, HttpStatus.OK);
	}
	
	
	/************* POSLOVNA LOGIKA *********************/
	
	/*
	 * dodavanje stavke u ponudu menija
	 */
	@RequestMapping(value = "/addMenuItem", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PriceListDTO> addMenuItem(@RequestBody PriceListDTO priceListDTO){
		//u trenutno aktivni meni dodajemo ove stavke>> trazimo trenutno aktivni meni
		Optional<Menu> menu = menuService.findActiveMenu(true);


		Menu activeMenu = menu.get();
		Item itemToAdd = itemService.findOneById(priceListDTO.getItem().getIdItem());
		
		if(menu.isPresent() == false  || itemToAdd == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		//treba izvrsiti proveru da li cenovniku stavki menija postoji stavka tog id ija koja je aktivna
		//?????????????????? >> da ne bi bila 2 puta ista dodata stavka

		//proveravamo da li vec u cenovniku postoji takva stavka
		PriceList postojeciCenovnik = priceListService.findActiveItemPriceObjectFromMenu(activeMenu.getIdMenu(), priceListDTO.getItem().getIdItem());

		if(postojeciCenovnik !=  null){
			//aktivna cena za taj item vec postoji definisana u cenovniku
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		PriceList pl = new PriceList();
		pl.setActive(true);
		pl.setDateFrom(LocalDate.now());
		pl.setDateTo(null);
		pl.setItem(itemToAdd);
		pl.setMenu(activeMenu);
		pl.setItemPrice(priceListDTO.getItemPrice());
		
		pl = priceListService.save(pl);
		
		return new ResponseEntity<>(new PriceListDTO(pl), HttpStatus.OK);
	}
	
	/*
	 * logicko brisanje stavke iz ponude menija >> naglasavamo i datum do kad je vazila stavka zavodimo ga...
	 */
	@RequestMapping(value="/deleteItemFromMenu/{itemId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteItemFromMenu(@PathVariable Long itemId){
		//trazimo trenutno aktivni meni
		Optional<Menu> menu = menuService.findActiveMenu(true);

		if(menu.isPresent() == false){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Menu curentActiveMenu = menu.get();
		System.out.println("Meni id je: " + curentActiveMenu.getIdMenu());
		
		//trazimo trenutno aktivnu stavku(sa trenutno aktivnom cenom) menija sa prosledjenim id-ijem stavke koja
		//je u okviru ovog menija (objekat PriceList)
		
		List<PriceList> priceListActive = priceListService.findAllWithItem(itemId);
		boolean pronadjena = false;

		//trenutno aktivna cena te stavke je: 
		for(PriceList p: priceListActive) {
			if(p.isActive() == true && p.getMenu().getIdMenu() == curentActiveMenu.getIdMenu()) {
				System.out.println("Aktivna cena stavke:  " + p.getItem().getName() + " , cena je: " + p.getItemPrice());
				p.setActive(false);
				
				DateTimeFormatter formater = DateTimeFormatter.ofPattern("YYYY-MM-dd");
				p.setDateTo(LocalDate.parse(formater.format(LocalDate.now())));
				
				priceListService.save(p);
				pronadjena = true;
			}
		}

		//nije pronadjen item sa tim id-ijem.. >>nije obrisano
		if(!pronadjena){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/*
	 * promena cene stavke cenovnika
	 */
	@RequestMapping(value ="/changeItemPrice/{idItem}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PriceListDTO> changeItemPrice(@PathVariable Long idItem, @RequestParam(name = "newPriceOfItem") double newPriceOfItem ) {
		
		Optional<Menu> menu = menuService.findActiveMenu(true);

		if(menu.isPresent() == false){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Menu curentActiveMenu = menu.get();

		//za taj meni trazimo sve aktivne iteme kod kojih je id == idItem
		PriceList activePriceListToChange =  priceListService.findActiveItemPriceObjectFromMenu(curentActiveMenu.getIdMenu(), idItem);
		
		//System.out.println("Stavka cenovnika za promenu je: " + activePriceListToChange.getId() + ", naziv stavke: " + activePriceListToChange.getItem().getName() + ", cena je: " + activePriceListToChange.getItemPrice());
		if(activePriceListToChange == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		//proglasimo staru stavku neaktivnom i dodelimo datum do kog je vazila ta cena
		activePriceListToChange.setActive(false);
		
		DateTimeFormatter formater = DateTimeFormatter.ofPattern("YYYY-MM-dd");
		activePriceListToChange.setDateTo(LocalDate.parse(formater.format(LocalDate.now())));
		priceListService.save(activePriceListToChange);
		
		//formiramo novu identicnu stavku koju cemo dodati sa tim da ce ta stavka biti
		//aktivna od datuma promene cene
		PriceList newPriceList = new PriceList();
		newPriceList.setActive(true);
		newPriceList.setDateFrom(LocalDate.parse(formater.format(LocalDate.now())));
		newPriceList.setDateTo(null);
		newPriceList.setItem(activePriceListToChange.getItem());
		newPriceList.setItemPrice(newPriceOfItem);
		newPriceList.setMenu(activePriceListToChange.getMenu());
		priceListService.save(newPriceList);
		
		return new ResponseEntity<>(new PriceListDTO(newPriceList), HttpStatus.OK);
	}
}
