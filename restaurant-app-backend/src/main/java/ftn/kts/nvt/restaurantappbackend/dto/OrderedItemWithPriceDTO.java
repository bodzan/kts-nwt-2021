package ftn.kts.nvt.restaurantappbackend.dto;

import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;

import java.util.Objects;

public class OrderedItemWithPriceDTO {
	private Long idOrderedItem;

	private OrderDTO order;
	private ItemDTO menuItem;
	private double price;
	private int quantity;
	private MenuItemStatus orderedItemStatus;

	public OrderedItemWithPriceDTO(OrderDTO order, ItemDTO menuItem, double price, int quantity, MenuItemStatus orderedItemStatus) {
		this.order = order;
		this.menuItem = menuItem;
		this.price = price;
		this.quantity = quantity;
		this.orderedItemStatus = orderedItemStatus;
	}

	public OrderedItemWithPriceDTO(Long idOrderedItem, OrderDTO order, ItemDTO menuItem, double price, int quantity, MenuItemStatus orderedItemStatus) {
		this.idOrderedItem = idOrderedItem;
		this.order = order;
		this.menuItem = menuItem;
		this.price = price;
		this.quantity = quantity;
		this.orderedItemStatus = orderedItemStatus;


	}

	public Long getId() {
		return idOrderedItem;
	}

	public void setId(Long id) {
		this.idOrderedItem = id;
	}

	public OrderDTO getOrder() {
		return order;
	}

	public void setOrder(OrderDTO order) {
		this.order = order;
	}

	public ItemDTO getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(ItemDTO menuItem) {
		this.menuItem = menuItem;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public MenuItemStatus getOrderedItemStatus() {
		return orderedItemStatus;
	}

	public void setOrderedItemStatus(MenuItemStatus orderedItemStatus) {
		this.orderedItemStatus = orderedItemStatus;
	}

}
