package ftn.kts.nvt.restaurantappbackend.model;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class Cook extends User {

    // @Column(name = "dishesInPreparation")
    private ArrayList<Integer> dishesInPreparation;

    public void ChangeOrderStatus(Order o, OrderStatus os){
        //TODO
    }

    public void AcceptDish(int id){
        //TODO
    }

    public void ChangeDishStatus(OrderStatus os){
        //TODO
    }

    public ArrayList<Integer> getDishesInPreparation() {
		return dishesInPreparation;
	}

	public void setDishesInPreparation(ArrayList<Integer> dishes) {
		this.dishesInPreparation = dishes;
	}

}
