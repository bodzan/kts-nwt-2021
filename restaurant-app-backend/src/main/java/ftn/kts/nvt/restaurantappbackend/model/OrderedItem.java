package ftn.kts.nvt.restaurantappbackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class OrderedItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idOrderedItem;
	
	@ManyToOne(fetch= FetchType.EAGER)
	private Order order;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Item menuItem;
	
	@Column(nullable = false)
	private int quantity;
	
	@Enumerated(EnumType.STRING)
	@Column(name="orderedItemStatus", nullable = false )
	private MenuItemStatus orderedItemStatus;
	
	public OrderedItem() {
		
	}
	

	public Long getId() {
		return idOrderedItem;
	}


	public void setId(Long id) {
		this.idOrderedItem = id;
	}


	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Item getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(Item menuItem) {
		this.menuItem = menuItem;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public Long getIdOrderedItem() {
		return idOrderedItem;
	}


	public void setIdOrderedItem(Long idOrderedItem) {
		this.idOrderedItem = idOrderedItem;
	}


	public MenuItemStatus getOrderedItemStatus() {
		return orderedItemStatus;
	}


	public void setOrderedItemStatus(MenuItemStatus orderedItemStatus) {
		this.orderedItemStatus = orderedItemStatus;
	}


	@Override
	public String toString() {
		return "OrderedItem [idOrderedItem=" + idOrderedItem + ", order=" + order + ", menuItem=" + menuItem
				+ ", quantity=" + quantity + ", orderedItemStatus=" + orderedItemStatus + "]";
	}
	
	
	
}
