package ftn.kts.nvt.restaurantappbackend.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Salary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private double currentSalary;

    @OneToMany
    private List<SalaryChange> salaryChangeHistory;

    public Salary(double newSalaryCurrent) {
        this.currentSalary = newSalaryCurrent;
        this.salaryChangeHistory = new ArrayList<>();
    }
}
