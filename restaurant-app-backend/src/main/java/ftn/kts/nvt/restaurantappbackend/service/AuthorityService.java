package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Authority;
import ftn.kts.nvt.restaurantappbackend.repository.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService {

    @Autowired
    public AuthorityRepository authorityRepository;


    public Authority getAuthorityByName(String name) {
        return authorityRepository.findByName(name);
    }
}
