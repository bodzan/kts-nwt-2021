package ftn.kts.nvt.restaurantappbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AdminDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String role;

    private double startSalary;

    private Date employmentDate;

}
