package ftn.kts.nvt.restaurantappbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderedItemRepository extends JpaRepository<OrderedItem, Long>{

    OrderedItem findOneByIdOrderedItem(Long idOrderedItem);

    /*selektuje orderItem objekte koji imaju idItema kao sto je prosledjen*/
    @Query("select oi from OrderedItem oi join fetch oi.menuItem i where i.idItem=?1")
    List<OrderedItem> findByIdOfItem(Long idItem);

    @Query("select oi from OrderedItem oi join fetch oi.order i where i.idOrder=?1")
    List<OrderedItem> findByOrderId(Long orderId);
}
