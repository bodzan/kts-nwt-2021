package ftn.kts.nvt.restaurantappbackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class RestaurantLayout {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private boolean active;

    @Column(name = "coordinates", nullable = false)
    private String tableCoordinates;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "layout_tables",
            joinColumns = @JoinColumn(name = "layout_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "table_id", referencedColumnName = "idTable"))
    private List<Tables> tables;

}
