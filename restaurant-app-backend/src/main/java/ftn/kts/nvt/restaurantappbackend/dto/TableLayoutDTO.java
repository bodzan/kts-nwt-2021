package ftn.kts.nvt.restaurantappbackend.dto;

import ftn.kts.nvt.restaurantappbackend.model.Room;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TableLayoutDTO {
    private Long id;
    private Room restaurantRoom;
}
