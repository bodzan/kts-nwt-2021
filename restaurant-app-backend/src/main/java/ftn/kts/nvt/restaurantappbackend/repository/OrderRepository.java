package ftn.kts.nvt.restaurantappbackend.repository;


import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import org.springframework.data.jpa.repository.JpaRepository;

import ftn.kts.nvt.restaurantappbackend.model.Order;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long>{


	Order findOneByIdOrder(Long idOrder);

	@Query("select o from Order o join fetch o.deliverForTable d where d.idTable=?1")
	public List<Order> findAllWithDeliver(Long idTable);



}
