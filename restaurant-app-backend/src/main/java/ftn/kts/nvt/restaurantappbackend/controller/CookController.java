package ftn.kts.nvt.restaurantappbackend.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ftn.kts.nvt.restaurantappbackend.dto.CookDTO;
import ftn.kts.nvt.restaurantappbackend.dto.ItemDTO;
import ftn.kts.nvt.restaurantappbackend.dto.OrderedItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.Cook;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Kitchen;
import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.service.CookService;
import ftn.kts.nvt.restaurantappbackend.service.OrderedItemService;
import ftn.kts.nvt.restaurantappbackend.service.UserService;


@RestController
@RequestMapping("/api/cooks")
public class CookController {
    
    @Autowired
    private CookService cookService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderedItemService orderedItemService;

    private Kitchen kitchen = new Kitchen();


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<CookDTO>> getAllCooks(){
        List<User> c = userService.getAllCooks();
        List<CookDTO> cDTOs = new ArrayList<>();

        for (User cook : c) {
        	CookDTO novi = new CookDTO(cook);
            cDTOs.add(novi);
        }

        return new ResponseEntity<>(cDTOs, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CookDTO> getCook(@PathVariable String id){
		Long l = Long.parseLong(id);
		User c = cookService.findById(l);

        if (c == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        CookDTO cDTO = new CookDTO(c.getId(),c.getFirstName(),c.getLastName(),c.getEmail(),c.getPassword(),c.getEmploymentDate(),c.getSalary(),c.getLastPasswordResetDate(),c.getRoles());
        return new ResponseEntity<>(cDTO, HttpStatus.OK);

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<CookDTO> createCook(@RequestBody CookDTO cDTO){
		User cook = new User();
        try {
        	cook.setId(cDTO.getId());
            cook.setFirstName(cDTO.getFirstName());
            cook.setLastName(cDTO.getLastName());
            cook.setEmail(cDTO.getEmail());
            cook.setPassword(cDTO.getPassword());
            cook.setEmploymentDate(cDTO.getEmploymentDate());
            cook.setSalary(cDTO.getSalary());
            cook.setLastPasswordResetDate(cDTO.getLastPasswordResetDate());
            cook.setRoles(cDTO.getRoles());
        	//cook.setDishesInPreparation(cDTO.getDishesInPreparation());
            
            User test = cookService.save(cook);
            
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(cDTO, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<CookDTO> deleteCookById(@PathVariable String id) throws Exception{
		Long l = Long.parseLong(id);
		User c = cookService.findById(l);

        if (c == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        cookService.deleteById(l);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    // Metode vezane za kuhinju i jela
    @RequestMapping(value = "/waitingDishes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<OrderedItemDTO>> addAllWaitingDishes(){

        List<OrderedItem> orderedItems = orderedItemService.findAll();
        List<OrderedItemDTO> oiDTOlist = new ArrayList<OrderedItemDTO>();


		for (OrderedItem orderedItem : orderedItems) {
            if(orderedItem.getOrderedItemStatus().equals(MenuItemStatus.WAITING) && orderedItem.getMenuItem().getCategory().equals(OrderCategory.DISH)){
                //kitchen.newDish(orderedItem);
                OrderedItemDTO oi = new OrderedItemDTO(orderedItem);
                oiDTOlist.add(oi);
            }
        }
		return new ResponseEntity<>(oiDTOlist,HttpStatus.OK);
	}


    @RequestMapping(value = "/dishesInPreparation", method = RequestMethod.GET)
	public ResponseEntity<List<OrderedItemDTO>> getAllPreparingDishes(){

        List<OrderedItem> orderedItems = orderedItemService.findAll();
        List<OrderedItemDTO> oiDTOlist = new ArrayList<OrderedItemDTO>();


		for (OrderedItem orderedItem : orderedItems) {
            if(orderedItem.getOrderedItemStatus().equals(MenuItemStatus.PREPARING) && orderedItem.getMenuItem().getCategory().equals(OrderCategory.DISH)){
                OrderedItemDTO oi = new OrderedItemDTO(orderedItem);
                oiDTOlist.add(oi);
            }
        }
        // treba da se vrati i id(ili neki drugi podatak) kuvara koji priprema jelo
		return new ResponseEntity<>(oiDTOlist,HttpStatus.OK);
	}


}
