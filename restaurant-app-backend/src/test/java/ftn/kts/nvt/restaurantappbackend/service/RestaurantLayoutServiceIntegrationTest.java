package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.RestaurantLayout;
import ftn.kts.nvt.restaurantappbackend.model.Room;
import ftn.kts.nvt.restaurantappbackend.model.Tables;
import ftn.kts.nvt.restaurantappbackend.repository.RestaurantLayoutRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.LayoutConstants.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RestaurantLayoutServiceIntegrationTest {

    @Autowired
    private RestaurantLayoutRepository restaurantLayoutRepository;


    @BeforeEach
    void setUp() {
    }

    @Test
    void addNewLayout() {
        RestaurantLayout newLayout = new RestaurantLayout();
        newLayout.setActive(false);
        Tables t1 = new Tables(LAYOUT_TABLE_ID1, Room.GROUND_FLOOR, null, true);
        Tables t2 = new Tables(LAYOUT_TABLE_ID2, Room.GROUND_FLOOR, null, true);
        Tables t3 = new Tables(LAYOUT_TABLE_ID3, Room.GROUND_FLOOR, null, true);
        newLayout.setTables(Arrays.asList(t1, t2, t3));

        newLayout = restaurantLayoutRepository.save(newLayout);

        assertEquals(NEW_LAYOUT_ID, newLayout.getId());

    }

    @Test
    void getActiveLayout() {
        RestaurantLayout activeLayout = restaurantLayoutRepository.findByActiveIsTrue();

        assertEquals(ACTIVE_LAYOUT_ID, activeLayout.getId());

    }

    @Test
    void getAllLayouts() {
        List<RestaurantLayout> layouts = restaurantLayoutRepository.findAll();

        assertEquals(NUMBER_OF_LAYOUTS, layouts.size());
    }

    @Test
    void getById() {
        RestaurantLayout layout = restaurantLayoutRepository.getById(ACTIVE_LAYOUT_ID);

        assertEquals(ACTIVE_LAYOUT_ID, layout.getId());

    }
}