package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Authority;
import ftn.kts.nvt.restaurantappbackend.repository.AuthorityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static ftn.kts.nvt.restaurantappbackend.constants.AuthorityConstants.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthorityServiceIntegrationTest {

    @Autowired
    private AuthorityRepository authorityRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAuthorityByName() {
        Authority existingAuthority = authorityRepository.findByName(MANAGER_AUTHORITY);

        assertEquals(MANAGER_AUTHORITY, existingAuthority.getName());
    }
}