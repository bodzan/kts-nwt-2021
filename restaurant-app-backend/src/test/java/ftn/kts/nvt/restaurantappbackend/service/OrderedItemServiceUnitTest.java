package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import ftn.kts.nvt.restaurantappbackend.repository.OrderedItemRepository;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static ftn.kts.nvt.restaurantappbackend.constants.OrderedItemServiceConstants.*;
import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class OrderedItemServiceUnitTest {

    @Autowired
    private OrderedItemService orderedItemService;

    @MockBean
    private OrderedItemRepository orderedItemRepository;


    // @Test
    // public void testFindOneById(){
    //     OrderedItem oi = new OrderedItem();
    //     oi.setId(3L);
    //     oi.setQuantity(1);
    //     oi.setOrderedItemStatus(MenuItemStatus.WAITING);
    //     Mockito.when(orderedItemRepository.findOneByIdOrderedItem(ORDERED_ITEM_ID)).thenReturn(oi);

    //     assertEquals(oi, orderedItemService.findOneById(ORDERED_ITEM_ID));

    // }

    
}
