package ftn.kts.nvt.restaurantappbackend.constants;

public class AuthorityConstants {

    public static final String CREATE_LAYOUT_AUTHORITY_NAME = "ADMIN_PERMISSIONS";
    public static final String CHANGE_LAYOUT_AUTHORITY_NAME = "CHANGE_LAYOUT_PERMISSION";
    public static final String MANAGER_AUTHORITY = "MANAGER_PERMISSIONS";
}
