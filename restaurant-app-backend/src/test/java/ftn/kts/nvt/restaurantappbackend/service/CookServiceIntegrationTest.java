package ftn.kts.nvt.restaurantappbackend.service;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static ftn.kts.nvt.restaurantappbackend.constants.CookServiceConstants.*;
import ftn.kts.nvt.restaurantappbackend.model.Cook;
import ftn.kts.nvt.restaurantappbackend.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class CookServiceIntegrationTest {

	@Autowired
	private CookService cookService;
	
	@Test
	public void testFindById() {
		User found = cookService.findById(COOK_ID);
		assertEquals(COOK_ID, found.getId());
	}

	// @Test
	// public void testFindByIdNotExisting() {
	// 	//User found = cookService.findById(COOK_ID_NON_EXISTING);
	// 	assertNull(cookService.findById(COOK_ID_NON_EXISTING));
	// }


	@Test
	public void testDelete() throws Exception {
		cookService.deleteById(COOK_ID);
		User found = cookService.findById(COOK_ID);
		assertEquals(true, found.isDeleted());
	}

}
