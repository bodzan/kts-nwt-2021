package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.constants.ItemConstants;
import ftn.kts.nvt.restaurantappbackend.dto.AuthTokenDTO;
import ftn.kts.nvt.restaurantappbackend.dto.ItemDTO;
import ftn.kts.nvt.restaurantappbackend.dto.LoginDTO;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;
import ftn.kts.nvt.restaurantappbackend.service.ItemService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ItemControllerIntegrationTest {

    @Autowired
    private ItemService itemService;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private String basePath = "/api/items";

    private String accessToken;


    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = testRestTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @Test
    @Order(1)
    public void test_addItem() {

        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        ItemDTO newItem = new ItemDTO(null, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1,
                ItemConstants.DESCRIPTION_1, ItemConstants.IS_ACTIVE_1);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(newItem, headers);

        String path = this.basePath + "/addNewItem";

        ResponseEntity<ItemDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.POST, httpEntity, ItemDTO.class);
        ItemDTO addedItem = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(7L, addedItem.getIdItem());
        assertEquals(ItemConstants.ALL_ITEMS_NUMBER+1, itemService.findAll().size());

        //roll back nakon dodavanja
        itemService.remove(addedItem.getIdItem()); //brisemo dodatu stavku
    }

    @Test
    public void test_getAllItems() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/allItems";

        ResponseEntity<ItemDTO[]> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity,ItemDTO[].class);
        ItemDTO[] allItems = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ItemConstants.ALL_ITEMS_NUMBER, allItems.length);
    }

    @Test
    public void test_getItem_succesfuly() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/findItem/" + ItemConstants.ID_ITEM_1;

        ResponseEntity<ItemDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, ItemDTO.class);
        ItemDTO foundedItem = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ItemConstants.ID_ITEM_1, foundedItem.getIdItem());
    }

    @Test
    public void test_getItem_itemNotFound() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/findItem/" + ItemConstants.ID_NOT_EXIST;

        ResponseEntity<ItemDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, ItemDTO.class);
        ItemDTO foundedItem = responseEntity.getBody();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNull(foundedItem);

    }

    @Test
    public void test_updateItemData_succesfuly() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        String path = this.basePath + "/updateItem";

        Item itemBeforeUpdate = itemService.findOneById(ItemConstants.ID_ITEM_1);

        ItemDTO itemToUpdate = new ItemDTO(ItemConstants.ID_ITEM_1, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1,
                ItemConstants.DESCRIPTION_1, false);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(itemToUpdate, headers);

        ResponseEntity <ItemDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT, httpEntity, ItemDTO.class);
        ItemDTO updatedItem = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //proverimo da li su vrednosti updatovane
        assertFalse(updatedItem.isActive());

        //roll back radimo
        itemService.update(itemBeforeUpdate, ItemConstants.ID_ITEM_1);
    }

    @Test
    public void test_updateItem_itemNotFound(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        String path = this.basePath + "/updateItem";

        ItemDTO itemToUpdate = new ItemDTO(ItemConstants.ID_NOT_EXIST, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1,
                ItemConstants.DESCRIPTION_1, false);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(itemToUpdate, headers);

        ResponseEntity <ItemDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT, httpEntity, ItemDTO.class);
        ItemDTO updatedItem = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

    }

    @Test
    public void test_deleteItem_succesfuly() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        Item itemToDelete = itemService.findOneById(ItemConstants.ID_ITEM_1);

        String path = this.basePath + "/deleteItem/" + ItemConstants.ID_ITEM_1;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //proverimo koliko je brojno stanje itema nakon brisanja

        int numberAfterDelete = this.itemService.findAll().size();
        assertEquals(ItemConstants.ALL_ITEMS_NUMBER-1, numberAfterDelete);

        //radimo roll back dodajemo obrisni item
        itemService.save(itemToDelete);
    }

    @Test
    public void test_deleteItem_itemNotFound() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/deleteItem/" + ItemConstants.ID_NOT_EXIST;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

    }

    @Test
    public void test_logicDeleteItem_succesfuly() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        Item itemToDelete = itemService.findOneById(ItemConstants.ID_ITEM_1);

        String path = this.basePath + "/logicDeleteItem/" + ItemConstants.ID_ITEM_1;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        //proverimo da li je status itema neaktivan
        assertFalse(this.itemService.findOneById(ItemConstants.ID_ITEM_1).isActive());

        //radimo roll back >> updatujemo logicki obrisani item
        itemService.update(itemToDelete, ItemConstants.ID_ITEM_1);
    }

    @Test
    public void test_logicDeleteItem_itemNotFound() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/logicDeleteItem/" + ItemConstants.ID_NOT_EXIST;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

    }
}