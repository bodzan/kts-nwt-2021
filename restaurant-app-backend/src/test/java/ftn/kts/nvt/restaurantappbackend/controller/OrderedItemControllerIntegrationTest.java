package ftn.kts.nvt.restaurantappbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static ftn.kts.nvt.restaurantappbackend.constants.OrderedItemServiceConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;

import ftn.kts.nvt.restaurantappbackend.dto.AuthTokenDTO;
import ftn.kts.nvt.restaurantappbackend.dto.CookDTO;
import ftn.kts.nvt.restaurantappbackend.dto.LoginDTO;
import ftn.kts.nvt.restaurantappbackend.dto.OrderedItemDTO;
import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import ftn.kts.nvt.restaurantappbackend.service.OrderedItemService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class OrderedItemControllerIntegrationTest {

    @Autowired
    private OrderedItemService orderedItemService;

    @Autowired
    private TestRestTemplate restTemplate;

    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = restTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }


    @Test
    public void testGetAllOrderedItem() {
        
        login(WAITER_EMAIL, WAITER_PASSWORD);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        //headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<OrderedItemDTO>> oiResponse = new ParameterizedTypeReference<List<OrderedItemDTO>>() {};

        ResponseEntity<List<OrderedItemDTO>> responseEntity = restTemplate
				.exchange("/api/orderedItem/getAllOrderedItem",HttpMethod.GET, httpEntity, oiResponse);

		List<OrderedItemDTO> ordereditems = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(FIND_ALL_NUMBER_OF_ITEMS, ordereditems.size());

    }

    @Test
    public void testGetOrderedItemByOrderId() {

        login(WAITER_EMAIL, WAITER_PASSWORD);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<OrderedItemDTO>> oiResponse = new ParameterizedTypeReference<List<OrderedItemDTO>>() {};

        ResponseEntity<List<OrderedItemDTO>> responseEntity = restTemplate
				.exchange("/api/orderedItem/getOrderedItemById/1",HttpMethod.GET, httpEntity, oiResponse);

        List<OrderedItemDTO> ordereditems = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(FIND_BY_ORDER_ID_NUMBER, ordereditems.size());

    }

    @Test
    public void testUpdateOrderedItemStatusValid(){
        login(COOK_EMAIL, COOK_PASSWORD);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        OrderedItem oibefore = orderedItemService.findOneById(ORDERED_ITEM_ID);
        OrderedItem oiUpdate = new OrderedItem();
        oiUpdate.setId(ORDERED_ITEM_ID);
        oiUpdate.setOrderedItemStatus(MenuItemStatus.PREPARING);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(oiUpdate, headers);

        ResponseEntity<OrderedItemDTO> responseEntity = restTemplate
				.exchange("/api/orderedItem/updateOrderedItemStatus",HttpMethod.PUT, httpEntity, OrderedItemDTO.class);

        OrderedItemDTO oi = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(oi.getOrderedItemStatus(), MenuItemStatus.PREPARING);

    }

    @Test
    public void testUpdateOrderedItemStatusNotValid(){
        login(COOK_EMAIL, COOK_PASSWORD);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        OrderedItem oibefore = orderedItemService.findOneById(ORDERED_ITEM_ID);
        OrderedItem oiUpdate = new OrderedItem();
        oiUpdate.setId(ORDERED_ITEM_ID_NONEXISTANT);
        oiUpdate.setOrderedItemStatus(MenuItemStatus.DONE);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(oiUpdate, headers);

        ResponseEntity<OrderedItemDTO> responseEntity = restTemplate
				.exchange("/api/orderedItem/updateOrderedItemStatus",HttpMethod.PUT, httpEntity, OrderedItemDTO.class);

        OrderedItemDTO oi = responseEntity.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

    }
    
}
