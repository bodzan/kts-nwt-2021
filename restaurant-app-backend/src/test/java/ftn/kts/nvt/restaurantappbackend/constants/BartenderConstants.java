package ftn.kts.nvt.restaurantappbackend.constants;


public class BartenderConstants {
    /** Podaci za unit testove za item ****/

    public static final int allOrderedDrinksPreparing_value = 0;
    public static final int allOrderedDrinksWaiting_value = 2;
    public static final int showAllDrinksToAccept_value = 2;

    public static final int showAllItemsWithCategoryAndStatusDrinkWaiting_value = 2;
    public static final int showAllItemsWithCategoryAndStatusDishWaiting_value = 3;
    public static final int showAllItemsWithCategoryAndStatusDrinkDone_value = 0;
    public static final int showAllItemsWithCategoryAndStatusDishDone_value = 0;

    public static final String bartender_username = "barista";
    public static final String bartender_password = "sifra123";

    public static final int C_getAllItems_value = 2;
    public static final int C_allWaitingDrinks_value = 2;
    public static final int C_allPreparingDrinks_value = 0;
}
