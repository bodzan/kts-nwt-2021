package ftn.kts.nvt.restaurantappbackend.constants;

import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;

public class ItemConstants {
    /** Podaci za unit testove za item ****/

    public static final Long ID_ITEM1 = 1L;
    public static final String ITEM_NAME1 = "riblja corba";
    public static final OrderSubcategory ORDER_SUBCATEGORY1 = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST1 = "";
    public static final String DESCRIPTION1 = "riblja corba sa mixom od 4 vrste recnih riba...";
    public static final OrderCategory ORDER_CATEGORY1 = OrderCategory.DISH;
    public static final boolean IS_ACTIVE1 = true;

    public static final Long ID_ITEM2 = 2L;
    public static final String ITEM_NAME2 = "bela corba";
    public static final OrderSubcategory ORDER_SUBCATEGORY2 = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST2 = "";
    public static final String DESCRIPTION2 = "pileca bela corba sa povrcem..";
    public static final OrderCategory ORDER_CATEGORY2 = OrderCategory.DISH;
    public static final boolean IS_ACTIVE2 = true;

    public static final int ALL_FOUNDED_NUMBER = 2;
    public static final Long ID_NOT_EXIST = 10L;

    //********** Podaci za integraciono testiranje **********/
    public static final Long ID_ITEM_1 = 1L;
    public static final String ITEM_NAME_1 = "pileca supa";
    public static final OrderSubcategory ORDER_SUBCATEGORY_1 = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST_1 = "";
    public static final String DESCRIPTION_1 = "Pileca supa sa povrcem..";
    public static final OrderCategory ORDER_CATEGORY_1 = OrderCategory.DISH;
    public static final boolean IS_ACTIVE_1 = true;

    public static final Long ID_ITEM_2 = 2L;
    public static final String ITEM_NAME_2 = "govedja supa";
    public static final OrderSubcategory ORDER_SUBCATEGORY_2 = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST_2 = "";
    public static final String DESCRIPTION_2 = "Govedja supa sa povrcem..";
    public static final OrderCategory ORDER_CATEGORY_2 = OrderCategory.DISH;
    public static final boolean IS_ACTIVE_2 = true;

    public static final int ALL_ITEMS_NUMBER = 6;

}
