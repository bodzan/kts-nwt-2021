package ftn.kts.nvt.restaurantappbackend.constants;

public class UserConstants {

    public static final long NEW_USER_ID = 5;
    public static final String NEW_USER_EMAIL = "novikonobar";
    public static final String NEW_USER_FIRSTNAME = "Stevan";
    public static final String NEW_USER_LASTNAME = "Stevic";
    public static final String NEW_USER_PASSWORD = "konobar";

    public static final String NEW_USER_ROLE = "Waiter";
    public static final String NEW_USER_ROLE_ID = "5";


    public static final long ADMIN_ID1 = 1;
    public static final String ADMIN_EMAIL1 = "admin";
    public static final String ADMIN_FIRSTNAME1 = "Bojan";
    public static final String ADMIN_LASTNAME1 = "Cakic";
    public static final String ADMIN_PASSWORD1 = "sifra123";

    public static final String ADMIN_PASSWORD_CHANGE = "novasifra1";

    public static final long MANAGER_ID1 = 2;
    public static final String MANAGER_EMAIL1 = "menadzer";
    public static final String MANAGER_FIRSTNAME1 = "Zoran";
    public static final String MANAGER_LASTNAME1 = "Stankovic";
    public static final String MANAGER_PASSWORD1 = "sifra123";

    public static final long WAITER_ID = 4;
    public static final String WAITER_EMAIL = "konobar";
    public static final String WAITER_FIRSTNAME = "Marko";
    public static final String WAITER_LASTNAME = "Markovic";
    public static final String WAITER_PASSWORD = "sifra123";

    public static final long BARTENDER_ID = 3;
    public static final String BARTENDER_EMAIL = "barista";
    public static final String BARTENDER_FIRSTNAME = "Nikola";
    public static final String BARTENDER_LASTNAME = "Nikolic";
    public static final String BARTENDER_PASSWORD = "sifra123";

    public static final long COOK_ID = 5;
    public static final String COOK_EMAIL = "kuvar";
    public static final String COOK_FIRSTNAME = "Ana";
    public static final String COOK_LASTNAME = "Anic";
    public static final String COOK_PASSWORD = "sifra123";

    public static final int GET_ALL_NUMBER_OF_USERS = 5;
    public static final int GET_ALL_PAGE_USERS = 4;
    public static final int GET_ALL_ACTIVE_USERS_NUMBER_OF_USERS = 4;
    public static final int ACTIVE_USERS = 4;

    public static final int NUMBER_OF_WAITERS = 1;
    public static final int NUMBER_OF_ADMINS = 1;
    public static final int NUMBER_OF_BARTENDERS = 1;
    public static final int NEW_NUMBER_OF_BARTENDERS = 2;
    public static final int NUMBER_OF_MANAGERS = 1;
    public static final int NUMBER_OF_COOKS = 0;


    public static final int PAGE_NUMBER = 0;
    public static final int PAGE_SIZE = 4;

    public static final String NON_EXISTENT_EMAIL = "nepostoji@mail.com";

}
