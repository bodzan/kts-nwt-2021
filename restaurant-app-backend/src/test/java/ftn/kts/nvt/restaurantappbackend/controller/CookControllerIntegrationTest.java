package ftn.kts.nvt.restaurantappbackend.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static ftn.kts.nvt.restaurantappbackend.constants.CookControllerConstants.*;
import ftn.kts.nvt.restaurantappbackend.dto.CookDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class CookControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetAllCooks() {
        ResponseEntity<CookDTO[]> responseEntity = restTemplate
				.getForEntity("/api/cooks/all", CookDTO[].class);

		CookDTO[] cooks = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(FIND_ALL_COOKS_NUMBER, cooks.length);

    }

    @Test
    public void testGetCook() {
        ResponseEntity<CookDTO> responseEntity = restTemplate
				.getForEntity("/api/cooks/4", CookDTO.class);

		CookDTO cook = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(cook);
        assertEquals(COOK_ID, cook.getId());

    }

    @Test
    public void testDeleteCookById(){
        
    }
    
}
