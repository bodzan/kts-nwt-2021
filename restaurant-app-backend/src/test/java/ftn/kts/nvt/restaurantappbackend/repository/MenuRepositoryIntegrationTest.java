package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@DataJpaTest
@ActiveProfiles("test")
public class MenuRepositoryIntegrationTest {

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private TestEntityManager entityManager;


    @BeforeEach
    public void setUp() {
        //entityManager.persist(new Menu(MenuConstants.MENU_IS_ACTIVE_1));

    }

    @Test
    public void test_findByIsActive_existsMenu() {
        Optional<Menu> found = this.menuRepository.findByIsActive(MenuConstants.MENU_IS_ACTIVE_1);

        assertTrue(found.isPresent());
        assertEquals(MenuConstants.MENU_ID_1, found.get().getIdMenu());
    }

    @Test
    public void test_findByIsActive_MenuNotExist(){
        Optional<Menu> found = this.menuRepository.findByIsActive(false);

        assertFalse(found.isPresent());
    }

    /*
    testiranje kad meni sa tim id-ije postoji u bazi
        >> treba da vrati taj meni
     */
    @Test
    public void test_findById_existMenu() {
        Optional<Menu> found = this.menuRepository.findById(MenuConstants.MENU_ID_1);

        assertTrue(found.isPresent());
        assertEquals(MenuConstants.MENU_ID_1, found.get().getIdMenu());
    }

    @Test
    public void test_findById_menuNotExist(){
        Optional<Menu> found = this.menuRepository.findById(MenuConstants.MENU_ID_NOT_EXIST);

        assertFalse(found.isPresent());
    }
}