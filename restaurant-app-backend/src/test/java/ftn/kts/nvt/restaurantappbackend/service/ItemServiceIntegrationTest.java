package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.ItemConstants;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.repository.ItemRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ItemServiceIntegrationTest {

    @Autowired
    private ItemService itemService;



    @Test
    @Order(1)
    public void test_findAll() {
        List<Item> found = itemService.findAll();

        assertEquals(ItemConstants.ALL_ITEMS_NUMBER, found.size());
    }

    @Test
    @Order(2)
    public void test_findOneById() {
        Item item = itemService.findOneById(ItemConstants.ID_ITEM_1);
        assertEquals(ItemConstants.ID_ITEM_1, item.getIdItem());
    }

    @Test
    @Order(9)
    public void test_save() {
        Item itemToSave = new Item(ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1, ItemConstants.DESCRIPTION_1, ItemConstants.IS_ACTIVE_1 );

        Item savedItem = itemService.save(itemToSave);

        assertEquals(ItemConstants.ALL_ITEMS_NUMBER+1, itemService.findAll().size());
        assertNotNull(savedItem);
    }

    @Test
    public void test_remove() {

        itemService.remove(ItemConstants.ID_ITEM_1);
        assertEquals(ItemConstants.ALL_ITEMS_NUMBER, itemService.findAll().size());
    }

    @Test
    @Order(3)
    public void test_findById_succesfuly() {
        Optional<Item> found = itemService.findById(ItemConstants.ID_ITEM_1);

        assertNotNull(found.get());
        assertEquals(ItemConstants.ID_ITEM1, found.get().getIdItem());
    }

    @Test
    @Order(4)
    public void test_findById_itemNotFound() {
        Optional<Item> found = itemService.findById(ItemConstants.ID_NOT_EXIST);

        assertFalse(found.isPresent());
    }

    @Test
    @Order(5)
    public void test_logicalRemove_succesfuly() {
        boolean removed = itemService.logicalRemove(ItemConstants.ID_ITEM_1);
        assertTrue(removed);
        //provera da li je postavljena aktivnost na false
        assertFalse(itemService.findById(ItemConstants.ID_ITEM_1).get().isActive());
    }

    @Test
    @Order(6)
    public void test_logicalRemove_itemNotExist() {
        boolean removed = itemService.logicalRemove(ItemConstants.ID_NOT_EXIST);
        assertFalse(removed);
    }

    @Test
    @Order(7)
    public void test_update_succesfuly() {
        Item itemToUpdate =new Item(ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1, ItemConstants.DESCRIPTION_1, false );
        boolean updated = itemService.update(itemToUpdate, ItemConstants.ID_ITEM_1);

        assertTrue(updated);
        //proverimo da li je updatovan status isActive
        assertFalse(itemService.findById(ItemConstants.ID_ITEM_1).get().isActive());

    }

    @Test
    @Order(8)
    public void test_update_itemNotFound() {
        Item itemToUpdate =new Item(ItemConstants.ITEM_NAME_1, ItemConstants.ORDER_CATEGORY_1, ItemConstants.ORDER_SUBCATEGORY_1, ItemConstants.ALERGEN_LIST_1, ItemConstants.DESCRIPTION_1, false );
        boolean updated = itemService.update(itemToUpdate, ItemConstants.ID_NOT_EXIST);

        assertFalse(updated);

    }
}