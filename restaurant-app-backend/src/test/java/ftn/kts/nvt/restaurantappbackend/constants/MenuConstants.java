package ftn.kts.nvt.restaurantappbackend.constants;

import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;

import java.time.LocalDate;

public class MenuConstants {
    public static final Long MENU_ID_1 = 1L;
    public static final boolean MENU_IS_ACTIVE_1 = true;

    public static final Long MENU_ID_2 = 2L;
    public static final boolean MENU_IS_ACTIVE_2 = false;

    public static final Long MENU_ID_NOT_EXIST = 5L;
    public static final int ALL_MENU_SIZE = 2;
    public static final int ALL_FOUNDED_INTEGRATION_TEST = 1;
    public static final int NUMBER_OF_PRICE_LIST = 6;

    /***** priceList object of menu ********/
    public static final Long PRICE_LIST_ID = 7L;
    public static final double ITEM_PRICE = 400;
    public static final LocalDate DATE_FROM = LocalDate.of(2021, 12, 20);
    public static final LocalDate DATE_TO = null;
    public static final boolean IS_ACTIVE = true;

    /***** item object ******/
    public static final Long ID_ITEM = 7L;
    public static final OrderCategory ORDER_CATEGORY = OrderCategory.DISH;
    public static final String NAME = "riblja corba";
    public static final OrderSubcategory ORDER_SUBCATEGORY = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST = "";
    public static final String DESCRIPTION = "riblja corba sa mixom od 4 vrste recnih riba...";

    public static final Long ID_OF_ALLREADY_ADDED_ITEM_TO_MENU = 6L;
    public static final Long ITEM_ID_EXIST = 5L;
}
