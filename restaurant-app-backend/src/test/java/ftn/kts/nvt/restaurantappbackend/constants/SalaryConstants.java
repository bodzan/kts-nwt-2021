package ftn.kts.nvt.restaurantappbackend.constants;

public class SalaryConstants {
    public static final double NEW_SALARY_CURRENT = 62000.0;
    public static final long NEW_SALARY_ID = 5L;
    public static final double NEW_USER_SALARY_CURRENT = 46000.0;

}
