package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.BartenderConstants;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Order;
import ftn.kts.nvt.restaurantappbackend.model.OrderStatus;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.WaiterConstants.*;
import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class WaiterServiceIntegrationTest {

    @Autowired
    private WaiterService waiterService;

    @Test
    public void CancelOrderedAndOrderItems() throws Exception {
        Order found = waiterService.CancelOrderedAndOrderItems(idOrder_editOrder);
        assertEquals(OrderStatus.CANCELED, found.getStatus());

    }

    @Test
    public void closeOrder() throws Exception {
        Order found = waiterService.closeOrder(idOrder_editOrder);
        assertEquals(OrderStatus.DONE, found.getStatus());

    }

    @Test
    public void findItemsNull() throws Exception {
        List<Item> found = waiterService.findItems("");
        assertEquals(null_findAll, found.size());

    }

    @Test
    public void findItems() throws Exception {
        List<Item> found = waiterService.findItems(name_findAll);
        assertEquals(by_name_findAll, found.size());

    }

    
    
}
