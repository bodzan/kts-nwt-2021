package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.ItemConstants;
import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.constants.PriceListConstants;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.repository.PriceListRepostory;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PriceListServiceIntegrationTest {

    @Autowired
    private PriceListService priceListService;

    @Autowired
    private PriceListRepostory priceListRepostory;

    @Autowired
    private MenuService menuService;

    @Test
    @Order(1)
    public void test_findAll() {
        List<PriceList> founded = priceListService.findAll();
        assertEquals(PriceListConstants.ALL_FOUNDED_INTEGRATION, founded.size());
    }

    @Test
    @Order(2)
    public void test_findOneById_succesfuly() {
        PriceList found = priceListService.findOneById(PriceListConstants.PRICE_LIST_ID1);
        assertEquals(PriceListConstants.PRICE_LIST_ID1, found.getId());
        assertNotNull(found);
    }

    @Test
    @Order(3)
    public void test_findOneById_notFound() {
        assertThrows(NullPointerException.class,
                () -> {
            PriceList found = priceListService.findOneById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST);
            assertNull(found);
        });

    }

    @Test
    @Order(9)
    void test_save() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Optional<Menu> menu = menuService.findActiveMenu(true);

        PriceList pl1 = new PriceList(menu.get(), item1, PriceListConstants.ITEM_PRICE_1, PriceListConstants.DATE_FROM_1, PriceListConstants.DATE_TO_1, PriceListConstants.IS_ACTIVE_1);

        PriceList saved = this.priceListService.save(pl1);

        assertNotNull(saved);
        assertEquals(PriceListConstants.PRICE_LIST_ID_1, saved.getId());

        //proverimo da li se ukupan broj priceList-ova povecao za 1
        assertEquals(PriceListConstants.ALL_FOUNDED_INTEGRATION + 1, priceListService.findAll().size());
    }

    @Test
    @Order(10)
    public void test_remove() {
        priceListService.remove(PriceListConstants.PRICE_LIST_ID1);
        //proverimo da li se ukupan broj priceList-ova smanjio
        assertEquals(PriceListConstants.ALL_FOUNDED_INTEGRATION, priceListService.findAll().size());
        //taj priceList ne bi trebalo da vise mozemo da pronadjemo sa findById

        assertThrows(NullPointerException.class, ()->{
            PriceList found = priceListService.findOneById(PriceListConstants.PRICE_LIST_ID1);
            assertNull(found);
        });

    }

    @Test
    @Order(4)
    public void test_findOneWithItem() {
        PriceList found = priceListService.findOneWithItem(PriceListConstants.ID_ITEM1);

        assertEquals(PriceListConstants.ID_ITEM1, found.getItem().getIdItem());
    }

    @Test
    @Order(5)
    public void test_findAllWithItem() {
        List<PriceList> allFound = priceListService.findAllWithItem(PriceListConstants.ID_ITEM1);
        assertEquals(PriceListConstants.ALL_WIHT_ITEM_SIZE, allFound.size());
    }

    @Test
    @Order(6)
    public void test_findActiveItemPriceObjectFromMenu() {
        PriceList activePrice = this.priceListService.findActiveItemPriceObjectFromMenu(MenuConstants.MENU_ID_1, PriceListConstants.ID_ITEM1);
        //da li je to cena koja je trenutno aktivna za taj item
        assertTrue(activePrice.isActive());
        //proverimo da li je dobra cena pronadjena
        assertEquals(PriceListConstants.PRICE_LIST_ID1, activePrice.getId());
        //proverimo da li je to cena za item sa id koji je prosledjen
        assertEquals(PriceListConstants.ID_ITEM1, activePrice.getItem().getIdItem());
    }

    @Test
    @Order(7)
    public void test_update_succesfuly() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Optional<Menu> menu = menuService.findActiveMenu(true);

        PriceList pl1 = new PriceList(menu.get(), item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1,false);

        boolean updated = priceListService.update(pl1, PriceListConstants.PRICE_LIST_ID2);

        assertTrue(updated);

        //proveravamo da li je dobro updatovan
        PriceList updatedPriceList = priceListService.findOneById(PriceListConstants.PRICE_LIST_ID2);
        assertEquals(false, updatedPriceList.isActive());
        assertEquals(PriceListConstants.ITEM_PRICE1, updatedPriceList.getItemPrice());

    }

    @Test
    @Order(8)
    public void test_update_priceListNotExist(){
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Optional<Menu> menu = menuService.findActiveMenu(true);

        PriceList pl1 = new PriceList(menu.get(), item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1,false);

        boolean updated = this.priceListService.update(pl1, PriceListConstants.ID_PRICE_LIST_NOT_EXIST);
        assertFalse(updated);
    }
}