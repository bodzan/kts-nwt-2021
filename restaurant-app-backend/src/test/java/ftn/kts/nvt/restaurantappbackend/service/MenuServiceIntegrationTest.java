package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MenuServiceIntegrationTest {

    @Autowired
    private MenuService menuService;

    @Autowired
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
    }

    @Test
    @Order(1)
    public void test_findById_existMenuWithId() {
        Optional<Menu> found = menuService.findById(MenuConstants.MENU_ID_1);

        assertNotNull(found);
        assertEquals(MenuConstants.MENU_ID_1, found.get().getIdMenu());

    }

    @Test
    @Order(2)
    public void test_findById_MenuWithIdNotExist() {
        Optional<Menu> found = menuService.findById(MenuConstants.MENU_ID_NOT_EXIST);

        assertFalse(found.isPresent());
    }

    @Test
    @Order(3)
    public void test_findAll(){
        List<Menu> allMenus = menuService.findAll();

        assertNotNull(allMenus);
        assertEquals(MenuConstants.ALL_FOUNDED_INTEGRATION_TEST, allMenus.size());

    }

    @Test
    @Order(10)
    public void test_save() {
        Menu m = new Menu(MenuConstants.MENU_IS_ACTIVE_1);

        Menu savedMenu = menuService.save(m);

        assertEquals(m.isActive(), savedMenu.isActive());
        assertNotNull(savedMenu);
        //da li je dodat jedan meni >> broj svih menija povecan za 1
        assertEquals(MenuConstants.ALL_FOUNDED_INTEGRATION_TEST+1, menuRepository.findAll().size());
    }

    @Test
    @Order(11)
    public void test_remove() {
        menuService.remove(MenuConstants.MENU_ID_1);

        //da li je broj menija sada smanjen za 1
        assertEquals(MenuConstants.ALL_FOUNDED_INTEGRATION_TEST, menuRepository.findAll().size());
    }

    @Test
    @Order(6)
    public void test_logicalRemove_menuWithIdExist() {
        boolean logicalyRemoved = menuService.logicalRemove(MenuConstants.MENU_ID_1);

        assertTrue(logicalyRemoved);

        //proveriti da li je oznacen kao obrisan
        Optional<Menu> removedMenu = menuRepository.findByIdMenu(MenuConstants.MENU_ID_1);
        assertFalse(removedMenu.get().isActive());
    }

    @Test
    @Order(7)
    public void test_logicalRemove_menuWithIdNotExist(){
        boolean logicalyRemoved = menuService.logicalRemove(MenuConstants.MENU_ID_NOT_EXIST);

        assertFalse(logicalyRemoved);
    }

    @Test
    @Order(4)
    public void test_findActiveMenu_activeMenuExist() {
        Optional<Menu> activeMenu = menuService.findActiveMenu(MenuConstants.MENU_IS_ACTIVE_1);

        assertNotNull(activeMenu);
        //System.out.print(activeMenu.get().getIdMenu());
        assertEquals(MenuConstants.MENU_ID_1, activeMenu.get().getIdMenu());
    }

    @Test
    @Order(5)
    public void test_findActiveMenu_notActiveMenuNotExist(){
        //za menije koji nisu aktivni ne postoji ni jedan
        Optional<Menu> activeMenu = menuService.findActiveMenu(false);

        assertFalse(activeMenu.isPresent());
    }

    @Test
    @Order(8)
    public void test_update_succesfuly(){
        Menu updateMenu = new Menu(MenuConstants.MENU_IS_ACTIVE_2);

        boolean updated = this.menuService.update(updateMenu, MenuConstants.MENU_ID_1);

        assertTrue(updated);
        //proverimo da li je updatovan
        assertEquals(MenuConstants.MENU_IS_ACTIVE_2, this.menuService.findById(MenuConstants.MENU_ID_1).get().isActive());

    }
    @Test
    @Order(9)
    public void test_update_menuNotExist(){
        Menu updateMenu = new Menu(MenuConstants.MENU_IS_ACTIVE_2);

        boolean updated = this.menuService.update(updateMenu, MenuConstants.MENU_ID_NOT_EXIST);

        assertFalse(updated);

    }
}