package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ftn.kts.nvt.restaurantappbackend.constants.BartenderConstants;
import java.util.List;
import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class BartenderServiceIntegrationTest {

    @Autowired
    private BartenderService bartenderService;

    @Test
	public void allOrderedDrinksPreparing() {
        List<OrderedItem> found = bartenderService.allOrderedDrinksPreparing();
        assertEquals(BartenderConstants.allOrderedDrinksPreparing_value, found.size());
    }

    @Test
    public void allOrderedDrinksWaiting() {
        List<OrderedItem> found = bartenderService.allOrderedDrinksWaiting();
        assertEquals(BartenderConstants.allOrderedDrinksWaiting_value, found.size());

    }

    @Test
    public void showAllDrinksToAccept() {
        List<Item> found = bartenderService.showAllDrinksToAccept();
        assertEquals(BartenderConstants.showAllDrinksToAccept_value, found.size());

    }

    
    
}
