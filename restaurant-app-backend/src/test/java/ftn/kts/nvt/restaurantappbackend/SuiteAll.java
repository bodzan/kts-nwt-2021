package ftn.kts.nvt.restaurantappbackend;


import ftn.kts.nvt.restaurantappbackend.service.UserServiceUnitTest;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;


@Suite
@SelectPackages({"ftn.kts.nvt.restaurantappbackend.service", "ftn.kts.nvt.restaurantappbackend.repository",
                "ftn.kts.nvt.restaurantappbackend.controller"})
@IncludeEngines("junit-jupiter")
public class SuiteAll {
}
