package ftn.kts.nvt.restaurantappbackend.constants;

import ftn.kts.nvt.restaurantappbackend.model.OrderCategory;
import ftn.kts.nvt.restaurantappbackend.model.OrderSubcategory;

import java.time.LocalDate;

public class PriceListConstants {

    /**** Podaci za unit testove* ****/

    //pricelist podaci
    public static final Long PRICE_LIST_ID1 = 1L;
    public static final double ITEM_PRICE1 = 400;
    public static final LocalDate DATE_FROM1 = LocalDate.of(2021, 12, 20);
    public static final LocalDate DATE_TO1 = null;
    public static final boolean IS_ACTIVE1 = true;

    public static final Long PRICE_LIST_ID2 = 2L;
    public static final double ITEM_PRICE2 = 200;
    public static final LocalDate DATE_FROM2 = LocalDate.of(2021, 01, 01);
    public static final LocalDate DATE_TO2 = LocalDate.of(2021, 12,20);
    public static final boolean IS_ACTIVE2 = false;


    //item podaci
    public static final Long ID_ITEM1 = 1L;
    public static final OrderCategory ORDER_CATEGORY1 = OrderCategory.DISH;
    public static final String NAME1 = "riblja corba";
    public static final OrderSubcategory ORDER_SUBCATEGORY1 = OrderSubcategory.SOUPS;
    public static final String ALERGEN_LIST1 = "";
    public static final String DESCRIPTION1 = "riblja corba sa mixom od 4 vrste recnih riba...";

    public static final int ALL_FOUNDED = 2;

    public static final Long ID_PRICE_LIST_NOT_EXIST = 10L;

    /********* PODACI ZA INTEGRACIONE TESTOVE *************/
    public static final int ALL_FOUNDED_INTEGRATION = 6;
    public static  final int ALL_WIHT_ITEM_SIZE = 1;

    //price list objekat
    public static final Long PRICE_LIST_ID_1 = 7L;
    public static final double ITEM_PRICE_1 = 550;
    public static final LocalDate DATE_FROM_1 = LocalDate.of(2021, 12, 20);
    public static final LocalDate DATE_TO_1 = null;
    public static final boolean IS_ACTIVE_1 = true;


}
