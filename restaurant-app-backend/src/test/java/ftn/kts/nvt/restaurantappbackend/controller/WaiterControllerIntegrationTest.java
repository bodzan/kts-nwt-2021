package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.*;
import ftn.kts.nvt.restaurantappbackend.model.OrderStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.WaiterConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
class WaiterControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = restTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void getAllItems() {
        login(waiter_username, waiter_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<OrderDTO>> layoutResponse = new ParameterizedTypeReference<List<OrderDTO>>() {};

        ResponseEntity<List<OrderDTO>> responseEntity = restTemplate.exchange("/api/waiter/tableOrder/" + C_tableOrder_idTable, HttpMethod.GET, httpEntity, layoutResponse);

        List<OrderDTO> allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
        assertEquals(C_tableOrder_value, allItems.size());
    }

    @Test
    void findOrderedItemById() {
        login(waiter_username, waiter_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<OrderedItemDTO> layoutResponse = new ParameterizedTypeReference<OrderedItemDTO>() {};

        ResponseEntity<OrderedItemDTO> responseEntity = restTemplate.exchange("/api/waiter/findOrderedItemById/" + C_findOrderedItemById_idOrdered, HttpMethod.GET, httpEntity, layoutResponse);

        OrderedItemDTO allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
    }

    @Test
    void createEmptyNewOrder() {
        login(waiter_username, waiter_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<OrderDTO> layoutResponse = new ParameterizedTypeReference<OrderDTO>() {};

        ResponseEntity<OrderDTO> responseEntity = restTemplate.exchange("/api/waiter/createEmptyNewOrder/" + C_createNewOrder_idTable, HttpMethod.POST, httpEntity, layoutResponse);

        OrderDTO allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
    }


}