package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.dto.*;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.MenuItemStatus;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;
import ftn.kts.nvt.restaurantappbackend.service.ItemService;
import ftn.kts.nvt.restaurantappbackend.service.MenuService;
import ftn.kts.nvt.restaurantappbackend.service.PriceListService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.MANAGER_EMAIL1;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.MANAGER_PASSWORD1;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class MenuControllerIntegrationTest {

    @Autowired
    private MenuService menuService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PriceListService priceListService;

    @Autowired
    private TestRestTemplate testRestTemplate;

    private String basePath = "/api/menu";

    private String accessToken;


    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = testRestTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @Test
    public void test_getAllMenus() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = basePath + "/allMenues";

        ResponseEntity<MenuDTO[]> restEntity = testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, MenuDTO[].class);

        MenuDTO[] allMenues = restEntity.getBody();

        //provera odgovora servera
        assertEquals(HttpStatus.OK, restEntity.getStatusCode());
        assertEquals(MenuConstants.ALL_FOUNDED_INTEGRATION_TEST, allMenues.length);
    }

    @Test
    public void test_getMenuById_succesfulyFound() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/menuById/" + MenuConstants.MENU_ID_1;

        ResponseEntity<MenuDTO> responseEntity = testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, MenuDTO.class);

        MenuDTO menu = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MenuConstants.MENU_ID_1, menu.getIdMenu());
    }

    @Test
    public void test_getMenuById_notFound(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/menuById/" + MenuConstants.MENU_ID_NOT_EXIST;

        ResponseEntity<MenuDTO> responseEntity = testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, MenuDTO.class);

        MenuDTO menu = responseEntity.getBody();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        assertNull(menu);
    }

    @Test
    public void test_createMenu(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        //broj menija koji su dodati
        int menuSizeBefore = this.menuService.findAll().size();

        //zbog roll backa cuvamo trenutno aktivni meni
        Optional<Menu> currentActiveMenu = this.menuService.findActiveMenu(true);

        MenuDTO m = new MenuDTO();
        m.setActive(true);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(m, headers);

        String path = this.basePath + "/createMenu";
        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.POST, httpEntity, MenuDTO.class);

        MenuDTO menu = responseEntity.getBody();
        System.out.println("Id menija je: " + menu.getIdMenu());
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertTrue(menu.isActive());

        //provera da li su ostali meniji proglaseni neaktivnim
        List<Menu> menuList = menuRepository.findAll();

        for(Menu meni : menuList){
            if(meni.getIdMenu() != menu.getIdMenu())
                assertFalse(meni.isActive());
        }

        //proverimo da li je broj sacuvanih menija veci za 1
        int menuSizeAfter = menuService.findAll().size();
        assertEquals(menuSizeBefore+1, menuSizeAfter);

        //*****nakon ovoga treba uraditi roll back****
        //brisemo sacuvani meni
        menuService.remove(menu.getIdMenu());

        //vracamo da je prethodni aktivan sada aktivan - update
        currentActiveMenu.get().setActive(true);
        this.menuService.save(currentActiveMenu.get());


    }

    @Test
    public void test_updateMenu_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        //meni pre update
        Optional<Menu> menuBefore = menuService.findById(MenuConstants.MENU_ID_1);

        MenuDTO newMenu = new MenuDTO();
        newMenu.setIdMenu(MenuConstants.MENU_ID_1);
        newMenu.setActive(false);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(newMenu, headers);

        String path = this.basePath + "/updateMenu";

        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT,
                httpEntity, MenuDTO.class);

        MenuDTO menuUpdated = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(menuUpdated.isActive());

        //roll back >> update ovog menija
        menuService.update(menuBefore.get(), MenuConstants.MENU_ID_1);
        //System.out.println("Vrednost aktivnosti je: " + menuService.findById(MenuConstants.MENU_ID_1).get().isActive());
    }

    @Test
    public void test_updateMenu_notFound(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        MenuDTO newMenu = new MenuDTO();
        newMenu.setIdMenu(MenuConstants.MENU_ID_NOT_EXIST);
        newMenu.setActive(true);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(newMenu, headers);

        String path = this.basePath + "/updateMenu";

        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT,
                httpEntity, MenuDTO.class);

        MenuDTO menuUpdated = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(menuUpdated);
    }

    @Test
    public void test_getActiveMenu_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path  = this.basePath + "/activeMenu";

        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, MenuDTO.class);
        MenuDTO activeMenu = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MenuConstants.MENU_ID_1, activeMenu.getIdMenu());

    }

    @Test
    public void test_deactivateMenu_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        //pre deaktivacije sacuvano
        Optional<Menu> menuBeforeDeactivate = menuService.findById(MenuConstants.MENU_ID_1);

        MenuDTO meni = new MenuDTO();
        meni.setActive(false);
        meni.setIdMenu(MenuConstants.MENU_ID_1);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(meni, headers);

        String path = this.basePath + "/deactivateMenu";

        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT,
                httpEntity, MenuDTO.class);

        MenuDTO menuDeactive = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(menuDeactive.isActive());

        //rollback >> ponovo aktiviramo taj meni
        menuService.update(menuBeforeDeactivate.get(), MenuConstants.MENU_ID_1);
    }

    @Test
    public void test_deactivateMenu_notFound(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        MenuDTO meni = new MenuDTO();
        meni.setActive(false);
        meni.setIdMenu(MenuConstants.MENU_ID_NOT_EXIST);

        HttpEntity<Object> httpEntity = new HttpEntity<Object>(meni, headers);

        String path = this.basePath + "/deactivateMenu";

        ResponseEntity<MenuDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT,
                httpEntity, MenuDTO.class);

        MenuDTO menuDeactive = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(menuDeactive);
    }

    @Test
    public void test_getAllActiveItemPriceList_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        String path = this.basePath + "/allActiveItemPriceList";

        ResponseEntity<PriceListDTO[]> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity,PriceListDTO[].class);

        PriceListDTO[] priceLists = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MenuConstants.NUMBER_OF_PRICE_LIST, priceLists.length);
    }

    //@Test
    /*public void test_getAllActiveItemPriceList_menuNotFound(){

        //za slucaj da nema aktivnih menija
        //deaktiviracemo meni za testiranje
        Optional<Menu> activeMenu = this.menuService.findActiveMenu(true);

        Menu deactivate = activeMenu.get();
        deactivate.setActive(false);
        menuService.update(deactivate, activeMenu.get().getIdMenu());

        String path = this.basePath + "/allActiveItemPriceList";

        ResponseEntity<PriceListDTO[]> responseEntity = this.testRestTemplate.getForEntity(path, PriceListDTO[].class);

        PriceListDTO[] priceList = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(priceList);

        //roll back >> ponovo aktiviramo meni
        menuService.update(activeMenu.get(), activeMenu.get().getIdMenu());
    }*/

    @Test
    public void test_getAllItemPriceList_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        //preuzimamo da vidimo da li ima aktivnog menija
        Optional<Menu> MENI = this.menuService.findActiveMenu(true);
        System.out.println("Meni je aktivan: " + MENI.get().isActive() + "iD : " + MENI.get().getIdMenu());

        String path = this.basePath + "/allItemPriceList";

        ResponseEntity<PriceListDTO[]> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.GET, httpEntity, PriceListDTO[].class);

        PriceListDTO[] priceLists = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(MenuConstants.NUMBER_OF_PRICE_LIST, priceLists.length);
    }

    /******** TESTOVI DEO SA POSLOVNOM LOGIKOM *********/

    /*
    DODAVANJE STAVKE U PONUDU MENIJA TEST
     */
    //za proveru da li ne postoji aktivni meni test je kao i prethodni pre ovoga.

    @Test
    public void test_addMenuItem_itemNotExist(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        String path = this.basePath + "/addMenuItem";

        //kreiramo objekat priceList-a
        MenuDTO menuDTO = new MenuDTO(this.menuService.findActiveMenu(true).get());
        //ovo je item koji jos nije dodat u bazi
        ItemDTO itemDTO = new ItemDTO(MenuConstants.ID_ITEM, MenuConstants.ORDER_CATEGORY, MenuConstants.NAME, MenuConstants.ORDER_SUBCATEGORY, MenuConstants.ALERGEN_LIST, MenuConstants.DESCRIPTION, true);

        PriceListDTO newPriceList = new PriceListDTO(MenuConstants.PRICE_LIST_ID, menuDTO, itemDTO, MenuConstants.ITEM_PRICE, MenuConstants.DATE_FROM, MenuConstants.DATE_TO, MenuConstants.IS_ACTIVE);

        HttpEntity httpEntity = new HttpEntity(newPriceList, headers);
        ResponseEntity<PriceListDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.POST, httpEntity, PriceListDTO.class );

        PriceListDTO priceList = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(priceList);
    }

    /*
    item sa tim id-ijem vec ima definisanu aktivnu cenu u tom meniju, odnosno vec je dodat u taj meni
     */
    @Test
    public void test_addMenuItem_menuItemAlreadyAdd() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        String path = this.basePath + "/addMenuItem";

        //kreiramo objekat priceList-a
        MenuDTO menuDTO = new MenuDTO(this.menuService.findActiveMenu(true).get());
        //ovo je item koji je vec dodat u meni
        ItemDTO itemDTO = new ItemDTO(this.itemService.findOneById(MenuConstants.ID_OF_ALLREADY_ADDED_ITEM_TO_MENU));

        PriceListDTO newPriceList = new PriceListDTO(MenuConstants.PRICE_LIST_ID, menuDTO, itemDTO, MenuConstants.ITEM_PRICE, MenuConstants.DATE_FROM, MenuConstants.DATE_TO, MenuConstants.IS_ACTIVE);

        HttpEntity httpEntity = new HttpEntity(newPriceList, headers);

        ResponseEntity<PriceListDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.POST, httpEntity, PriceListDTO.class );
        PriceListDTO priceList = responseEntity.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(priceList);

    }

    @Test
    public void test_addMenuItem_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);


        String path = this.basePath + "/addMenuItem";

        //kreiramo objekat priceList-a
        MenuDTO menuDTO = new MenuDTO(this.menuService.findActiveMenu(true).get());
        //formiracemo ovaj item i dodati ga u bazu da bude vec kreiran ali nije dodat u ovaj meni
        Item newItem = new Item(MenuConstants.NAME, MenuConstants.ORDER_CATEGORY, MenuConstants.ORDER_SUBCATEGORY, MenuConstants.ALERGEN_LIST, MenuConstants.DESCRIPTION, true);
        this.itemService.save(newItem);
        ItemDTO itemDTO = new ItemDTO(newItem);

        PriceListDTO newPriceList = new PriceListDTO(MenuConstants.PRICE_LIST_ID, menuDTO, itemDTO, MenuConstants.ITEM_PRICE, MenuConstants.DATE_FROM, MenuConstants.DATE_TO, MenuConstants.IS_ACTIVE);
        HttpEntity httpEntity = new HttpEntity(newPriceList, headers);

        ResponseEntity<PriceListDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.POST, httpEntity, PriceListDTO.class );
        PriceListDTO priceList = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        //testiraj broj priceList objekata u meniju nakon dodavanja
        int numberOfPriceList = this.priceListService.findAll().size();
        assertEquals(MenuConstants.NUMBER_OF_PRICE_LIST+1, numberOfPriceList);

        //roll back >> brisanje dodaog itema u meni
        priceListService.remove(MenuConstants.ID_ITEM);
        itemService.remove(MenuConstants.ID_ITEM);

    }

    /*** logicko brisanje stavke iz ponude menija ****/

    @Test
    public  void test_deleteItemFromMenu_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);

        PriceList beforeLogicalDelete = priceListService.findActiveItemPriceObjectFromMenu(MenuConstants.MENU_ID_1, MenuConstants.ITEM_ID_EXIST);

        String path = this.basePath + "/deleteItemFromMenu/" + MenuConstants.ITEM_ID_EXIST;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class );

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        //proverimo koliko ima objeakta u meniju nakon brisanja >> posto je logicko ostace isti broj elemenata
        int numberAfterDelete = menuService.findAllWithMenuObj(MenuConstants.MENU_ID_1).size();
        assertEquals(MenuConstants.NUMBER_OF_PRICE_LIST, numberAfterDelete);

        //proverimo da li je obrisan logicki objekat sa trazenim id-ijem
        PriceList deletedLogicaly = priceListService.findOneWithItem(MenuConstants.ITEM_ID_EXIST);
        assertEquals(false, deletedLogicaly.isActive());

        //moze da se proveri i da li je datum promenjen
        assertNotNull(deletedLogicaly.getDateTo());

        //roll-back
        //treba da updatujemo logicki obrisani objekat
        this.priceListService.update(beforeLogicalDelete, MenuConstants.ITEM_ID_EXIST);


    }

    @Test
    public void test_deleteItemFromMenu_itemNotFound(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);

        String path = this.basePath + "/deleteItemFromMenu/" + MenuConstants.ID_ITEM;

        ResponseEntity<Void> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.DELETE, httpEntity, Void.class );

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

    }

    /*
    promena cene priceList objektu menija >> testovi
     */
    @Test
    public void test_changeItemPrice_succesfuly(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);


        //pre promene priceList objekat
        PriceList beforeChangeObj = priceListService.findActiveItemPriceObjectFromMenu(MenuConstants.MENU_ID_1, MenuConstants.ITEM_ID_EXIST);

        String path = this.basePath + "/changeItemPrice/" + MenuConstants.ITEM_ID_EXIST + "?newPriceOfItem="+ MenuConstants.ITEM_PRICE;

        ResponseEntity<PriceListDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT, httpEntity, PriceListDTO.class);

        PriceListDTO price = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        //proverimo da li je prethodna cena proglasena za neaktivnu i da li je datum vazenja do razlicit od null
        List<PriceList> allPriceForItem = priceListService.findAllWithItem(MenuConstants.ITEM_ID_EXIST);

        //predposlednja je stara cena
        //proverimo da li je promenjena aktivnost na false
        assertFalse(allPriceForItem.get(allPriceForItem.size()-2).isActive());
        //proverimo da li je podesena granica do za datum vazenja cene
        assertNotNull(allPriceForItem.get(allPriceForItem.size()-2).getDateTo());

        //********* provere za novu cenu**********
        //postavljena je nova cena koja je prosledjena
        assertEquals(MenuConstants.ITEM_PRICE, allPriceForItem.get(allPriceForItem.size()-1).getItemPrice());
        //poslednja cena je nova cena koja je sada aktivna
        assertTrue(allPriceForItem.get(allPriceForItem.size()-1).isActive());
        //poslednja cena je nova cena koja ima datum pocetka vazenja ali nema datum do kog vazi
        assertNotNull(allPriceForItem.get(allPriceForItem.size()-1).getDateFrom());
        assertNull(allPriceForItem.get(allPriceForItem.size()-1).getDateTo());

        //radimo roll-back
        //brisemo poslednji dodat koji je proglasen za aktivan
        priceListService.remove(allPriceForItem.get(allPriceForItem.size()-1).getId());
        //updatujemo predposlednji objekat >> vratimo ga na staro kao pre logickog brisanja
        priceListService.update(beforeChangeObj, allPriceForItem.get(allPriceForItem.size()-2).getId());
    }

    /*
    priceList objekat za item sa prosledjenim id-ijem koji trebamo da promenimo ne postoji u okviru tog menija kao aktivan
     */
    @Test
    public void test_changeItemPrice_itemNotExist(){
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        HttpEntity httpEntity = new HttpEntity(headers);

        String path = this.basePath + "/changeItemPrice/" + MenuConstants.ID_ITEM + "?newPriceOfItem="+ MenuConstants.ITEM_PRICE;

        ResponseEntity<PriceListDTO> responseEntity = this.testRestTemplate.exchange(path, HttpMethod.PUT, httpEntity, PriceListDTO.class);

        PriceListDTO price = responseEntity.getBody();
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertNull(price);

    }










}