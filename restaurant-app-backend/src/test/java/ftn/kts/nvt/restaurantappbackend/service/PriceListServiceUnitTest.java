package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.ItemConstants;
import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.constants.PriceListConstants;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.model.PriceList;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;
import ftn.kts.nvt.restaurantappbackend.repository.PriceListRepostory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.swing.plaf.DimensionUIResource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class PriceListServiceUnitTest {

    @MockBean
    private PriceListRepostory priceListRepostory;

    @MockBean
    private MenuRepository menuRepository;

    @Autowired
    private PriceListService priceListService;

    @BeforeEach
    public void setUp() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);

        PriceList pl1 = new PriceList(menu, item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1, PriceListConstants.IS_ACTIVE1);
        pl1.setId(PriceListConstants.PRICE_LIST_ID1);
        PriceList pl2 = new PriceList(menu, item1, PriceListConstants.ITEM_PRICE2, PriceListConstants.DATE_FROM2, PriceListConstants.DATE_TO2, PriceListConstants.IS_ACTIVE2);
        pl2.setId(PriceListConstants.PRICE_LIST_ID2);

        List<PriceList> allPriceLists = new ArrayList<>();

        allPriceLists.add(pl1);
        allPriceLists.add(pl2);

        // ** mokovanje metoda PRICE LIST REPOZITORIJUMA

        // findAll()
        Mockito.when(priceListRepostory.findAll()).thenReturn(allPriceLists);

        //findById(id)
        Mockito.when(priceListRepostory.findById(PriceListConstants.PRICE_LIST_ID1)).thenReturn(Optional.of(pl1));
        Mockito.when(priceListRepostory.findById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST)).thenReturn(null);

        //remove
        Mockito.doNothing().when(priceListRepostory).deleteById(PriceListConstants.PRICE_LIST_ID1);

        //findOneWithItem(itemId)
        Mockito.when(priceListRepostory.findOneWithItem(PriceListConstants.ID_ITEM1)).thenReturn(pl1);

        //findAllWithItem(itemId)
        Mockito.when(priceListRepostory.findAllwithItem(PriceListConstants.ID_ITEM1)).thenReturn(allPriceLists);

        //findAllWithMenu(idMenu)
        Mockito.when(priceListRepostory.findAllWithMenu(MenuConstants.MENU_ID_1)).thenReturn(allPriceLists);

        //findById(id)
        Mockito.when(priceListRepostory.findById(PriceListConstants.PRICE_LIST_ID1)).thenReturn(Optional.of(pl1));
        Mockito.when(priceListRepostory.findById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST)).thenReturn(null);





    }

    @Test
    public void test_findAll() {
        List<PriceList> found = this.priceListService.findAll();

        assertNotNull(found);
        assertEquals(PriceListConstants.ALL_FOUNDED, found.size());
    }

    @Test
    void test_findOneById_succesfuly() {
        PriceList found = priceListService.findOneById(PriceListConstants.PRICE_LIST_ID1);

        assertNotNull(found);
        assertEquals(PriceListConstants.PRICE_LIST_ID1, found.getId());
    }

    @Test
    void test_findOneById_priceListNotExist() {
        //proverimo da li je bacen NullPointException
        Exception exception = assertThrows(NullPointerException.class, () -> {
            PriceList found = priceListService.findOneById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST);
            assertNull(found);
        });
        //proverimo da li je pozivana funkcija findById(id)
        verify(priceListRepostory, times(1)).findById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST);

    }


    @Test
    void test_save() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);

        PriceList pl1 = new PriceList(menu, item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1, PriceListConstants.IS_ACTIVE1);
        pl1.setId(PriceListConstants.PRICE_LIST_ID1);

        Mockito.when(this.priceListRepostory.save(pl1)).thenReturn(pl1);

        PriceList saved = this.priceListService.save(pl1);

        assertNotNull(saved);
        assertEquals(PriceListConstants.PRICE_LIST_ID1, saved.getId());
    }

    @Test
    void test_remove() {
        this.priceListService.remove(PriceListConstants.PRICE_LIST_ID1);
        Mockito.verify(this.priceListRepostory, times(1)).deleteById(PriceListConstants.PRICE_LIST_ID1);
    }

    @Test
    void test_findOneWithItem() {
        PriceList found = this.priceListService.findOneWithItem(PriceListConstants.ID_ITEM1);

        assertNotNull(found);
        assertEquals(PriceListConstants.ID_ITEM1, found.getItem().getIdItem());
    }

    @Test
    void test_findAllWithItem() {
        List<PriceList> found = priceListService.findAllWithItem(PriceListConstants.ID_ITEM1);

        assertNotNull(found);
        assertEquals(PriceListConstants.ALL_FOUNDED, found.size());
    }


    @Test
    void test_findActiveItemPriceObjectFromMenu() {
        PriceList activePriceList = priceListService.findActiveItemPriceObjectFromMenu(MenuConstants.MENU_ID_1, PriceListConstants.ID_ITEM1);

        assertNotNull(activePriceList);
        //proverimo da li se id-trenutno aktivnog poklapa sa pronadjenim
        assertEquals(PriceListConstants.PRICE_LIST_ID1, activePriceList.getId());
        //proverimo da li joj je cena aktivna
        assertTrue(activePriceList.isActive());
    }

    @Test
    void test_update_succesfuly() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);

        PriceList pl1 = new PriceList(menu, item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1,false);
        //pl1.setId(PriceListConstants.PRICE_LIST_ID1);

        boolean updated = priceListService.update(pl1, PriceListConstants.PRICE_LIST_ID1);

        assertTrue(updated);
        assertFalse(priceListService.findOneById(PriceListConstants.PRICE_LIST_ID1).isActive());

    }

    @Test
    void test_update_priceListNotExist(){
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);

        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);

        PriceList pl1 = new PriceList(menu, item1, PriceListConstants.ITEM_PRICE1, PriceListConstants.DATE_FROM1, PriceListConstants.DATE_TO1,false);
        //pl1.setId(PriceListConstants.PRICE_LIST_ID1);

        //proverimo da li je bacen NullPointException
        Exception exception = assertThrows(NullPointerException.class, () -> {
            boolean updated = priceListService.update(pl1, PriceListConstants.ID_PRICE_LIST_NOT_EXIST);

            assertFalse(updated);
        });
        //proverimo da li je pozivana funkcija findById(id)
        verify(priceListRepostory, times(1)).findById(PriceListConstants.ID_PRICE_LIST_NOT_EXIST);



    }
}