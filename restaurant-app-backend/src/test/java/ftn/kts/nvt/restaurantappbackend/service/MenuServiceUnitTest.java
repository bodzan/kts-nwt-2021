package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.model.Menu;
import ftn.kts.nvt.restaurantappbackend.repository.MenuRepository;
import ftn.kts.nvt.restaurantappbackend.repository.PriceListRepostory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class MenuServiceUnitTest {

    @Autowired
    private MenuService menuService;

    @MockBean
    private PriceListRepostory priceListRepostory;

    @MockBean
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);
        menu.setIdMenu(MenuConstants.MENU_ID_1);

        Menu menu2 = new Menu(MenuConstants.MENU_IS_ACTIVE_2);
        menu2.setIdMenu(MenuConstants.MENU_ID_2);

        List<Menu> menuList = new ArrayList<>();
        menuList.add(menu);
        menuList.add(menu2);



        //mokovanje funkcija MENU REPOZITORIJUMA

        //findById()
        Mockito.when(this.menuRepository.findByIdMenu(MenuConstants.MENU_ID_1)).thenReturn(Optional.of(menu));
        Mockito.when(this.menuRepository.findByIdMenu(MenuConstants.MENU_ID_NOT_EXIST)).thenReturn(Optional.empty());

        //findAll()
        Mockito.when(this.menuRepository.findAll()).thenReturn(menuList);

        //remove()
        doNothing().when(menuRepository).deleteById(MenuConstants.MENU_ID_1);

        //logicalRemove()
        //--- metoda findById() je vec mokovana

        //findActiveMenu()
        Mockito.when(this.menuRepository.findByIsActive(MenuConstants.MENU_IS_ACTIVE_1)).thenReturn(Optional.of(menu));
        Mockito.when(this.menuRepository.findByIsActive(MenuConstants.MENU_IS_ACTIVE_2)).thenReturn(Optional.empty());

    }

    @Test
    public void test_findById_menuWithIdExist() {
        //trazimo sa id-ijem >>
        Optional<Menu> found = menuService.findById(MenuConstants.MENU_ID_1);

        Mockito.verify(this.menuRepository, times(1)).findByIdMenu(MenuConstants.MENU_ID_1);
        assertTrue(found.isPresent());
        assertEquals(MenuConstants.MENU_ID_1, found.get().getIdMenu());
    }

    @Test
    public void test_findById_menuWithIdNotExist() {
        Optional<Menu> found = menuService.findById(MenuConstants.MENU_ID_NOT_EXIST);

        Mockito.verify(this.menuRepository, times(1)).findByIdMenu(MenuConstants.MENU_ID_NOT_EXIST);
        assertFalse(found.isPresent());

    }

    @Test
    public void test_findAll() {
        List<Menu> found = this.menuService.findAll();

        Mockito.verify(menuRepository, times(1)).findAll();
        assertNotNull(found);
        assertEquals(MenuConstants.ALL_MENU_SIZE, found.size());
    }

    @Test
    public void test_save() {
        Menu menu = new Menu(MenuConstants.MENU_IS_ACTIVE_1);
        menu.setIdMenu(MenuConstants.MENU_ID_1);

        //save() >> mokovanje
        Mockito.when(this.menuRepository.save(menu)).thenReturn(menu);

        Menu savedMenu = menuService.save(menu);
        System.out.println(savedMenu.isActive());

        verify(menuRepository, times(1)).save(menu);
        assertNotNull(savedMenu);
        assertEquals(MenuConstants.MENU_ID_1, savedMenu.getIdMenu());
    }

    @Test
    public void test_remove() {
        menuService.remove(MenuConstants.MENU_ID_1);
        verify(menuRepository, times(1)).deleteById(MenuConstants.MENU_ID_1);
    }

    @Test
    public void test_logicalRemove_idExists(){
        boolean removedLogic = this.menuService.logicalRemove(MenuConstants.MENU_ID_1);

        verify(this.menuRepository, times(1)).findByIdMenu(MenuConstants.MENU_ID_1);
        assertTrue(removedLogic);
    }

    @Test
    public void test_logicalRemove_idNotExist(){
        boolean removedLogic = this.menuService.logicalRemove(MenuConstants.MENU_ID_NOT_EXIST);

        verify(this.menuRepository, times(1)).findByIdMenu(MenuConstants.MENU_ID_NOT_EXIST);
        assertFalse(removedLogic);
    }

    @Test
    public void test_findActiveMenu_activeMenuExists() {
        Optional<Menu> found = this.menuService.findActiveMenu(MenuConstants.MENU_IS_ACTIVE_1);

        verify(this.menuRepository, times(1)).findByIsActive(MenuConstants.MENU_IS_ACTIVE_1);
        assertTrue(found.isPresent());
        assertEquals(MenuConstants.MENU_IS_ACTIVE_1, found.get().isActive());
    }

    //trazimo menije ciji je status aktivnosti false, ali takvi meniji ne postoje
    @Test
    public void test_findActiveMenu_notActiveMenuNotExist(){
        Optional<Menu> found = this.menuService.findActiveMenu(MenuConstants.MENU_IS_ACTIVE_2);

        verify(this.menuRepository, times(1)).findByIsActive(MenuConstants.MENU_IS_ACTIVE_2);
        assertFalse(found.isPresent());

    }

    @Test
    public void test_update_succesfuly(){
        Menu updateMenu = new Menu(MenuConstants.MENU_IS_ACTIVE_2);

        boolean updated = this.menuService.update(updateMenu, MenuConstants.MENU_ID_1);

        assertTrue(updated);
        //proverimo da li je updatovan
        assertEquals(MenuConstants.MENU_IS_ACTIVE_2, this.menuService.findById(MenuConstants.MENU_ID_1).get().isActive());

    }
    @Test
    public void test_update_menuNotExist(){
        Menu updateMenu = new Menu(MenuConstants.MENU_IS_ACTIVE_2);

        boolean updated = this.menuService.update(updateMenu, MenuConstants.MENU_ID_NOT_EXIST);

        assertFalse(updated);

    }


}