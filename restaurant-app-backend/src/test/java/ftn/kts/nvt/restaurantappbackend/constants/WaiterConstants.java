package ftn.kts.nvt.restaurantappbackend.constants;


import ftn.kts.nvt.restaurantappbackend.model.Order;

public class WaiterConstants {
    /** Podaci za unit testove za item ****/

    Order pom = new Order();
    public static final Long idOrder_editOrder = 1L;
    public static final int null_findAll = 6;

    public static final int by_name_findAll = 1;
    public static final String name_findAll = "cola";


    public static final String waiter_username = "konobar";
    public static final String waiter_password = "sifra123";

    public static final int C_tableOrder_value = 1;
    public static final Long C_tableOrder_idTable = 1L;

    public static final Long C_findOrderedItemById_idOrdered = 1L;

    public static final Long C_createNewOrder_idTable = 3L;
}
