package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.constants.ItemConstants;
import ftn.kts.nvt.restaurantappbackend.constants.MenuConstants;
import ftn.kts.nvt.restaurantappbackend.model.Item;
import ftn.kts.nvt.restaurantappbackend.repository.ItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class ItemServiceUnitTest {

    @Autowired
    private ItemService itemService;

    @MockBean
    private ItemRepository itemRepository;

    @BeforeEach
    public void setUp() {
        Item item1 = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        item1.setIdItem(ItemConstants.ID_ITEM1);
        Item item2 = new Item(ItemConstants.ITEM_NAME2, ItemConstants.ORDER_CATEGORY2, ItemConstants.ORDER_SUBCATEGORY2, ItemConstants.ALERGEN_LIST2, ItemConstants.DESCRIPTION2, ItemConstants.IS_ACTIVE2 );
        item2.setIdItem(ItemConstants.ID_ITEM2);

        List<Item> allItems = new ArrayList<>();
        allItems.add(item1);
        allItems.add(item2);

        //Mokovanje metoda ITEM REPOZITORIJUMA

        // findAll() metoda
        Mockito.when(itemRepository.findAll()).thenReturn(allItems);

        //findOneById(id)
        Mockito.when(itemRepository.findOneByIdItem(ItemConstants.ID_ITEM1)).thenReturn(item1);

        //remove()
        Mockito.doNothing().when(itemRepository).deleteById(ItemConstants.ID_ITEM1);

        //findById(id)
        //slucaj kad je prosledjen id objekta koji postoji
        Mockito.when(itemRepository.findById(ItemConstants.ID_ITEM1)).thenReturn(Optional.of(item1));
        //prosledjen id objekta koji ne postoji
        Mockito.when(itemRepository.findById(ItemConstants.ID_NOT_EXIST)).thenReturn(Optional.empty());

        //logicalRemove(id)>> isto kao i prethodno sa findById



    }

    @Test
    public void test_findAll() {
        List<Item> found = itemService.findAll();

        Mockito.verify(itemRepository, times(1)).findAll();

        assertNotNull(found);
        assertEquals(ItemConstants.ALL_FOUNDED_NUMBER, found.size());
    }

    @Test
    void test_findOneById() {
        Item found = itemService.findOneById(ItemConstants.ID_ITEM1);

        Mockito.verify(itemRepository, times(1)).findOneByIdItem(ItemConstants.ID_ITEM1);

        assertNotNull(found);
        assertEquals(ItemConstants.ID_ITEM1, found.getIdItem());
    }

    @Test
    public void test_save() {

        Item itemToSave = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, ItemConstants.IS_ACTIVE1 );
        itemToSave.setIdItem(ItemConstants.ID_ITEM1);

        //save(Item item) mokovanje
        Mockito.when(itemRepository.save(itemToSave)).thenReturn(itemToSave);

        Item savedItem = itemRepository.save(itemToSave);

        Mockito.verify(itemRepository, times(1)).save(itemToSave);

        assertEquals(ItemConstants.ID_ITEM1, savedItem.getIdItem());
    }

    @Test
    public void test_remove() {
        itemService.remove(ItemConstants.ID_ITEM1);
        Mockito.verify(itemRepository, times(1)).deleteById(ItemConstants.ID_ITEM1);
    }

    @Test
    public void test_findById_succesful() {
        Optional<Item> found = itemService.findById(ItemConstants.ID_ITEM1);

        verify(itemRepository, times(1)).findById(ItemConstants.ID_ITEM1);
        assertNotNull(found);
        assertEquals(ItemConstants.ID_ITEM1, found.get().getIdItem());
    }

    //kad ne postoji item sa tim id-ijem
    @Test
    public void test_findById_notExist(){
        Optional<Item> found = itemService.findById(ItemConstants.ID_NOT_EXIST);

        verify(itemRepository, times(1)).findById(ItemConstants.ID_NOT_EXIST);
        assertFalse(found.isPresent());
    }

    @Test
    public void test_logicalRemove_succesful() {
        boolean removed = itemService.logicalRemove(ItemConstants.ID_ITEM1);

        verify(itemRepository, times(1)).findById(ItemConstants.ID_ITEM1);
        assertTrue(removed);

        //proverimo da li je postavljen na false isActive property
        Optional<Item> itemLogicalyRemoved = itemService.findById(ItemConstants.ID_ITEM1);
        assertFalse(itemLogicalyRemoved.get().isActive());

    }

    @Test
    public void test_logicalRemove_itemNotExist(){
        boolean removed = this.itemService.logicalRemove(ItemConstants.ID_NOT_EXIST);

        assertFalse(removed);
    }

    @Test
    public void test_update_succesful() {
        Item itemUpdate = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, false );
        Mockito.when(itemRepository.save(itemUpdate)).thenReturn(itemUpdate);

        boolean updated = this.itemService.update(itemUpdate, ItemConstants.ID_ITEM1);

        verify(itemRepository, times(1)).findById(ItemConstants.ID_ITEM1);

        assertTrue(updated);
        //da li item sa tim id-ijem sad nije aktivan >>odnosno da li je updatovan
        Optional<Item> updatedItem = itemService.findById(ItemConstants.ID_ITEM1);
        assertFalse(updatedItem.get().isActive());

    }

    @Test
    public void test_update_itemNotExist(){
        Item itemUpdate = new Item(ItemConstants.ITEM_NAME1, ItemConstants.ORDER_CATEGORY1, ItemConstants.ORDER_SUBCATEGORY1, ItemConstants.ALERGEN_LIST1, ItemConstants.DESCRIPTION1, false );
        Mockito.when(itemRepository.save(itemUpdate)).thenReturn(itemUpdate);

        boolean updated = this.itemService.update(itemUpdate, ItemConstants.ID_NOT_EXIST);

        verify(itemRepository, times(1)).findById(ItemConstants.ID_NOT_EXIST);

        assertFalse(updated);

    }
}