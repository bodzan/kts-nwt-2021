package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.User;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.RoleConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.SalaryConstants.*;

@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserServiceIntegrationTest {

    @Autowired
    private UserService userService;


    @BeforeEach
    void setUp() {
    }

    @Test
    void loadUserByUsername() {
        UserDetails userDetails = userService.loadUserByUsername(ADMIN_EMAIL1);
        assertEquals(ADMIN_EMAIL1, userDetails.getUsername());
    }

    @Test
    void getUserPage() {
        Pageable pageable = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);
        Page<User> userPage = userService.getUserPage(pageable);

        assertEquals(GET_ALL_PAGE_USERS, userPage.getNumberOfElements());
    }

    @Test
    void getAllUsers() {
        List<User> users = userService.getAllUsers();

        assertEquals(GET_ALL_NUMBER_OF_USERS, users.size());
    }

    @Test
    void getAllActiveUsers() {
        List<User> users = userService.getAllActiveUsers();

        assertEquals(GET_ALL_ACTIVE_USERS_NUMBER_OF_USERS, users.size());
    }

    @Test
    void getAllAdmins() {
        List<User> users = userService.getAllAdmins();

        assertEquals(NUMBER_OF_ADMINS, users.size());
    }

    @Test
    void getAllManagers() {
        List<User> users = userService.getAllManagers();

        assertEquals(NUMBER_OF_MANAGERS, users.size());
    }

    @Test
    void getAllBartenders() {
        List<User> users = userService.getAllBartenders();

        assertEquals(NUMBER_OF_BARTENDERS, users.size());
    }

    @Test
    void getAllCooks() {
        List<User> users = userService.getAllCooks();

        assertEquals(NUMBER_OF_COOKS, users.size());
    }

    @Test
    void getAllWaiters() {
        List<User> users = userService.getAllWaiters();

        assertEquals(NUMBER_OF_WAITERS, users.size());
    }

    @Test
    void getById() {
    }

    @Test
    void deleteUserById() {
    }

    @Test
    void updateSalary() {
    }

    @Test
    void addNewUser() {
    }
}