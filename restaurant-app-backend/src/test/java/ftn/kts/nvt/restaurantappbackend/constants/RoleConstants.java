package ftn.kts.nvt.restaurantappbackend.constants;

public class RoleConstants {
    public static final long ADMIN_ROLE_ID = 1L;
    public static final String ADMIN_ROLE_NAME = "ROLE_ADMIN";
    public static final long MANAGER_ROLE_ID = 2L;
    public static final String MANAGER_ROLE_NAME = "ROLE_MANAGER";
    public static final long BARTENDER_ROLE_ID = 3L;
    public static final String BARTENDER_ROLE_NAME = "ROLE_BARTENDER";
    public static final long WAITER_ROLE_ID = 4L;
    public static final String WAITER_ROLE_NAME = "ROLE_WAITER";
    public static final long COOK_ROLE_ID = 5L;
    public static final String COOK_ROLE_NAME = "ROLE_COOK";
}
