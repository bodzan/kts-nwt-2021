package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.*;
import ftn.kts.nvt.restaurantappbackend.helper.LayoutMapper;
import ftn.kts.nvt.restaurantappbackend.helper.UserMapper;
import ftn.kts.nvt.restaurantappbackend.model.Room;
import ftn.kts.nvt.restaurantappbackend.service.RestaurantLayoutService;
import ftn.kts.nvt.restaurantappbackend.service.TablesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.LayoutConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.SalaryConstants.NEW_USER_SALARY_CURRENT;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.NEW_USER_EMAIL;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
class AdminControllerIntegrationTest {

    @Autowired
    private TablesService tablesService;

    @Autowired
    private RestaurantLayoutService restaurantLayoutService;

    @Autowired
    private TestRestTemplate restTemplate;

    private LayoutMapper layoutMapper;

    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = restTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @BeforeEach
    void setUp() {

        layoutMapper = new LayoutMapper();
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void getActiveLayout() {
        login(ADMIN_EMAIL1, ADMIN_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ResponseEntity<RestaurantLayoutDTO> responseEntity = restTemplate.exchange("/api/admins/layouts/active", HttpMethod.GET, httpEntity, RestaurantLayoutDTO.class);

        RestaurantLayoutDTO layoutDTO = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(layoutDTO);
        assertEquals(ACTIVE_LAYOUT_ID, layoutDTO.getId());
    }

    @Test
    void getAllLayouts() {
        login(ADMIN_EMAIL1, ADMIN_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<RestaurantLayoutDTO>> layoutResponse = new ParameterizedTypeReference<List<RestaurantLayoutDTO>>() {};

        ResponseEntity<List<RestaurantLayoutDTO>> responseEntity = restTemplate.exchange("/api/admins/layouts/all", HttpMethod.GET, httpEntity, layoutResponse);

        List<RestaurantLayoutDTO> allLayouts = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allLayouts);
        assertEquals(NUMBER_OF_LAYOUTS, allLayouts.size());
    }

    @Test
    void getLayoutById() {
        login(ADMIN_EMAIL1, ADMIN_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ResponseEntity<RestaurantLayoutDTO> responseEntity = restTemplate.exchange("/api/admins/layouts/" + ACTIVE_LAYOUT_ID, HttpMethod.GET, httpEntity, RestaurantLayoutDTO.class);

        RestaurantLayoutDTO layoutDTO = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(layoutDTO);
        assertEquals(ACTIVE_LAYOUT_ID, layoutDTO.getId());
    }

    @Test
    void createNewRestaurantLayout() {
        login(ADMIN_EMAIL1, ADMIN_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        RestaurantLayoutDTO newLayoutDTO = new RestaurantLayoutDTO();
        newLayoutDTO.setActive(false);
        TableLayoutDTO t1 = new TableLayoutDTO(LAYOUT_TABLE_ID1, Room.GROUND_FLOOR);
        TableLayoutDTO t2 = new TableLayoutDTO(LAYOUT_TABLE_ID2, Room.GROUND_FLOOR);
        TableLayoutDTO t3 = new TableLayoutDTO(LAYOUT_TABLE_ID3, Room.GROUND_FLOOR);
        List<TableLayoutDTO> tableLayoutDTOS = Arrays.asList(t1, t2, t3);
        newLayoutDTO.setTables(tableLayoutDTOS);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(newLayoutDTO, headers);


        ResponseEntity<RestaurantLayoutDTO> responseEntity =
                restTemplate.exchange("/api/admins/layouts/new", HttpMethod.POST, httpEntity, RestaurantLayoutDTO.class);

        RestaurantLayoutDTO layoutDTO = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(layoutDTO);
        assertEquals(NEW_LAYOUT_ID, layoutDTO.getId());
    }
}