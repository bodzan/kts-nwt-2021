package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.repository.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static ftn.kts.nvt.restaurantappbackend.constants.RoleConstants.*;

@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RoleServiceIntegrationTest {

    @Autowired
    private RoleRepository roleRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getRoleByName() {
        Role role = roleRepository.findByName(ADMIN_ROLE_NAME);
        assertEquals(ADMIN_ROLE_NAME, role.getName());
    }
}