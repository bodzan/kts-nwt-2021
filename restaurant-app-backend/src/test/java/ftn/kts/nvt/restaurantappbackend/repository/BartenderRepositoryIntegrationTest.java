package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.BartenderConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.RoleConstants.BARTENDER_ROLE_ID;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource("classpath:application-test.properties")
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class BartenderRepositoryIntegrationTest {

    @Autowired
    private BartenderRepository bartenderRepository;


    @Test
    void showAllItemsWithCategoryAndStatusDrinkWaiting() {
        List<Item> items = bartenderRepository.showAllItemsWithCategoryAndStatus(OrderCategory.DRINK, MenuItemStatus.WAITING);
        assertEquals(showAllItemsWithCategoryAndStatusDrinkWaiting_value, items.size());
    }

    @Test
    void showAllItemsWithCategoryAndStatusDishWaiting() {
        List<Item> items = bartenderRepository.showAllItemsWithCategoryAndStatus(OrderCategory.DISH, MenuItemStatus.WAITING);
        assertEquals(showAllItemsWithCategoryAndStatusDishWaiting_value, items.size());
    }

    @Test
    void showAllItemsWithCategoryAndStatusDrinkDone() {
        List<Item> items = bartenderRepository.showAllItemsWithCategoryAndStatus(OrderCategory.DRINK, MenuItemStatus.DONE);
        assertEquals(showAllItemsWithCategoryAndStatusDrinkDone_value, items.size());
    }

    @Test
    void showAllItemsWithCategoryAndStatusDishDone() {
        List<Item> items = bartenderRepository.showAllItemsWithCategoryAndStatus(OrderCategory.DISH, MenuItemStatus.DONE);
        assertEquals(showAllItemsWithCategoryAndStatusDishDone_value, items.size());
    }

}