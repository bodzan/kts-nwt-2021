package ftn.kts.nvt.restaurantappbackend.repository;

import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.RoleConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource("classpath:application-test.properties")
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
class UserRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {

        Role roleBartender = entityManager.find(Role.class, BARTENDER_ROLE_ID);
        entityManager.persist(new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(roleBartender)));
//        entityManager.persist(new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(roleBartender)));
//        entityManager.persist(new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(roleBartender)));
    }

    @Test
    void findByRoles_Id() {
        List<User> waiters = userRepository.findByRoles_Id(BARTENDER_ROLE_ID);
        assertEquals(NEW_NUMBER_OF_BARTENDERS, waiters.size());

    }


    @Test
    void findAllByDeletedFalse() {
        List<User> all = userRepository.findAllByDeletedFalse();
        assertEquals(ACTIVE_USERS, all.size());
    }
}