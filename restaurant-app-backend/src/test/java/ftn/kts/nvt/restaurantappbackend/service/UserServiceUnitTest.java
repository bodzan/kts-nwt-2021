package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.Role;
import ftn.kts.nvt.restaurantappbackend.model.Salary;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.repository.RoleRepository;
import ftn.kts.nvt.restaurantappbackend.repository.SalaryChangeRepository;
import ftn.kts.nvt.restaurantappbackend.repository.SalaryRepository;
import ftn.kts.nvt.restaurantappbackend.repository.UserRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static ftn.kts.nvt.restaurantappbackend.constants.RoleConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.SalaryConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserServiceUnitTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private
    RoleRepository roleRepository;

    @MockBean
    private SalaryRepository salaryRepository;

    @MockBean
    private SalaryChangeRepository salaryChangeRepository;

    @BeforeEach
    public void setUp() {

        Role adminRole = new Role(ADMIN_ROLE_ID, ADMIN_ROLE_NAME);
        Role managerRole = new Role(MANAGER_ROLE_ID, MANAGER_ROLE_NAME);
        Role bartenderRole = new Role(BARTENDER_ROLE_ID, BARTENDER_ROLE_NAME);
        Role waiterRole = new Role(WAITER_ROLE_ID, WAITER_ROLE_NAME);
        Role cookRole = new Role(COOK_ROLE_ID, COOK_ROLE_NAME);

        given(roleRepository.findByName(ADMIN_ROLE_NAME)).willReturn(adminRole);
        given(roleRepository.findByName(BARTENDER_ROLE_NAME)).willReturn(bartenderRole);
        given(roleRepository.findByName(COOK_ROLE_NAME)).willReturn(cookRole);
        given(roleRepository.findByName(MANAGER_ROLE_NAME)).willReturn(managerRole);
        given(roleRepository.findByName(WAITER_ROLE_NAME)).willReturn(waiterRole);

        User admin = new User();
        admin.setId(ADMIN_ID1);
        admin.setEmail(ADMIN_EMAIL1);
        admin.setPassword(ADMIN_PASSWORD1);
        admin.setFirstName(ADMIN_FIRSTNAME1);
        admin.setLastName(ADMIN_LASTNAME1);
        admin.setRoles(Arrays.asList(adminRole, managerRole));

        User bartender = new User();
        bartender.setId(BARTENDER_ID);
        bartender.setEmail(BARTENDER_EMAIL);
        bartender.setPassword(BARTENDER_PASSWORD);
        bartender.setFirstName(BARTENDER_FIRSTNAME);
        bartender.setLastName(BARTENDER_LASTNAME);
        bartender.setRoles(Arrays.asList(bartenderRole));

        User waiter = new User();
        waiter.setId(WAITER_ID);
        waiter.setEmail(WAITER_EMAIL);
        waiter.setPassword(WAITER_PASSWORD);
        waiter.setFirstName(WAITER_FIRSTNAME);
        waiter.setLastName(WAITER_LASTNAME);
        waiter.setRoles(Arrays.asList(waiterRole));
        waiter.setDeleted(true);

        User cook = new User();
        cook.setId(COOK_ID);
        cook.setEmail(COOK_EMAIL);
        cook.setPassword(COOK_PASSWORD);
        cook.setFirstName(COOK_FIRSTNAME);
        cook.setLastName(COOK_LASTNAME);
        cook.setRoles(Arrays.asList(cookRole));

        List<User> allUsers = Arrays.asList(admin, bartender, cook, waiter);
        List<User> activeUsers = Arrays.asList(admin, bartender, cook);

        Pageable pageable = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);
        Page<User> userPage = new PageImpl<>(allUsers, pageable,
                GET_ALL_NUMBER_OF_USERS);


        given(userRepository.findAll(pageable)).willReturn(userPage);


        given(userRepository.findAll()).willReturn(allUsers);

        given(userRepository.findByEmailAndDeletedFalse(ADMIN_EMAIL1)).willReturn(Optional.of(admin));
        given(userRepository.findAllByDeletedFalse()).willReturn(activeUsers);
        given(userRepository.findByRoles_Id(adminRole.getId())).willReturn(Collections.singletonList(admin));
        given(userRepository.findByRoles_Id(managerRole.getId())).willReturn(Collections.singletonList(admin));
        given(userRepository.findByRoles_IdAndDeletedIsFalse(bartenderRole.getId())).willReturn(Collections.singletonList(bartender));
        given(userRepository.findByRoles_Id(waiterRole.getId())).willReturn(Collections.singletonList(waiter));
        given(userRepository.findByRoles_Id(cookRole.getId())).willReturn(Collections.singletonList(cook));


        User newUser = new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(waiterRole));


        User savedUser = new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(waiterRole));
        savedUser.setId(NEW_USER_ID);


        Salary newSalary = new Salary(NEW_SALARY_CURRENT);
        Salary savedSalary = new Salary(NEW_SALARY_CURRENT);
        savedSalary.setId(NEW_SALARY_ID);

        newUser.setSalary(newSalary);
        savedUser.setSalary(savedSalary);

        given(userRepository.findById(waiter.getId())).willReturn(Optional.of(waiter));

        given(salaryRepository.save(newSalary)).willReturn(savedSalary);
        given(userRepository.save(newUser)).willReturn(savedUser);




    }

    @Test
    public void loadUserByUsername() {

    }

    @Test
    public void getUserPage() {
        Pageable pageable = PageRequest.of(PAGE_NUMBER, PAGE_SIZE);
        Page<User> userPage = userService.getUserPage(pageable);

        verify(userRepository, times(1)).findAll(pageable);
        assertEquals(GET_ALL_PAGE_USERS, userPage.getNumberOfElements());
    }

    @Test
    public void getAllUsers() {
        List<User> allUsers = userService.getAllUsers();

        verify(userRepository, times(1)).findAll();
        assertEquals(GET_ALL_NUMBER_OF_USERS, allUsers.size());
    }

    @Test
    public void getAllActiveUsers() {
        List<User> activeUsers = userService.getAllActiveUsers();

        verify(userRepository, times(1)).findAllByDeletedFalse();
        assertEquals(GET_ALL_ACTIVE_USERS_NUMBER_OF_USERS, activeUsers.size());
    }


    @Order(1)
    @Test
    public void getAllAdmins() {
        List<User> users = userService.getAllAdmins();

        verify(roleRepository, times(1)).findByName(ADMIN_ROLE_NAME);
        verify(userRepository, times(1)).findByRoles_Id(ADMIN_ROLE_ID);
        assertEquals(NUMBER_OF_ADMINS, users.size());
    }

    @Order(2)
    @Test
    public void getAllManagers() {
        List<User> users = userService.getAllManagers();

        verify(roleRepository, times(1)).findByName(MANAGER_ROLE_NAME);
        verify(userRepository, times(1)).findByRoles_Id(MANAGER_ROLE_ID);
        assertEquals(NUMBER_OF_MANAGERS, users.size());
    }

    @Order(3)
    @Test
    public void getAllBartenders() {
        List<User> users = userService.getAllBartenders();

        verify(roleRepository, times(1)).findByName(BARTENDER_ROLE_NAME);
        verify(userRepository, times(1)).findByRoles_IdAndDeletedIsFalse(BARTENDER_ROLE_ID);
        assertEquals(NUMBER_OF_BARTENDERS, users.size());
    }

    @Order(4)
    @Test
    public void getAllCooks() {
        List<User> users = userService.getAllCooks();

        verify(roleRepository, times(1)).findByName(COOK_ROLE_NAME);
        verify(userRepository, times(1)).findByRoles_Id(COOK_ROLE_ID);
        assertEquals(NUMBER_OF_COOKS, users.size());
    }

    @Order(5)
    @Test
    public void getAllWaiters() {
        List<User> users = userService.getAllWaiters();

        verify(roleRepository, times(1)).findByName(WAITER_ROLE_NAME);
        verify(userRepository, times(1)).findByRoles_Id(WAITER_ROLE_ID);
        assertEquals(NUMBER_OF_WAITERS, users.size());
    }

    @Test
    public void getById() {
        Optional<User> user = userService.getById(WAITER_ID);

        verify(userRepository, times(1)).findById(WAITER_ID);
        assertEquals(WAITER_ID, user.orElseThrow().getId());

    }

    @Test
    public void deleteUserById() {
        boolean deleted = userService.deleteUserById(WAITER_ID);

        verify(userRepository, times(1)).findById(WAITER_ID);
        assertTrue(deleted);
    }

    @Test
    public void updateSalary() {
    }

    @Test
    public void addNewUser() {
        Role waiterRole = new Role(WAITER_ROLE_ID, WAITER_ROLE_NAME);
        User newUser = new User(NEW_USER_EMAIL, NEW_USER_FIRSTNAME, NEW_USER_LASTNAME, NEW_USER_PASSWORD, false, Arrays.asList(waiterRole));
        Salary newSalary = new Salary(NEW_SALARY_CURRENT);
        newUser.setSalary(newSalary);

        newUser = userService.addNewUser(newUser, newSalary);

        verify(salaryRepository, times(1)).save(newSalary);
//        verify(userRepository, times(1)).save(newUser);

        assertEquals(NEW_USER_EMAIL, newUser.getEmail());

    }
}