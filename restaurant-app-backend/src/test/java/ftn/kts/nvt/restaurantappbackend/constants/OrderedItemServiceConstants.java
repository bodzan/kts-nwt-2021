package ftn.kts.nvt.restaurantappbackend.constants;

public class OrderedItemServiceConstants {
    public static final long FIND_ALL_NUMBER_OF_ITEMS = 5;
    public static final Long ORDERED_ITEM_ID = 1L;
    public static final Integer NEW_OI_QUANTITY = 3;

    public static final Long ORDERED_ITEM_ID_NONEXISTANT = 17L;

    public static final Long ORDER_ID = 1L;
    public static final Integer ORDER_ID_SIZE = 3;
    public static final long FIND_BY_ORDER_ID_NUMBER = 3;
}
