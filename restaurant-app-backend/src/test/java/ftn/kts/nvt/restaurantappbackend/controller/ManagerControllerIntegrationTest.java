package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.AuthTokenDTO;
import ftn.kts.nvt.restaurantappbackend.dto.LoginDTO;
import ftn.kts.nvt.restaurantappbackend.dto.UpdateSalaryDTO;
import ftn.kts.nvt.restaurantappbackend.dto.UserDTO;
import ftn.kts.nvt.restaurantappbackend.helper.UserMapper;
import ftn.kts.nvt.restaurantappbackend.helper.UserPageImpl;
import ftn.kts.nvt.restaurantappbackend.model.Salary;
import ftn.kts.nvt.restaurantappbackend.model.User;
import ftn.kts.nvt.restaurantappbackend.service.PriceListService;
import ftn.kts.nvt.restaurantappbackend.service.RoleService;
import ftn.kts.nvt.restaurantappbackend.service.UserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static ftn.kts.nvt.restaurantappbackend.constants.SalaryConstants.*;
import static ftn.kts.nvt.restaurantappbackend.constants.UserConstants.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
//@Transactional
class ManagerControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PriceListService priceListService;

    private UserMapper mapper;

    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = restTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @BeforeEach
    void setUp() {

        mapper = new UserMapper();
    }

    @AfterEach
    void tearDown() {
        Optional<User> optionalUser = userService.getById(NEW_USER_ID);
        optionalUser.ifPresent(user -> userService.trueDeleteById(user.getId()));

    }

    @Test
    void getAllUsers() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(GET_ALL_NUMBER_OF_USERS, allUsers.size());

    }

    @Test
    void getAllActiveUsers() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/allActive", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(GET_ALL_ACTIVE_USERS_NUMBER_OF_USERS, allUsers.size());
    }

    @Test
    void getAllAdmins() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all/admins", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(NUMBER_OF_ADMINS, allUsers.size());
    }

    @Test
    void getAllManagers() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all/managers", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(NUMBER_OF_MANAGERS, allUsers.size());
    }

    @Test
    void getAllBartenders() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all/bartenders", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(NUMBER_OF_BARTENDERS, allUsers.size());
    }

    @Test
    void getAllWaiters() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all/waiters", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(NUMBER_OF_WAITERS, allUsers.size());
    }

    @Test
    void getAllCooks() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<UserDTO>> userPageResponseType = new ParameterizedTypeReference<List<UserDTO>>() {};

        ResponseEntity<List<UserDTO>> responseEntity = restTemplate.exchange("/api/managers/all/cooks", HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(NUMBER_OF_COOKS, allUsers.size());
    }

    @Test
    void getUserById() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ResponseEntity<UserDTO> responseEntity = restTemplate.exchange("/api/managers/" + WAITER_ID, HttpMethod.GET, httpEntity, UserDTO.class);

        UserDTO user = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(user);
        assertEquals(WAITER_ID, user.getId());
    }

    @Test
    void getUserPage() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<UserPageImpl<UserDTO>> userPageResponseType = new ParameterizedTypeReference<UserPageImpl<UserDTO>>() {};

        ResponseEntity<UserPageImpl<UserDTO>> responseEntity = restTemplate.exchange(
                "/api/managers/page?page="+ PAGE_NUMBER + "&size="+ PAGE_SIZE, HttpMethod.GET, httpEntity, userPageResponseType);

        List<UserDTO> allUsers = responseEntity.getBody().getContent();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allUsers);
        assertEquals(GET_ALL_PAGE_USERS, allUsers.size());

    }


    @Test
    void deleteUserById() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

//        ParameterizedTypeReference<UserPageImpl<UserDTO>> userPageResponseType = new ParameterizedTypeReference<UserPageImpl<UserDTO>>() {};

        ResponseEntity<Void> responseEntity = restTemplate.exchange(
                "/api/managers/" + WAITER_ID, HttpMethod.DELETE, httpEntity, Void.class);


        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

    }

    @Test
    void addNewEmployee() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(NEW_USER_EMAIL);
        userDTO.setFirstName(NEW_USER_FIRSTNAME);
        userDTO.setLastName(NEW_USER_LASTNAME);
        userDTO.setRole(NEW_USER_ROLE);
        userDTO.setEmploymentDate(LocalDate.now());
        userDTO.setSalary(NEW_USER_SALARY_CURRENT);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(userDTO, headers);


        ResponseEntity<UserDTO> responseEntity =
                restTemplate.exchange("/api/managers/newEmployee", HttpMethod.POST, httpEntity, UserDTO.class);

        UserDTO newUser = responseEntity.getBody();

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(newUser);
        assertEquals(NEW_USER_EMAIL, newUser.getEmail());
    }

    @Test
    void changeEmployeesSalary() {
        login(MANAGER_EMAIL1, MANAGER_PASSWORD1);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);

        UpdateSalaryDTO salaryDTO = new UpdateSalaryDTO();
        salaryDTO.setNewSalary(NEW_SALARY_CURRENT);
        salaryDTO.setUserId(BARTENDER_ID);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(salaryDTO, headers);


        ResponseEntity<Salary> responseEntity =
                restTemplate.exchange("/api/managers/employeeSalary", HttpMethod.PUT, httpEntity, Salary.class);

        Salary s = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(s);
        assertEquals(NEW_SALARY_CURRENT, s.getCurrentSalary());

    }



}