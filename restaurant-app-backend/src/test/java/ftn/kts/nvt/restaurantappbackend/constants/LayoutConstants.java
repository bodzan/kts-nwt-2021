package ftn.kts.nvt.restaurantappbackend.constants;

public class LayoutConstants {

    public static final long ACTIVE_LAYOUT_ID = 1L;
    public static final long NEW_LAYOUT_ID = 2L;

    public static final long LAYOUT_TABLE_ID1 = 1L;
    public static final long LAYOUT_TABLE_ID2 = 2L;
    public static final long LAYOUT_TABLE_ID3 = 3L;

    public static final int NUMBER_OF_LAYOUTS = 2;

}
