package ftn.kts.nvt.restaurantappbackend.service;

import ftn.kts.nvt.restaurantappbackend.model.OrderedItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.OrderedItemServiceConstants.*;
import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
public class OrderedItemServiceIntegrationTest {

    @Autowired
    private OrderedItemService orderedItemService;

    @Test
	public void testFindAll() {
        List<OrderedItem> found = orderedItemService.findAll();
        assertEquals(FIND_ALL_NUMBER_OF_ITEMS, found.size());
    }

    @Test
	public void testFindOneById() {
        OrderedItem found = orderedItemService.findOneById(ORDERED_ITEM_ID);
        assertEquals(ORDERED_ITEM_ID, found.getId());

    }

    // @Test
	// public void testSave() {
    //     OrderedItem newitem = new OrderedItem();
    //     newitem.setQuantity(NEW_OI_QUANTITY);
    //     OrderedItem oi = orderedItemService.save(newitem);
    //     assertEquals(NEW_OI_QUANTITY, oi.getQuantity());
    // }

//    @Test
//	public void testFindByOrderId() {
//        List<OrderedItem> found = orderedItemService.findByOrderId(ORDER_ID);
//        assertEquals(ORDER_ID_SIZE, found.size());
//
//    }

    @Test
	public void testRemove() {
        orderedItemService.remove(ORDERED_ITEM_ID);
        //TODO: sta assertovati?
    }

    
    
}
