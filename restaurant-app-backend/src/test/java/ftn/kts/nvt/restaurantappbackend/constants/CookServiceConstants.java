package ftn.kts.nvt.restaurantappbackend.constants;

public class CookServiceConstants {
	public static final Long COOK_ID = 5L;
	public static final Long COOK_ID_NON_EXISTING = 17L;
	public static final long FIND_ALL_NUMBER_OF_ITEMS = 1;
	public static final String NEW_COOK_FNAME = "IME";
	public static final String NEW_COOK_LNAME = "PREZIME";

}
