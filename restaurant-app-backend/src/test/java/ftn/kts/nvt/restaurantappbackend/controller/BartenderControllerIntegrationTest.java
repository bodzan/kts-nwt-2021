package ftn.kts.nvt.restaurantappbackend.controller;

import ftn.kts.nvt.restaurantappbackend.dto.*;
import ftn.kts.nvt.restaurantappbackend.helper.LayoutMapper;
import ftn.kts.nvt.restaurantappbackend.model.Room;
import ftn.kts.nvt.restaurantappbackend.service.RestaurantLayoutService;
import ftn.kts.nvt.restaurantappbackend.service.TablesService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static ftn.kts.nvt.restaurantappbackend.constants.BartenderConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application-test.properties")
@ActiveProfiles("test")
class BartenderControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private String accessToken;

    public void login(String username, String password) {
        ResponseEntity<AuthTokenDTO> responseEntity = restTemplate.postForEntity("/auth/login",
                new LoginDTO(username,password), AuthTokenDTO.class);
        accessToken = "Bearer " + responseEntity.getBody().getAccessToken();
    }

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void getAllItems() {
        login(bartender_username, bartender_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<ItemDTO>> layoutResponse = new ParameterizedTypeReference<List<ItemDTO>>() {};

        ResponseEntity<List<ItemDTO>> responseEntity = restTemplate.exchange("/api/bartender/allWaitingItems", HttpMethod.GET, httpEntity, layoutResponse);

        List<ItemDTO> allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
        assertEquals(C_getAllItems_value, allItems.size());
    }

    @Test
    void allWaitingDrinks() {
        login(bartender_username, bartender_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<ItemDTO>> layoutResponse = new ParameterizedTypeReference<List<ItemDTO>>() {};

        ResponseEntity<List<ItemDTO>> responseEntity = restTemplate.exchange("/api/bartender/allWaitingDrinks", HttpMethod.GET, httpEntity, layoutResponse);

        List<ItemDTO> allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
        assertEquals(C_allWaitingDrinks_value, allItems.size());
    }

    @Test
    void allPreparingDrinks() {
        login(bartender_username, bartender_password);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", accessToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Object> httpEntity = new HttpEntity<Object>(headers);

        ParameterizedTypeReference<List<ItemDTO>> layoutResponse = new ParameterizedTypeReference<List<ItemDTO>>() {};

        ResponseEntity<List<ItemDTO>> responseEntity = restTemplate.exchange("/api/bartender/allPreparingDrinks", HttpMethod.GET, httpEntity, layoutResponse);

        List<ItemDTO> allItems= responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(allItems);
        assertEquals(C_allPreparingDrinks_value, allItems.size());
    }

}