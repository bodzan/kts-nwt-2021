insert into salary(current_salary) values (40000.0);
insert into salary(current_salary) values (60000.0);
insert into salary(current_salary) values (55000.0);
insert into salary(current_salary) values (80000.0);
insert into salary(current_salary) values (62000.0);

INSERT into users(deleted, first_name, last_name, email, password, employment_date, salary_id)
values (false, 'Bojan', 'Cakic', 'admin', '$2a$12$taVJL8lHZ/vI2cX9i/3Sj.C4YbDQdPG0bjEaNjUcZKOvrGHOy9kYK', '2020-06-22', 1);

INSERT into users(deleted, first_name, last_name, email, password, employment_date, salary_id)
values (false, 'Zoran', 'Stankovic', 'menadzer', '$2a$12$taVJL8lHZ/vI2cX9i/3Sj.C4YbDQdPG0bjEaNjUcZKOvrGHOy9kYK', '2020-06-22', 5);

INSERT into users(deleted, first_name, last_name, email, password, employment_date, salary_id)
values (false, 'Nikola', 'Nikolic', 'barista', '$2a$12$taVJL8lHZ/vI2cX9i/3Sj.C4YbDQdPG0bjEaNjUcZKOvrGHOy9kYK', '2020-06-22', 2);

INSERT into users(deleted, first_name, last_name, email, password, employment_date, salary_id)
values (false, 'Marko', 'Markovic', 'konobar', '$2a$12$taVJL8lHZ/vI2cX9i/3Sj.C4YbDQdPG0bjEaNjUcZKOvrGHOy9kYK', '2020-06-22', 3);

INSERT into users(deleted, first_name, last_name, email, password, employment_date, salary_id)
values (false, 'Ana', 'Anic', 'kuvar', '$2a$12$taVJL8lHZ/vI2cX9i/3Sj.C4YbDQdPG0bjEaNjUcZKOvrGHOy9kYK', '2020-06-22', 4);

INSERT into role(id, name) values (1, 'ROLE_ADMIN');
INSERT into role(id, name) values (2, 'ROLE_MANAGER');
INSERT into role(id, name) values (3, 'ROLE_BARTENDER');
INSERT into role(id, name) values (4, 'ROLE_WAITER');
INSERT into role(id, name) values (5, 'ROLE_COOK');

INSERT into authority(id, name) values (1, 'ADMIN_PERMISSIONS');
INSERT into authority(id, name) values (2, 'MANAGER_PERMISSIONS');
INSERT into authority(id, name) values (3, 'BARTENDER_PERMISSIONS');
INSERT into authority(id, name) values (4, 'WAITER_PERMISSIONS');
INSERT into authority(id, name) values (5, 'COOK_PERMISSIONS');

insert into user_role (user_id, role_id) values (1, 1);
insert into user_role (user_id, role_id) values (2, 2);
insert into user_role (user_id, role_id) values (3, 3);
insert into user_role (user_id, role_id) values (4, 4);
insert into user_role (user_id, role_id) values (5, 5);

insert into role_authority (role_id, authority_id) values (1, 1);
insert into role_authority (role_id, authority_id) values (2, 2);
insert into role_authority (role_id, authority_id) values (3, 3);
insert into role_authority (role_id, authority_id) values (4, 4);
insert into role_authority (role_id, authority_id) values (5, 5);


insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('pileca supa', 'DISH', 'SOUPS', '', 'Pileca supa sa povrcem..', true );
insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('govedja supa', 'DISH', 'SOUPS', '', 'Govedja supa sa povrcem..', true  );
insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('riblja corba', 'DISH', 'SOUPS', '', 'Riblja corba sa mixom od 4 vrste ribe..', true  );


insert into menu(is_active) values (true);

insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 1, 300.00, '2020-02-02', '2021-11-11', true);
insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 2, 300.00, '2020-02-02', '2021-11-11', true);
insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 3, 350.00, '2020-02-02', '2021-11-11', true);

/*podaci za narudzbine
 */
insert into sto_tabela(restaurant_room, is_active) values ('GROUND_FLOOR', true);
insert into sto_tabela(restaurant_room, is_active) values ('GROUND_FLOOR', true);
insert into sto_tabela(restaurant_room, is_active) values ('GROUND_FLOOR', true);

insert into order_tabela(date_and_time, note, status, total_price, deliver_for_table_id_table) values ('2021-11-20 13:00', 'nema napomena', 'WAITING', 0, 1);
insert into order_tabela(date_and_time, note, status, total_price, deliver_for_table_id_table) values ('2021-11-20 12:00', 'nema napomena', 'WAITING', 0, 2);

insert into ordered_item(order_id_order, menu_item_id_item, quantity, ordered_item_status) values(1, 2, 1, 'WAITING');
insert into ordered_item(order_id_order, menu_item_id_item, quantity, ordered_item_status) values(1, 1, 1, 'WAITING');
insert into ordered_item(order_id_order, menu_item_id_item, quantity, ordered_item_status) values(1, 3, 1, 'WAITING');

insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('cola', 'DRINK', 'NON_ALCOHOLIC_DRINKS', '', 'Koka kola..', true  );
insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('caj nana', 'DRINK', 'HOT_DRINKS', '', 'Caj od nane..', true  );
insert into item (item_name, category, subcategory, alergen_list, description, is_active) values ('voda voda', 'DRINK', 'WATER', '', 'Arteska voda..', true  );

insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 4, 300.00, '2020-02-02', '2021-11-11', true);
insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 5, 300.00, '2020-02-02', '2021-11-11', true);
insert into price_list(menu_id_menu, item_id_item, item_price, date_from, date_to, is_active) values(1, 6, 350.00, '2020-02-02', '2021-11-11', true);

insert into ordered_item(order_id_order, menu_item_id_item, quantity, ordered_item_status) values(2, 5, 1, 'WAITING');
insert into ordered_item(order_id_order, menu_item_id_item, quantity, ordered_item_status) values(2, 4, 1, 'WAITING');


insert into restaurant_layout(coordinates, active) values ('1,30,60;2,100,120;3,50,150', true);
insert into layout_tables(layout_id, table_id) values (1, 1);
insert into layout_tables(layout_id, table_id) values (1, 2);
insert into layout_tables(layout_id, table_id) values (1, 3);

insert into restaurant_layout(coordinates, active) values ('1,60,60;2,200,220;3,150,250', false);
insert into layout_tables(layout_id, table_id) values (2, 1);
insert into layout_tables(layout_id, table_id) values (2, 2);
insert into layout_tables(layout_id, table_id) values (2, 3);